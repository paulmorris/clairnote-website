module.exports = {
  parser: "@typescript-eslint/parser",
  extends: [
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended",
    "prettier",
  ],
  plugins: ["@typescript-eslint", "prettier", "jsx-a11y"],
  parserOptions: {
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: "module", // Allows for the use of imports
  },
  env: {
    browser: true,
    node: true,
  },
  rules: {
    quotes: "off",
    "@typescript-eslint/quotes": [
      2,
      "double",
      {
        avoidEscape: true,
      },
    ],
    // In the future, @typescript-eslint/explicit-module-boundary-types would
    // be good to enable but there are too many errors right now.
    "@typescript-eslint/explicit-module-boundary-types": "off",

    // indent: ["error", 2, { SwitchCase: 1 }],
    "@typescript-eslint/ban-ts-comment": "off",
    "@typescript-eslint/no-var-requires": "off",

    "prettier/prettier": [
      "error",
      {
        trailingComma: "es5",
      },
    ],
  },
};
