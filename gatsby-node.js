/**
 * Implement Gatsby's Node APIs in this file.
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const pathLib = require("path");
// const { createFilePath } = require("gatsby-source-filesystem");
const { slugFromTag, yearsFromPosts } = require("./src/js/utils-commonjs.js");

const makePath = (date, slug) =>
  `/blog/${date.slice(0, 4)}/${date.slice(5, 7)}/${slug}/`;

exports.onCreateNode = ({ node, actions }) => {
  if (node.internal.type === "MarkdownRemark") {
    const path = makePath(node.frontmatter.date, node.frontmatter.slug);
    actions.createNodeField({
      node,
      name: "path",
      value: path,
    });
  }
};

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  return new Promise((resolve, reject) => {
    graphql(`
      {
        allMarkdownRemark(
          filter: { frontmatter: { draft: { ne: true } } }
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                path
              }
              frontmatter {
                title
                date
              }
            }
          }
          group(field: frontmatter___tags) {
            fieldValue
          }
        }
      }
    `).then((result) => {
      if (result.errors) {
        return reject(result.errors);
      }

      // post pages
      const posts = result.data.allMarkdownRemark.edges;
      const last = posts.length - 1;
      posts.forEach((post, i) => {
        const previous = i === last ? null : posts[i + 1].node;
        const next = i === 0 ? null : posts[i - 1].node;
        createPage({
          path: post.node.fields.path,
          component: pathLib.resolve("./src/templates/blog-post.js"),
          context: {
            // Data passed to context is available
            // in page queries as GraphQL variables.
            previous,
            next,
          },
        });
      });

      // tag pages
      const tags = result.data.allMarkdownRemark.group;
      tags.forEach((tag) => {
        createPage({
          path: `/blog/tag/${slugFromTag(tag.fieldValue)}/`,
          component: pathLib.resolve("./src/templates/tag-page.js"),
          context: {
            tag: tag.fieldValue,
          },
        });
      });

      // year pages
      yearsFromPosts(posts).forEach((year) => {
        createPage({
          path: `/blog/${year}/`,
          component: pathLib.resolve("./src/templates/year-page.js"),
          context: {
            yearRegex: `/${year}/`,
            year,
          },
        });
      });

      resolve();
    });
  });
};
