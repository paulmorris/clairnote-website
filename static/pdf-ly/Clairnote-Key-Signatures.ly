\version "2.19.49"
\language "english"
% clairnote-type = sn

\include "clairnote.ly"

\pointAndClickOff

#(set-default-paper-size "letterlandscape")

\header{
  title = \markup \small \concat { "Key Signatures in " \clairnoteTypeName " Music Notation" }
  tagline = \markup \teeny \lower #5 \concat { \clairnoteTypeName " Music Notation | " \clairnoteTypeUrl " | Music engraving by LilyPond | www.lilypond.org" }
}

\paper {
  top-margin = 10
  bottom-margin = 10
  print-page-number = ##f
}

majscale = \relative { c'8 d e f g a b c }
minscale = \relative { a'8 b c d e f g a }

modalSet =
#(define-music-function (bar from-pitch) (ly:music? ly:pitch?)
   #{
     $bar
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ g' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ d' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ a' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ e' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ b' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ fs' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ cs' #}) { $bar }
     \break
     $bar
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ f' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ bf' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ ef' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ af' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ df' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ gf' #}) { $bar }
     \transpose $from-pitch #(ly:pitch-transpose from-pitch #{ cf' #}) { $bar }
     \break
   #})

keyparade = {
  \clef treble
  \override Staff.TimeSignature #'stencil = ##f
  \time 1/1

  \key c \major
  \majscale
  \key g \major
  \transpose c g { \majscale }
  \key d \major
  \transpose c d { \majscale }
  \key a \major
  \transpose c a { \majscale }
  \key e \major
  \transpose c e { \majscale }
  \key b \major
  \transpose c b { \majscale }
  \key fs \major
  \transpose c fs { \majscale }
  \key cs \major
  \transpose c cs { \majscale }
  \break

  \key c \major
  \majscale
  \key f \major
  \transpose c f { \majscale }
  \key bf \major
  \transpose c bf { \majscale }
  \key ef \major
  \transpose c ef { \majscale }
  \key af \major
  \transpose c af { \majscale }
  \key df \major
  \transpose c df { \majscale }
  \key gf \major
  \transpose c gf { \majscale }
  \key cf \major
  \transpose c cf { \majscale }

  \pageBreak
  \key a \minor
  \minscale
  \key e \minor
  \transpose a e { \minscale }
  \key b \minor
  \transpose a b { \minscale }
  \key fs \minor
  \transpose a fs { \minscale }
  \key cs \minor
  \transpose a cs { \minscale }
  \key gs \minor
  \transpose a gs { \minscale }
  \key ds \minor
  \transpose a ds { \minscale }
  \key as \minor
  \transpose a as { \minscale }
  \break

  \key a \minor
  \minscale
  \key d \minor
  \transpose a d { \minscale }
  \key g \minor
  \transpose a g { \minscale }
  \key c \minor
  \transpose a c { \minscale }
  \key f \minor
  \transpose a f { \minscale }
  \key bf \minor
  \transpose a bf { \minscale }
  \key ef \minor
  \transpose a ef { \minscale }
  \key af \minor
  \transpose a af { \minscale }


  % MODES
  \pageBreak
  \modalSet { \key c \ionian c'1 } c
  \modalSet { \key d \dorian d'1 } d
  \pageBreak
  \modalSet { \key e \phrygian e'1 } e
  \modalSet { \key f \lydian f'1 } f
  \pageBreak
  \modalSet { \key g \mixolydian g'1 } g
  \modalSet { \key a \aeolian a'1 } a
  \pageBreak
  \modalSet { \key b \locrian b'1 } b
}

keyParadeMarkups = {
  \override TextScript #'extra-offset = #'(-5 . 3)
  s1^\markup \tiny "Sharp Major Keys"
  s1 s1 s1
  s1 s1 s1 s1
  s1^\markup \tiny "Flat Major Keys"
  s1 s1 s1
  s1 s1 s1 s1
  s1^\markup \tiny "Sharp Minor Keys"
  s1 s1 s1
  s1 s1 s1 s1
  s1^\markup \tiny "Flat Minor Keys"
  s1 s1 s1
  s1 s1 s1 s1
  s1^\markup \tiny "Sharp Ionian (Major) Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Flat Ionian (Major) Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Sharp Dorian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Flat Dorian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Sharp Phrygian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Flat Phrygian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Sharp Lydian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Flat Lydian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Sharp Mixolydian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Flat Mixolydian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "SharpAeolian (Minor) Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Flat Aeolian (Minor) Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Sharp Locrian Keys"
  s1 s1 s1 s1 s1 s1 s1
  s1^\markup \tiny "Flat Locrian Keys"
  s1 s1 s1 s1 s1 s1 s1
}

\score
{
  <<
    \new Staff \with {
      explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    }
    {
      <<
        \voiceOne {
          \keyparade
        }
        \voiceTwo {
          \keyParadeMarkups
        }
      >>
    }

    \new TradStaff \with {
      % \override Clef #'space-alist #'key-signature = #'(minimum-space . 5.0)
      explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
      printKeyCancellation = ##f
      \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)
    }
    { \keyparade }

  >>
  \layout {
    indent = 0
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
    % \midi { }
  }
}
