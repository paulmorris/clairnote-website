\version "2.19.49"
\include "clairnote-code.ly"

#(set-default-paper-size "letter")

\header {
  title = "Clairnote Template with Piano Staff and Lyrics"
  tagline = \markup \teeny {Clairnote Music Notation   (clairnote.org)   Music engraving by LilyPond   (www.lilypond.org)}
}

upper = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4
  a4 b c d
}

lower = \relative c {
  \clef bass
  \key c \major
  \time 4/4
  a2 c
}

text = \lyricmode {
  Aaa Bee Cee Dee
}

\score {
  \new GrandStaff <<

    \new Staff = upper \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
    } {
      \new Voice = "singer" \upper
    }

    \new Lyrics \lyricsto "singer" \text

    \new Staff = lower \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
    } {
      \lower
    }

  >>

  \layout {
    \context {
      \GrandStaff
      \accepts "Lyrics"
    }
    \context {
      \Lyrics
      \consists "Bar_engraver"
    }
  }
  \midi { }
}