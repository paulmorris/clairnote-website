\version "2.19.49"
% clairnote-type = dn
\include "clairnote.ly"

#(set-default-paper-size "letter")

\header {
  title = "Clairnote Template with Single Staff and Chords"
  tagline = \markup \teeny {Clairnote Music Notation   (clairnote.org)   Music engraving by LilyPond   (www.lilypond.org)}
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

  f4 e8[ c] d4 g
  a2 ~ a
}

harmonies = \chordmode {
  c4:m f:min7 g:maj c:aug
  d2:dim b:5
}

\score {
  <<

    \new ChordNames {
      \set chordChanges = ##t
      \harmonies
    }

    \new Staff \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
    } {
      \melody
    }

  >>
  \layout { }
  \midi { }
}