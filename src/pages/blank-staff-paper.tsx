import { PageProps } from "gatsby";
import React from "react";

import { BlankStaffPaper } from "../components/pages/BlankStaffPaper";

import { snProps } from "../js/utils";

const BlankStaffPaperPage = (props: PageProps) => (
  <BlankStaffPaper {...{ ...snProps, ...props }} />
);

export default BlankStaffPaperPage;
