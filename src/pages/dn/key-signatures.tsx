import { PageProps } from "gatsby";
import React from "react";

import { KeySignatures } from "../../components/pages/KeySignatures";

import { dnProps } from "../../js/utils";

const KeySignaturesPage = (props: PageProps) => (
  <KeySignatures {...{ ...dnProps, ...props }} />
);

export default KeySignaturesPage;
