import { PageProps } from "gatsby";
import React from "react";

import { HandwrittenSheetMusic } from "../../components/pages/HandwrittenSheetMusic";

import { dnProps } from "../../js/utils";

const HandwrittenSheetMusicPage = (props: PageProps) => (
  <HandwrittenSheetMusic {...{ ...dnProps, ...props }} />
);

export default HandwrittenSheetMusicPage;
