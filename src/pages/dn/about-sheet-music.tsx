import { PageProps } from "gatsby";
import React from "react";

import { AboutSheetMusic } from "../../components/pages/AboutSheetMusic";

import { dnProps } from "../../js/utils";

const AboutSheetMusicPage = (props: PageProps) => (
  <AboutSheetMusic {...{ ...dnProps, ...props }} />
);

export default AboutSheetMusicPage;
