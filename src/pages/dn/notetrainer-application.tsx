import { PageProps } from "gatsby";
import React from "react";

import { NoteTrainerApplication } from "../../components/pages/NoteTrainerApplication";

import { dnProps } from "../../js/utils";

const NoteTrainerApplicationPage = (props: PageProps) => (
  <NoteTrainerApplication {...{ ...dnProps, ...props }} />
);

export default NoteTrainerApplicationPage;
