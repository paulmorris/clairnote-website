import { PageProps } from "gatsby";
import React from "react";

import { Community } from "../components/pages/Community";

import { snProps } from "../js/utils";

const AboutPage = (props: PageProps) => (
  <Community {...{ ...snProps, ...props }} />
);

export default AboutPage;
