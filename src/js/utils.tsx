import React from "react";
import { SelectOption, SnOrDnProps } from "../types/types";

/**
 * Like in Guile Scheme, return an array containing 'count' numbers,
 * starting from 'start' and adding 'step' each time.
 */
export const iota = (count: number, start = 0, step = 1) =>
  Array(count)
    .fill(undefined)
    .map((_, index) => start + index * step);

/**
 * Return an array of numbers from `start` to `end` (inclusive).
 */
export const range = (start: number, end: number): number[] =>
  Array.from({ length: end - start + 1 }, (_, index) => start + index);

export const capitalize = (s: string) => s.charAt(0).toUpperCase() + s.slice(1);

/**
 * Takes an integer n and returns a random integer between 0 and n - 1
 * (inclusive). For example, pass 3 and get back a 0, 1, or 2.
 */
export const randomInteger = (n: number) => {
  return Math.floor(Math.random() * n);
};

/**
 * Given an array of numbers, returns the number that's closest to a target
 * number.
 */
export const closestNumber = (numbers: number[], targetNumber: number) => {
  if (numbers.length === 0) {
    return undefined;
  }
  let closest = numbers[0];
  let smallestDiff = Infinity;

  numbers.forEach((number) => {
    const diff = Math.abs(targetNumber - number);
    if (diff < smallestDiff) {
      smallestDiff = diff;
      closest = number;
    }
  });

  return closest;
};

export const dnOnlyPages = [
  "/notetrainer-application/",
  "/handwritten-sheet-music/",
  "/software-musescore/",
];

export const snProps: SnOrDnProps = Object.freeze({
  sn: true,
  dn: false,
  clairnoteName: "Clairnote SN",
  otherClairnoteName: "Clairnote DN",
  urlDir: "/",
  otherUrlDir: "/dn/",
  lineOrSolid: "line",
  spaceOrHollow: "space",
});

export const dnProps: SnOrDnProps = Object.freeze({
  sn: false,
  dn: true,
  clairnoteName: "Clairnote DN",
  otherClairnoteName: "Clairnote SN",
  urlDir: "/dn/",
  otherUrlDir: "/",
  lineOrSolid: "solid",
  spaceOrHollow: "hollow",
});

export function makeSelectOptions(
  selectOptions: SelectOption<string | number>[]
) {
  return selectOptions.map(({ value, label }) => (
    <option value={value} key={value}>
      {label}
    </option>
  ));
}

/**
 * Trim whitespace from start and end of a string and replace multiple
 * whitespace characters with a single space. Optionally modify each word with
 * a mapping function.
 */
export const transformSearchQuery = (
  query: string,
  modifyWord?: (s: string) => string
): string => {
  const words1 = query.trim().split(/\s+/);

  const words2 = modifyWord ? words1.map(modifyWord) : words1;

  return words2.join(" ");
};
