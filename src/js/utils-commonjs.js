// This file's exports are used by gatsby-node.js so it needs to be a commonjs
// module.
// TODO: Could this become a TypeScript file?

const slugFromTag = (tag) => {
  return {
    LilyPond: "lilypond",
    MuseScore: "musescore",
    "Music Notation": "music-notation",
    "Sheet Music": "sheet-music",
    Untagged: "untagged",
    "Clairnote Website": "clairnote-website",
  }[tag];
};

/**
 * @param {{ node: { frontmatter:{ date: string } } }[]} posts
 * @return {string[]} Array of years.
 */
const yearsFromPosts = (posts) => {
  const years = new Set();
  posts.forEach((post) => {
    years.add(post.node.frontmatter.date.slice(0, 4));
  });
  return Array.from(years);
};

/* Use commonjs module syntax so we can import into gatsby-node.js. */
module.exports = {
  slugFromTag,
  yearsFromPosts,
};
