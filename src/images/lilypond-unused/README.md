This directory contains lilypond files for creating images
for the website, but these are currently not being used.
Probably they were used in the past.
Or they were created and were ready to use but were not used for some reason.

Disclaimer: no claims are made regarding the quality of the code in these .ly
files. They could undoubtedly be improved given sufficient time and effort.
