This directory contains 'work-in-progress' and 'draft'
lilypond files for creating images for the website.

Disclaimer: no claims are made regarding the quality of the code in these .ly
files. They could undoubtedly be improved given sufficient time and effort.
