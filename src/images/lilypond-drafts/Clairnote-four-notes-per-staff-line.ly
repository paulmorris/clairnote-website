\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 68)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

\layout {
  \context {
    \Score
    \remove "System_start_delimiter_engraver"
    % \override StaffGrouper.staff-staff-spacing.padding = #100
    % \override StaffGrouper.staff-staff-spacing.basic-distance = #100
  }
  \context {
    \Lyrics
    %\override VerticalAxisGroup.nonstaff-relatedstaff-spacing.basic-distance = #5
  }
  \context {
    \Staff
    \override VerticalAxisGroup
    .default-staff-staff-spacing.basic-distance = #18
  }
}

\score {
  \new Staff
  \with {
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \remove "Time_signature_engraver"
    \remove "Clef_engraver"
    % \remove "Bar_engraver"
    % \cnNoteheadStyle "funksol"
    \cnStaffOctaveSpan 1
  }{
    \new Voice = "one" {
      \override Score.SpacingSpanner
      .common-shortest-duration = #(ly:make-moment 1 12 )

      % \override TextScript #'extra-offset = #'(0 . 0)
      s8 % ^\markup \with-color #grey { \fontsize #-6 \sans ""}
      \cadenzaOn
      \stemUp
      e'4 f' fs' g' gs'
    }
  }
}
