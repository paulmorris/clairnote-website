\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 6.7 in) ; width
                                        (* 4.0 in) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0.5 \cm
  bottom-margin = 0.5 \cm
  left-margin = 0.5 \cm
  right-margin = 0.5 \cm

  top-system-spacing.basic-distance = 0
  top-system-spacing.minimum-distance = 0
  top-system-spacing.padding = 0

  top-markup-spacing.basic-distance = 0
  top-markup-spacing.minimum-distance = 0
  top-markup-spacing.padding = 0
}


text = \lyricmode {
  \markup \with-color #grey  \sans \fontsize #-7 {" "}
}

textB = \markup \with-color #grey  \sans \fontsize #-4 {Middle C (C4)}

\layout {
  % ragged-right = ##t
  indent = #0
  line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 16)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)
  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
}

\markup {
  \justify-line {
    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "one" {
              \clef treble
              c'4
            }
          }
          \new TradStaff {
            \new Voice = "two" {
              \clef treble
              c'4
            }
          }
          \new Lyrics \lyricsto "two" \text
        >>

      }
      \vspace #1
      \textB
      \vspace #0.3
      \with-color #grey  \sans \fontsize #-2 "Treble Clef"
    }
    % \hspace #2

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "three" {
              \clef bass
              c'4
            }
          }
          \new TradStaff {
            \new Voice = "four" {
              \clef bass
              c'4
            }
          }
          \new Lyrics \lyricsto "four" \text
        >>
      }
      \vspace #1
      \textB
      \vspace #0.3
      \with-color #grey  \sans \fontsize #-2 "Bass Clef"

    }
    % \hspace #2
    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "five" {
              \clef alto
              c'4
            }
          }
          \new TradStaff {
            \new Voice = "six" {
              \clef alto
              c'4
            }
          }
          \new Lyrics \lyricsto "six" \text
        >>
      }
      \vspace #1
      \textB
      \vspace #0.3
      \with-color #grey  \sans \fontsize #-2 "Alto Clef"
    }
  }
}