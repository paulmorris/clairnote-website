\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 16 cm) ; width
                                        (* 9 cm) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0
  bottom-margin = 0
  left-margin = 1
  right-margin = 1
  top-system-spacing = ##f
}

text = \lyricmode {
  \markup \with-color #grey  \sans \fontsize #-7 {" "}
}

textB = \markup \with-color #grey  \sans \fontsize #-5 {Middle C (C4)}

smallsize = #-4

\layout {
  % ragged-right = ##t
  % indent = #0
  % line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 16)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
}

\markup {
  \justify-line {

    \center-column {
      \vspace #1.5
      \score {
        <<
          \new Staff {
            \new Voice = "b" {
              s8 <c' c'' c'''>4
            }
          }
          \new Lyrics \lyricsto "b" \text
        >>

      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "Two-Octave"
      \with-color #grey  \sans \fontsize #smallsize "Clairnote Staff"
    }

    \center-column {
      \vspace #0.75
      \score {
        <<
          \new Staff \with {
            \cnStaffOctaveSpan 3
            \remove "Bar_engraver"
          }
          {
            \new Voice = "one" {
              s8 <c c' c'' c'''>4
            }
          }
          \new Lyrics \lyricsto "one" \text
        >>

      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "Three-Octave"
      \with-color #grey  \sans \fontsize #smallsize "Clairnote Staff"
    }

    \center-column {
      \score {
        <<
          \new Staff \with {
            \cnStaffOctaveSpan 4
            \remove "Bar_engraver"
          } {
            \new Voice = "three" {
              s8 <c, c c' c'' c'''>4
            }
          }
          \new Lyrics \lyricsto "three" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "Four-Octave"
      \with-color #grey  \sans \fontsize #smallsize "Clairnote Staff"
    }

  }
}