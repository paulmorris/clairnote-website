\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

% for properly scaled svg images for clairnote.org
#(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\layout {
  % \override Score.BarNumber.break-visibility = #all-invisible

  \context {
    \Staff
    \remove Clef_engraver
    \remove Time_signature_engraver
  }
}

\header {
  title = ""
  composer = ""
  % tagline = ""
}

\paper {
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = ##f
}

\relative c''' {
  \cadenzaOn
  s4
  % gs8[ a as b] c[ cs d ds] e[ f fs g]
  % c d e f fs g a as b c d e f fs g a b
  c_ \markup \sans \lower #2.0 \with-color #grey
  \fontsize #-6
  \center-column {
    "C ledger lines are not shown when they are not needed, so that ledger lines resemble"
    "the staff, to make reading ledger line notes easier."
  }
  cs d ds e f fs
  g gs a as b
  % c d e f g a b c
}

