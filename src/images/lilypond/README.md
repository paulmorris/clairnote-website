This directory contains .ly files used to generate the SVG images used on the
site.

Disclaimer: no claims are made regarding the quality of the code in these .ly
files. They could undoubtedly be improved given sufficient time and effort.
