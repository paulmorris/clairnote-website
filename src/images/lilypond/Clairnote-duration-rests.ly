\version "2.19.49"
\include "clairnote.ly"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-page-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
  paper-width = 13.0 \cm
  score-markup-spacing = 0 \in
  indent = 0
}

rests = \relative f {
  r1 r2 r4 r8 r16
}

fsize = -5
spc = 3

% we can't use lyrics for rests, they make no sound

restsMarks = {
  r1_\markup \with-color #grey \sans \fontsize #fsize \halign #-0.6 { \lower  #spc  \center-column {"Whole" "Rests"}}
  r2_\markup \with-color #grey \sans \fontsize #fsize \halign #-0.6 { \lower  #spc  \center-column {"Half" "Rests"}}
  r4_\markup \with-color #grey \sans \fontsize #fsize \halign #-0.3 { \lower  #spc  \center-column {"Quarter" "Rests"}}
  r8_\markup \with-color #grey \sans \fontsize #fsize \halign #-0.3 { \lower  #spc  \center-column {"8th" "Rests"}}
  r16_\markup \with-color #grey \sans \fontsize #fsize \halign #-0.5 { \lower #spc  \center-column {"16th" "Rests"}}
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }
    \new Voice = "one" {
      \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24)
      % \once \override Score.NoteColumn.X-offset = #spc
      % \override TextScript #'extra-offset = #'(0 . 0)
      % s4^\markup { \fontsize #-6 \with-color #grey \sans {Chromatic Scale}}
      \once \omit Rest
      r8
      \rests
      \once \omit Rest
      r8
    }
  >>
}

\markup \vspace #0.5

\score {
  <<
    \new TradStaff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }
    \new Voice = "two" {
      \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24)
      % \override TextScript #'extra-offset = #'(0 . 0)
      % s4^\markup { \fontsize #-6 \with-color #grey \sans {}}
      \once \omit Rest
      r8
      \restsMarks
      \once \omit Rest
      r8
    }
  >>
}
