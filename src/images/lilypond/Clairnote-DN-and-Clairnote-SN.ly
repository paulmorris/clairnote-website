\version "2.19.63"
clairnote-type = dn
\include "clairnote.ly"
\initClairnoteSN

#(set-global-staff-size 34)
\language "english"
#(set-default-paper-size "letter")

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
\pointAndClickOff

\layout {
  \override Score.BarNumber.break-visibility = #all-invisible

  \context {
    \Staff
    % ...
  }
}

\header {
  tagline = ""
}

\paper {
  page-breaking = #ly:one-page-breaking
  indent = 0
  ragged-right = ##t
  paper-width = 21.5 \cm

  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

m =
\relative {
  f' fs g gs 
  a bf b c 
  cs2 d 
  ds e
  <d f a>4 <bf d f> <c e g> <f, a c>4
}

\new TradStaff {
  \key f \major
  \numericTimeSignature
  \time 4/4
  \override TextScript #'extra-offset = #'(-6.5 . 0)
  <>^\markup \with-color #grey { \fontsize #-6 \sans "Traditional Music Notation"}
  \m
}


\new Staff {
  \key f \major
  \time 4/4
  \override TextScript #'extra-offset = #'(-9.5 . 0)
  <>^\markup \with-color #grey { \fontsize #-6 \sans "Clairnote SN"}
  \m
}

\new StaffClairnoteDN {
  \key f \major
  \time 4/4
  \override TextScript #'extra-offset = #'(-9.5 . 0)
  <>^\markup \with-color #grey { \fontsize #-6 \sans "Clairnote DN"}
  \m
}
