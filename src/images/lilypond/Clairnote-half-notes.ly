\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 4.5 in) ; width
                                        (* 1.5 in) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0 \cm
  bottom-margin = 0 \cm
  left-margin = 0.2 \cm
  right-margin = 0.2 \cm
  top-system-spacing = ##f
}


text = \lyricmode {
  \markup \with-color #grey  \sans \fontsize #-7 {" "}
}

textB = \markup \with-color #grey  \sans \fontsize #-5 {Middle C (C4)}

smallsize = #-6

\layout {
  % ragged-right = ##t
  % indent = #0
  % line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 4)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \remove "Bar_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \remove "Bar_engraver"
  }
}

\markup {
  \justify-line {

    \center-column {
      \score {
        \new TradStaff {
          \new Voice = "b" {
            \omit Rest r8
            e'2 f'
          }
        }
      }
      \vspace #1.12
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Traditional" "Half Notes"}
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \with-color #grey \large "="
    }

    \center-column {
      \score {
        \new Staff
        {
          \new Voice = "one" {
            \omit Rest r8
            e'2 f'
          }
        }

      }
      \vspace #0.8
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Clairnote" "Half Notes"}
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }
%{
    \center-column {
      \with-color #grey \large "="
    }

    \center-column {
      \score {
        \new Staff
        {
          \new Voice = "three" {
            \omit Rest r8
            gs'2
          }
        }
      }
      \vspace #0.8
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Clairnote" "Half Note"}
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }
%}
  }
}