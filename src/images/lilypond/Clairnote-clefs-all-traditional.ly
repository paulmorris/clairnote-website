\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 10 in) ; width
                                        (* 3.9 in) ; height
                                        )) paper-alist))
 
\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0.5 \cm
  bottom-margin = 0.5 \cm
  left-margin = 0.5 \cm
  right-margin = 0.5 \cm
  top-system-spacing = ##f
}


text = \lyricmode {
  \markup \with-color #grey  \sans \fontsize #-7 {" "}
}

textB = \markup \with-color #grey  \sans \fontsize #-5 {Middle C (C4)}

smallsize = #-4

\layout {
  % ragged-right = ##t
  indent = #0
  line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 1)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    forceClef = ##t
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
}

\markup {
  \justify-line {

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "a" {
              \clef treble
              s16
            }
          }
          \new TradStaff {
            \new Voice = "b" {
              \clef french
              s16
            }
          }
          % \new Lyrics \lyricsto "b" \text
        >>

      }
      \vspace #1
      \rotate #90 \with-color #grey \sans \fontsize #smallsize "French"
      % \with-color #grey  \sans \fontsize #smallsize "E1"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "one" {
              \clef "treble"
              s16
            }
          }
          \new TradStaff {
            \new Voice = "two" {
              \clef "treble"
              s16
            }
          }
          % \new Lyrics \lyricsto "two" \text
        >>

      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90 "Treble"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "three" {
              \clef treble
              s16
            }
          }
          \new TradStaff {
            \new Voice = "four" {
              \clef soprano
              s16
            }
          }
          % \new Lyrics \lyricsto "four" \text
        >>
      }
      \vspace #1
      \with-color #grey \sans \fontsize #smallsize \rotate #90 "Soprano"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "five" {
              \clef alto
              s16
            }
          }
          \new TradStaff {
            \new Voice = "six" {
              \clef mezzosoprano
              s16
            }
          }
          % \new Lyrics \lyricsto "six" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90  "Mezzosoprano"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "seven" {
              \clef alto
              s16
            }
          }
          \new TradStaff {
            \new Voice = "eight" {
              \clef alto
              s16
            }
          }
          %  \new Lyrics \lyricsto "eight" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90 "Alto"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "nine" {
              \clef alto
              s16
            }
          }
          \new TradStaff {
            \new Voice = "ten" {
              \clef tenor
              s16
            }
          }
          % \new Lyrics \lyricsto "ten" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90  "Tenor"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "d" {
              \clef bass
              s16
            }
          }
          \new TradStaff {
            \new Voice = "e" {
              \clef baritone
              s16
            }
          }
          % \new Lyrics \lyricsto "e" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90  "Baritone"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "d" {
              \clef bass
              s16
            }
          }
          \new TradStaff {
            \new Voice = "e" {
              \clef varbaritone
              s16
            }
          }
          % \new Lyrics \lyricsto "e" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90  "Varbaritone"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "d" {
              \clef bass
              s16
            }
          }
          \new TradStaff {
            \new Voice = "e" {
              \clef bass
              s16
            }
          }
          % \new Lyrics \lyricsto "e" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90  "Bass"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "d" {
              \clef bass
              s16
            }
          }
          \new TradStaff {
            \new Voice = "e" {
              \clef subbass
              s16
            }
          }
          % \new Lyrics \lyricsto "e" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90  "Subbass"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "d" {
              \clef percussion
              s16
            }
          }
          \new TradStaff {
            \new Voice = "e" {
              \clef percussion
              s16
            }
          }
          % \new Lyrics \lyricsto "e" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize \rotate #90  "Percussion"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

  }
}