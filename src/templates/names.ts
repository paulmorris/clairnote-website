// Resolve author slugs with full author names.
export const namesMap: Record<string, string> = {
  paulmorris: "Paul Morris",
  andres_megias: "Andrés Megías",
};
