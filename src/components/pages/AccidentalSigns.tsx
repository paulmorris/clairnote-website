import React from "react";
import { Link } from "gatsby";
import Layout from "../parts/Layout";
import { PageComponentProps } from "../../types/types";
import { NextPageLink } from "../parts/NextPageLink";

// @ts-ignore
import AccidentalsGDn from "../../images/svg-dn/Clairnote-accidentals-G.svg";
// @ts-ignore
import AccidentalsGSn from "../../images/svg-sn/Clairnote-accidentals-G.svg";

// @ts-ignore
import AccidentalsGABDn from "../../images/svg-dn/Clairnote-accidentals-G-A-B.svg";
// @ts-ignore
import AccidentalsGABSn from "../../images/svg-sn/Clairnote-accidentals-G-A-B.svg";

// @ts-ignore
import AccidentalsFewerGGsADn from "../../images/svg-dn/Clairnote-accidentals-fewer-G-Gs-A.svg";
// @ts-ignore
import AccidentalsFewerGGsASn from "../../images/svg-sn/Clairnote-accidentals-fewer-G-Gs-A.svg";

// @ts-ignore
import AccidentalsFewerEChordsDn from "../../images/svg-dn/Clairnote-accidentals-fewer-E-chords.svg";
// @ts-ignore
import AccidentalsFewerEChordsSn from "../../images/svg-sn/Clairnote-accidentals-fewer-E-chords.svg";

// @ts-ignore
import AccidentalsNaturalsDn from "../../images/svg-dn/Clairnote-accidentals-naturals.svg";
// @ts-ignore
import AccidentalsNaturalsSn from "../../images/svg-sn/Clairnote-accidentals-naturals.svg";

// @ts-ignore
import AccidentalsDoubleSharpsFlatsDn from "../../images/svg-dn/Clairnote-accidentals-double-sharps-flats.svg";
// @ts-ignore
import AccidentalsDoubleSharpsFlatsSn from "../../images/svg-sn/Clairnote-accidentals-double-sharps-flats.svg";

// @ts-ignore
import AccidentalsChromaticScaleDn from "../../images/svg-dn/Clairnote-accidentals-chromatic-scale.svg";
// @ts-ignore
import AccidentalsChromaticScaleSn from "../../images/svg-sn/Clairnote-accidentals-chromatic-scale.svg";

import * as pageStyles from "../global-styles/page.module.css";

export const AccidentalSigns = (props: PageComponentProps) => {
  const { clairnoteName, dn, urlDir } = props;

  const title = "Accidental Signs";

  const description = `Discussion and illustration of accidental signs in ${clairnoteName} music notation, where they simply indicate the note's traditional name and that it is an accidental.`;

  return (
    <Layout title={title} description={description} {...props}>
      <article className="page">
        <h1 className={`title is-3 ${pageStyles.pageTitle}`}>{title}</h1>
        <div>
          <p>
            {clairnoteName} has accidental signs that are similar to traditional
            accidental signs but they work a little bit differently. Much like
            traditional accidental signs they indicate:
          </p>
          <ul>
            <li>That a note is an accidental (i.e. not in the current key).</li>
            <li>
              The note's name, whether it is a sharp, flat, natural, double
              sharp, double flat, etc.
            </li>
          </ul>
          <p>
            The difference is that unlike traditional accidental signs they do
            not indicate that the following note(s) should be played a semitone
            higher or lower (an essential instruction the musician must follow
            to play the right notes). Instead they effectively indicate that the
            following note has <em>already</em> been sharpened or flattened.
            They communicate the names of accidental notes but do not
            (typically) affect their pitch.
            <sup id="ftnref1" className={pageStyles.footnoteRef}>
              <a href="#ftn1">[1]</a>
            </sup>{" "}
            As such they play a very minimal role when it comes to reading and
            playing notes. For example, a piano or guitar player could ignore
            the accidental signs and still play the correct notes.
          </p>
          <p>Here is an illustration of accidental signs in both systems:</p>
          <div className={pageStyles.imageContainer}>
            <img
              src={dn ? AccidentalsGDn : AccidentalsGSn}
              alt={
                "Sharp and Flat Accidentals in " +
                clairnoteName +
                " music notation"
              }
            />
          </div>
          <p>
            Since {clairnoteName}
            's sharp and flat signs have a different meaning than traditional
            sharp and flat signs, new symbols are used — simple
            "matchstick-style" symbols pointing up for sharp or down for flat.
            These symbols are more visually subtle and take up less horizontal
            space than traditional accidental symbols.
          </p>
          <div className={pageStyles.imageContainer}>
            <img
              src={dn ? AccidentalsGABDn : AccidentalsGABSn}
              alt={
                "Enharmonic accidentals in " + clairnoteName + " music notation"
              }
            />
          </div>
          <p>
            Above is an illustration of two pairs of enharmonically equivalent
            notes — notes like the black keys on a piano that have different
            names but have the same pitch (in 12-tone equal temperament
            <sup id="ftnref1" className={pageStyles.footnoteRef}>
              <a href="#ftn1">[1]</a>
            </sup>
            ). In {clairnoteName} it is much clearer that they have the same
            pitch. The alternative accidental signs simply indicate their
            different names and do not affect their pitch. (Differentiating
            between enharmonic equivalents using {clairnoteName}
            's accidental signs and{" "}
            <Link to={urlDir + "key-signatures/"}>Key Signatures</Link> makes it
            possible to use the standard{" "}
            <Link to={urlDir + "names-of-notes-and-intervals/"}>
              Names of Notes and Intervals
            </Link>
            .
            <sup id="ftnref2" className={pageStyles.footnoteRef}>
              <a href="#ftn2">[2]</a>
            </sup>
            )
          </p>
          <p>
            {clairnoteName}
            's accidental signs remain in effect until the end of the measure in
            which they appear, just like traditional accidental signs. Fewer
            accidental signs are needed because there are twelve staff positions
            per octave instead of seven. The following measure illustrates this
            point with its repeating melodic pattern of G, G-sharp, and A, where
            only the first G-sharp gets an accidental sign. Notice how{" "}
            {clairnoteName} uses less horizontal space because it requires fewer
            accidental signs.
          </p>
          <div className={pageStyles.imageContainer}>
            <img
              src={dn ? AccidentalsFewerGGsADn : AccidentalsFewerGGsASn}
              alt={"Melody showing fewer accidental signs in " + clairnoteName}
            />
          </div>
          <p>
            Below is a similar illustration showing alternating E-minor and
            E-major chords (triads in{" "}
            <a
              href="https://en.wikipedia.org/wiki/Inversion_%28music%29"
              target="_blank"
              rel="noopener noreferrer"
            >
              second inversion
            </a>
            ).
          </p>
          <div className={pageStyles.imageContainer}>
            <img
              src={dn ? AccidentalsFewerEChordsDn : AccidentalsFewerEChordsSn}
              alt={"Chords showing fewer accidental signs in " + clairnoteName}
            />
          </div>
          <p>
            Traditional notation often requires accidental signs that cancel
            previous ones in a given measure, but this is very rare in Clairnote
            SN. For it to happen there would have to be two different
            enharmonically equivalent notes in the same measure. For example,
            F-sharp and G-flat, or E and F-flat.
          </p>
          <p>
            {clairnoteName} has natural signs that indicate natural notes that
            are outside of the current key, just as in traditional notation. For
            example, an F natural note in the key of G major (where F is sharp)
            would have a natural sign, as shown below.
            <sup id="ftnref3" className={pageStyles.footnoteRef}>
              <a href="#ftn3">[3]</a>
            </sup>{" "}
            (See also {clairnoteName}
            's <Link to={urlDir + "key-signatures/"}>Key Signatures</Link>
            .)
          </p>
          <div className={pageStyles.imageContainer}>
            <img
              src={dn ? AccidentalsNaturalsDn : AccidentalsNaturalsSn}
              alt={"Natural signs in " + clairnoteName + " music notation"}
            />
          </div>
          <p>
            Double sharps and double flats are indicated by doubled sharp and
            flat symbols.
          </p>
          <div className={pageStyles.imageContainer}>
            <img
              src={
                dn
                  ? AccidentalsDoubleSharpsFlatsDn
                  : AccidentalsDoubleSharpsFlatsSn
              }
              alt={
                "Double sharps and double flats in " +
                clairnoteName +
                " music notation"
              }
            />
          </div>
          <p>
            To give a taste of {clairnoteName}
            's accidental signs in a more musical context, here is an
            illustration of an ascending and descending chromatic scale.
          </p>
          <div className={pageStyles.imageContainer}>
            <img
              src={
                dn ? AccidentalsChromaticScaleDn : AccidentalsChromaticScaleSn
              }
              alt={
                "Ascending chromatic scale in " +
                clairnoteName +
                " with accidentals"
              }
            />
          </div>
          <NextPageLink
            title="Names of Notes and Intervals"
            to={urlDir + "names-of-notes-and-intervals/"}
          />
          <div className={pageStyles.footnotes}>
            <ol>
              <li id="ftn1">
                This assumes the standard tuning system of 12-tone equal
                temperament. In other less commonly used tuning systems the
                pitch or intonation of "enharmonically equivalent" notes may
                differ slightly. In that case {clairnoteName}'s accidental signs
                and key signatures indicate these subtle shifts in
                pitch/intonation, as well as the different names of the notes.
                See the{" "}
                <a
                  href="http://musicnotation.org/tutorials/enharmonic-equivalents/"
                  target="_blank"
                  rel="external noopener noreferrer"
                >
                  Enharmonic Equivalents
                </a>{" "}
                tutorial on the Music Notation Project's site.{" "}
                <a className={pageStyles.footnoteReturn} href="#ftnref1">
                  Return
                </a>
              </li>
              <li id="ftn2">
                Even if someone has no need to distinguish enharmonic
                equivalents and uses an{" "}
                <a
                  href="http://musicnotation.org/wiki/nomenclature/nomenclatures-overview/"
                  target="_blank"
                  rel="external noopener noreferrer"
                >
                  alternative note naming system
                </a>{" "}
                that does not differentiate between them, {clairnoteName}'s
                accidental signs are still useful since they indicate which
                notes are outside of the current key. This reassures musicians
                that they are playing the correct note, even if it may sound out
                of place. However, musicians can always just ignore them or even
                omit them, for example if they are creating sheet music with{" "}
                <Link title="Software: LilyPond" to={urlDir + "software/"}>
                  LilyPond
                </Link>
                . Omitting them may be especially appropriate for use with
                atonal or non-diatonic music.{" "}
                <a className={pageStyles.footnoteReturn} href="#ftnref2">
                  Return
                </a>
              </li>
              <li id="ftn3">
                A different approach would be to use the same "matchstick"
                accidental symbols for natural notes. For example, indicating
                that a note was raised from B-flat (in the key signature) to a
                B-natural (as an accidental outside the key) by using an
                upwards-pointing matchstick accidental sign rather than a
                natural sign. This would more directly indicate that such
                accidental notes have been lowered or raised. However, it makes
                it harder to determine the names of accidental notes since there
                would no longer be a one-to-one correspondence between the name
                of a note and its accidental sign. Since {clairnoteName}
                's accidental signs are largely about communicating the names of
                notes, it uses natural signs to indicate natural notes. Thus the
                note's name (sharp, flat, natural) is always directly indicated
                by the accidental sign.{" "}
                <a className={pageStyles.footnoteReturn} href="#ftnref3">
                  Return
                </a>
              </li>
            </ol>
          </div>
        </div>
      </article>
    </Layout>
  );
};
