import React from "react";
import { observer } from "mobx-react";
// import DevTools from "mobx-react-devtools";

import * as styles from "./sheet-music.module.css";
import { SheetMusicStore } from "./SheetMusicStore";

export const Pagination = observer(function Pagination({
  store,
  navigateOnParamChange,
}: {
  store: SheetMusicStore;
  navigateOnParamChange: (disableScrollUpdate?: boolean) => void;
}) {
  const pageNumber = store.state.pageNumber;

  const navigateToNewPage = (pageNumber: number) => {
    store.setPageNumber(pageNumber);
    navigateOnParamChange(false);
    // Not needed because the navigate call clears the scroll position.
    // window.scrollTo(0, 0);
  };

  return (
    <ul>
      {pageNumber > 1 && (
        <li className={styles.paginationItem}>
          <button
            className={styles.paginationLink}
            onClick={() => navigateToNewPage(pageNumber - 1)}
          >
            Previous
          </button>
        </li>
      )}

      {store.paginationNumbers.map((paginationNumber, index) => (
        <li className={styles.paginationItem} key={index}>
          {paginationNumber === pageNumber ? (
            <button className={styles.paginationLinkCurrent}>
              {paginationNumber}
            </button>
          ) : (
            <button
              className={styles.paginationLink}
              onClick={() => navigateToNewPage(paginationNumber)}
            >
              {paginationNumber}
            </button>
          )}
        </li>
      ))}

      {pageNumber !== store.paginationNumbers.slice(-1)[0] && (
        <li className={styles.paginationItem}>
          <button
            className={styles.paginationLink}
            onClick={() => navigateToNewPage(pageNumber + 1)}
          >
            Next
          </button>
        </li>
      )}
    </ul>
  );
});
