import React, { useEffect, useState } from "react";
import { Link, navigate } from "gatsby";
import Layout from "../../parts/Layout";
import { observer } from "mobx-react";
// import DevTools from "mobx-react-devtools";

import { Collection, SheetMusicStore } from "./SheetMusicStore";
import {
  FilterSelect,
  mutopiaInstruments,
  mutopiaStyles,
  mutopiaComposersOptions,
  sessionMeters,
} from "./filters";
import { SearchResults } from "./results";
import { Pagination } from "./pagination";
import { PageComponentProps, SelectOption } from "../../../types/types";
import { makeSelectOptions } from "../../../js/utils";

import * as styles from "./sheet-music.module.css";
import * as pageStyles from "../../global-styles/page.module.css";
// import * as utilStyles from "../../global-styles/util.module.css";

export default observer(function SheetMusic(props: PageComponentProps) {
  const { urlDir } = props;
  const params = new URLSearchParams(props.location.search);
  const [store] = useState(new SheetMusicStore(params));

  const title = "Sheet Music";
  const description = `A collection of sheet music in ${props.clairnoteName} music notation. Free to download, print, copy, modify, perform, etc. Thanks to the Mutopia Project, the Session, and LilyPond.`;

  const allLabel = `All (${store.resultIdsAll.length})`;
  const mutopiaLabel = `Mutopia Project Collection (${store.resultIdsMutopia.length})`;
  const sessionLabel = `The Session Collection (${store.resultIdsSession.length})`;
  const miscLabel = `Miscellaneous Collection (${store.resultIdsMisc.length})`;

  const collectionOptions: SelectOption<Collection>[] = [
    { label: allLabel, value: "all" },
    { label: mutopiaLabel, value: "mutopia" },
    { label: sessionLabel, value: "session" },
    { label: miscLabel, value: "misc" },
  ];

  const navigateOnParamChange = (disableScrollUpdate = true) => {
    const params = String(store.urlParams);

    const path = params.length
      ? `${location.pathname}?${params}`
      : location.pathname;

    navigate(path, {
      replace: true,
      state: { disableScrollUpdate },
    });
  };

  const handleSearchChange = (query: string) => {
    store.setSearchInput(query);
    navigateOnParamChange();
  };

  const handleCollectionChange = (collection: Collection) => {
    store.setCollection(collection);
    navigateOnParamChange();
  };

  return (
    <Layout title={title} description={description} {...props}>
      <article className="page">
        <h1 className={`title is-3 ${pageStyles.pageTitle}`}>{title}</h1>
        <div>
          <div className={styles.sheetMusicWrapper}>
            <div>
              <div className="field has-addons">
                <div className="control is-expanded">
                  <input
                    id="searchBox"
                    className="input"
                    type="text"
                    placeholder="Search for sheet music"
                    name="query"
                    value={store.state.searchInput}
                    onChange={(e) => handleSearchChange(e.target.value)}
                  />
                </div>
                <div className="control">
                  <button
                    className="button"
                    id="search-button"
                    value="Clear"
                    onClick={() => handleSearchChange("")}
                  >
                    ✕ {/* &#x2715 */}
                  </button>
                </div>
              </div>

              <div className={styles.collectionSelectMenuContainer}>
                <div className="select is-small">
                  <select
                    title="Filter by Collection"
                    onChange={(event) => {
                      const value = event.target.value;
                      // TypeScript can be stupid sometimes.
                      for (const entry of collectionOptions) {
                        if (entry.value === value) {
                          handleCollectionChange(value);
                        }
                      }
                    }}
                    value={store.state.collection}
                  >
                    {makeSelectOptions(collectionOptions)}
                  </select>
                </div>
              </div>
            </div>

            <div className={styles.searchFilterUi}>
              {store.state.collection === "all" && (
                <>
                  <div>
                    Sheet music from various sources converted to{" "}
                    {props.clairnoteName} with LilyPond. See{" "}
                    <Link to={urlDir + "about-sheet-music"}>
                      About Sheet Music
                    </Link>
                    .
                  </div>
                  <div className={styles.collectionDescription} />
                </>
              )}
              {store.state.collection === "mutopia" && (
                <>
                  <div>
                    <FilterSelect
                      name="Filter by Instrument"
                      title="Filter by Instrument"
                      filterSetter={(value) => {
                        store.setFilterInstrument(value);
                        navigateOnParamChange();
                      }}
                      defaultValue={store.state.filterInstrument}
                      options={mutopiaInstruments}
                    />
                    <FilterSelect
                      name="Filter by Style"
                      title="Filter by Style"
                      filterSetter={(value) => {
                        store.setFilterStyle(value);
                        navigateOnParamChange();
                      }}
                      defaultValue={store.state.filterStyle}
                      options={mutopiaStyles}
                    />
                    <FilterSelect
                      name="Filter by Composer"
                      title="Filter by Composer"
                      filterSetter={(value) => {
                        store.setFilterComposer(value);
                        navigateOnParamChange();
                      }}
                      defaultValue={store.state.filterComposer}
                      options={mutopiaComposersOptions}
                    />
                  </div>
                  <div className={styles.collectionDescription}>
                    Sheet music from{" "}
                    <a
                      href={store.currentCollectionUrl}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {store.currentCollectionName}
                    </a>
                    , converted to {props.clairnoteName} with LilyPond. See{" "}
                    <Link to={urlDir + "about-sheet-music"}>
                      About Sheet Music
                    </Link>
                    . Filter search results by instrument, style, and/or
                    composer.
                  </div>
                </>
              )}
              {store.state.collection === "session" && (
                <>
                  <div>
                    <FilterSelect
                      name="Filter by Meter"
                      title="Filter by Meter"
                      filterSetter={(val) => {
                        store.setFilterSessionMeter(val);
                        navigateOnParamChange();
                      }}
                      defaultValue={store.state.filterSessionMeter}
                      options={sessionMeters}
                    />
                  </div>
                  <div className={styles.collectionDescription}>
                    Sheet music from{" "}
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={store.currentCollectionUrl}
                    >
                      {store.currentCollectionName}
                    </a>
                    , converted to {props.clairnoteName} with LilyPond. See{" "}
                    <Link to={urlDir + "about-sheet-music"}>
                      About Sheet Music
                    </Link>
                    . Filter search results by meter.
                  </div>
                </>
              )}
              {store.state.collection === "misc" && (
                <>
                  <div className={styles.collectionDescription}>
                    Sheet music from various sources converted to{" "}
                    {props.clairnoteName} with LilyPond. See{" "}
                    <Link to={urlDir + "about-sheet-music"}>
                      About Sheet Music
                    </Link>
                    .
                  </div>
                </>
              )}
              <div>
                {`${store.currentResultIds.length} result${
                  store.currentResultIds.length === 1 ? "" : "s"
                }`}
                {store.pagesCount > 1 &&
                  `, page ${store.state.pageNumber} of ${store.pagesCount}`}
              </div>
            </div>
            <SearchResults
              store={store}
              clairnoteType={props.dn ? "dn" : "sn"}
            />
            {store.paginationNumbers.length > 1 && (
              <Pagination
                store={store}
                navigateOnParamChange={navigateOnParamChange}
              />
            )}
          </div>
        </div>
      </article>
    </Layout>
  );
});
