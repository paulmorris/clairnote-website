import { observable, action, computed, configure, makeObservable } from "mobx";
import lunr from "lunr";
import { debounce } from "lodash";

import { iota, transformSearchQuery } from "../../../js/utils";

// @ts-ignore
import { mutopiaItems } from "./data-mutopia-items";
import { mutopiaIdsSorted } from "./data-mutopia-ids-sorted";

// @ts-ignore
import { sessionItems } from "./data-session-items";
import { sessionIdsSorted } from "./data-session-ids-sorted";

import { miscIdsSorted } from "./data-misc-ids-sorted";

import {
  mutopiaComposersMap,
  mutopiaInstrumentsMap,
  mutopiaStylesMap,
  sessionMetersMap,
} from "./filters";

import {
  addIdPrefixMisc,
  addIdPrefixMutopia,
  addIdPrefixSession,
  idPrefixIsMisc,
  idPrefixIsMutopia,
  idPrefixIsSession,
  removeIdPrefix,
} from "./utils";

// @ts-ignore
import { indexAll } from "./index-all";

const indexAllLoaded = lunr.Index.load(indexAll);

configure({ enforceActions: "observed" });

export type Collection = "all" | "mutopia" | "session" | "misc";

interface SheetMusicStoreState {
  collection: Collection;
  searchInput: string;
  searchQuery: string;
  resultsPerPage: number;
  pageNumber: number;
  filterInstrument: string;
  filterComposer: string;
  filterStyle: string;
  filterSessionMeter: string;
}

export class SheetMusicStore {
  // @observable
  state: SheetMusicStoreState = {
    collection: "all",
    searchInput: "",
    searchQuery: "",
    resultsPerPage: 10,
    pageNumber: 1,
    filterInstrument: "any-instrument",
    filterComposer: "any-composer",
    filterStyle: "any-style",
    filterSessionMeter: "any-meter",
  };

  constructor(urlParams?: URLSearchParams) {
    makeObservable(this, {
      state: observable,
      resultIdsAll: computed,
      resultIdsMutopia: computed,
      resultIdsSession: computed,
      resultIdsMisc: computed,
      currentResultIds: computed,
      resultIdsToShow: computed,
      pagesCount: computed,
      paginationNumbers: computed,
      currentCollectionName: computed,
      currentCollectionUrl: computed,
      urlParams: computed,

      setCollection: action,
      setSearchInput: action,
      // TODO: mobx devtools warnings suggest that setSearchQueryDebounced is
      // not actually an action, but things are working.
      setSearchQuery: action,
      setSearchQueryDebounced: action,
      setFilterInstrument: action,
      setFilterComposer: action,
      setFilterStyle: action,
      setFilterSessionMeter: action,
      setPageNumber: action,
    });

    if (urlParams) {
      const q = urlParams.get("q");
      if (q) {
        this.setSearchInput(q);
        // Directly set the search query so there's no flash of results
        // changing on page load.
        this.setSearchQuery(q);
      }
      const collection = urlParams.get("collection");
      if (
        collection === "mutopia" ||
        collection === "session" ||
        collection === "misc"
      ) {
        this.setCollection(collection);
      }
      if (collection === "mutopia") {
        const instrument = urlParams.get("instrument");
        const style = urlParams.get("style");
        const composer = urlParams.get("composer");
        if (instrument) {
          this.setFilterInstrument(instrument);
        }
        if (style) {
          this.setFilterStyle(style);
        }
        if (composer) {
          this.setFilterComposer(composer);
        }
      }
      if (collection === "session") {
        const meter = urlParams.get("meter");
        if (meter) {
          this.setFilterSessionMeter(meter);
        }
      }
      // The parsed page number could be NaN which is falsy.
      const page = parseInt(urlParams.get("page") || "1", 10);
      if (page && page > 1) {
        this.setPageNumber(page);
      }
    }
  }

  /**
   * Array of IDs of search results for all the sheet music.
   *
   * @see https://css-tricks.com/how-to-add-lunr-search-to-your-gatsby-website/
   */
  get resultIdsAll() {
    // Avoid issues with 'this'.
    const queryTerms = this.state.searchQuery
      ? this.state.searchQuery.split(" ")
      : [];

    if (queryTerms.length === 0) {
      // All results (browse mode).
      return [
        // TODO: convert to strings before runtime.
        ...mutopiaIdsSorted.map((id) => addIdPrefixMutopia(String(id))),
        ...sessionIdsSorted.map((id) => addIdPrefixSession(String(id))),
        ...miscIdsSorted.map(addIdPrefixMisc),
      ];
    }

    const wildcardOptions = {
      wildcard: lunr.Query.wildcard.LEADING | lunr.Query.wildcard.TRAILING,
    };

    // Do an "all term" search that uses "or" logic.
    // The search includes both full terms (with fuzzy matching for longer
    // terms), and wildcard terms.
    const allTermResultIds: string[] = indexAllLoaded
      .query((q) => {
        queryTerms.forEach((queryTerm) => {
          q.term(queryTerm, { editDistance: queryTerm.length > 5 ? 1 : 0 });
          q.term(queryTerm, wildcardOptions);
        });
      })
      .map((item) => item.ref);

    if (queryTerms.length === 1) {
      // If there's only one search term we are already done.
      return allTermResultIds;
    }

    // Do a "single term" search for each term.
    const singleTermResultIdSets: Set<string>[] = queryTerms.map(
      (queryTerm) => {
        const results = indexAllLoaded.query((q) => {
          q.term(queryTerm, { editDistance: queryTerm.length > 5 ? 1 : 0 });
          q.term(queryTerm, wildcardOptions);
        });
        return new Set(results.map((item) => item.ref));
      }
    );

    // Return the intersection ("and" logic) of each single term search,
    // in the same order as the all term search results.
    const intersectionIds: string[] = allTermResultIds.filter((item) => {
      return singleTermResultIdSets.every((set) => set.has(item));
    });

    return intersectionIds;
  }

  // @computed
  get resultIdsMutopia() {
    const ids1 = this.resultIdsAll.filter(idPrefixIsMutopia);

    const instrumentLabel = mutopiaInstrumentsMap.get(
      this.state.filterInstrument
    );
    const ids2 =
      !instrumentLabel || this.state.filterInstrument === "any-instrument"
        ? ids1
        : ids1.filter((id) =>
            mutopiaItems[removeIdPrefix(id)][3].includes(instrumentLabel)
          );

    const styleLabel = mutopiaStylesMap.get(this.state.filterStyle);
    const ids3 =
      !styleLabel || this.state.filterStyle === "any-style"
        ? ids2
        : ids2.filter(
            (id) => mutopiaItems[removeIdPrefix(id)][0] === styleLabel
          );

    const composerMutopiaId = mutopiaComposersMap.get(
      this.state.filterComposer
    );
    const ids4 =
      !composerMutopiaId || this.state.filterComposer === "any-composer"
        ? ids3
        : ids3.filter(
            (id) => mutopiaItems[removeIdPrefix(id)][1] === composerMutopiaId
          );
    return ids4;
  }

  // @computed
  get resultIdsSession() {
    const ids1 = this.resultIdsAll.filter(idPrefixIsSession);
    const meterLabel = sessionMetersMap.get(this.state.filterSessionMeter);

    return !meterLabel || this.state.filterSessionMeter === "any-meter"
      ? ids1
      : ids1.filter((id) => sessionItems[removeIdPrefix(id)][1] === meterLabel);
  }

  // @computed
  get resultIdsMisc() {
    const ids = this.resultIdsAll.filter(idPrefixIsMisc);
    return ids;
  }

  // @computed
  get currentResultIds() {
    return this.state.collection === "all"
      ? this.resultIdsAll
      : this.state.collection === "mutopia"
      ? this.resultIdsMutopia
      : this.state.collection === "session"
      ? this.resultIdsSession
      : this.state.collection === "misc"
      ? this.resultIdsMisc
      : // Should never happen.
        [];
  }

  // @computed
  get resultIdsToShow() {
    const to = this.state.pageNumber * this.state.resultsPerPage;
    const from = to - this.state.resultsPerPage;
    const ids = this.currentResultIds.slice(from, to);
    return ids;
  }

  // @computed
  get pagesCount() {
    const totalResults = this.currentResultIds.length;
    const pagesCount = Math.ceil(totalResults / this.state.resultsPerPage);
    return pagesCount;
  }

  // @computed
  get paginationNumbers() {
    const current = this.state.pageNumber;
    const pageCount = this.pagesCount;

    const numbers =
      current < 7
        ? // near start
          iota(pageCount < 10 ? pageCount : 10, 1)
        : pageCount - 5 < current
        ? // near end
          iota(10, pageCount - 9)
        : // in middle
          iota(10, current - 5);
    return numbers;
  }

  // @computed
  get currentCollectionName() {
    const collection = this.state.collection;
    return collection === "mutopia"
      ? "the Mutopia Project"
      : collection === "session"
      ? "the Session"
      : "";
  }

  // @computed
  get currentCollectionUrl() {
    const collection = this.state.collection;
    return collection === "mutopia"
      ? "http://www.mutopiaproject.org/"
      : collection === "session"
      ? "https://thesession.org/"
      : undefined;
  }

  get urlParams() {
    const params = new URLSearchParams();

    const q = transformSearchQuery(this.state.searchInput);
    if (q) {
      params.set("q", q);
    }

    const collection = this.state.collection;
    if (collection !== "all") {
      params.set("collection", collection);
    }

    if (collection === "mutopia") {
      const style = this.state.filterStyle;
      const instrument = this.state.filterInstrument;
      const composer = this.state.filterComposer;
      if (instrument && instrument !== "any-instrument") {
        params.set("instrument", instrument);
      }
      if (style && style !== "any-style") {
        params.set("style", style);
      }
      if (composer && composer !== "any-composer") {
        params.set("composer", composer);
      }
    }
    if (collection === "session") {
      const meter = this.state.filterSessionMeter;
      if (meter && meter !== "any-meter") {
        params.set("meter", meter);
      }
    }
    const page = this.state.pageNumber;
    if (page && page > 1) {
      params.set("page", String(page));
    }
    return params;
  }

  // ACTIONS

  /**
   * Sets the content of the search input field.
   */
  setSearchInput(query: string) {
    this.state.searchInput = query;
    this.setSearchQueryDebounced(query);
  }
  /**
   * Transforms the input into a search query for use with lunr.
   */
  setSearchQuery(queryInput: string) {
    this.setPageNumber(1);
    // Remove leading and trailing spaces.
    const query = queryInput
      .trim()
      // Remove any wildcard characters.
      .replace(/\*/g, "")
      .toLowerCase()
      // Split by whitespaces which removes all whitespace.
      .split(/\s+/)
      // Remove single letters but keep single digits (0-9).
      .filter((term) => term.length > 1 || !isNaN(parseInt(term)))
      // Re-assemble into a string so computed can easily do an equality check.
      .join(" ");

    this.state.searchQuery = query;
  }
  /**
   * Debounced setter for the search query so the user finishes typing before
   * the search is performed.
   */
  setSearchQueryDebounced = debounce(this.setSearchQuery, 500);

  setCollection(collection: Collection) {
    // Reset all filters when the collection changes.
    this.setFilterInstrument("any-instrument");
    this.setFilterComposer("any-composer");
    this.setFilterStyle("any-style");
    this.setFilterSessionMeter("any-meter");
    this.setPageNumber(1);
    this.state.collection = collection;
  }
  setFilterInstrument(value: string) {
    this.setPageNumber(1);
    this.state.filterInstrument = value;
  }
  setFilterComposer(value: string) {
    this.setPageNumber(1);
    this.state.filterComposer = value;
  }
  setFilterStyle(value: string) {
    this.setPageNumber(1);
    this.state.filterStyle = value;
  }
  setFilterSessionMeter(value: string) {
    this.setPageNumber(1);
    this.state.filterSessionMeter = value;
  }
  setPageNumber(pageNumber: number) {
    this.state.pageNumber = pageNumber;
  }
}
