import React from "react";
import Layout from "../parts/Layout";
import { Link } from "gatsby";
import { Game } from "../parts/Game/Game";

import { PageComponentProps } from "../../types/types";

import * as pageStyles from "../global-styles/page.module.css";

export const Learn = (props: PageComponentProps) => {
  const { dn, sn, clairnoteName, urlDir } = props;
  const title = "Learn";
  const description = `An interactive game for learning to read notes and intervals in ${props.clairnoteName} music notation. Like flash cards but better.`;
  return (
    <Layout title={title} description={description} {...props}>
      <article className="page">
        <h1 className={`title is-3 ${pageStyles.pageTitle}`}>{title}</h1>

        <Game sn={sn} />

        <p>
          Learn to read notes and intervals in {clairnoteName} by playing this
          simple game. Click the start button and notes will appear on the upper
          staff. Then play the notes one by one on the keyboard below the staff.
          This is a fun way to interactively practice reading the notes on the
          staff.
        </p>
        <p>
          In addition to the traditional keyboard you can also practice reading
          notes using a{" "}
          <a
            title="About 6-6 Janko-style Keyboard Layout"
            href="http://musicnotation.org/wiki/instruments/janko-keyboard"
            target="_blank"
          >
            Janko-style keyboard
          </a>
          , guitar, violin, or mandolin.
        </p>
        <p>
          For a new challenge, you can select a different set of notes or
          intervals by using the menus above the staves that begin with "Notes"
          and "1 Octave". You can also change the number of notes or intervals
          that are shown on the staff by using the "4 at a Time" menu.
        </p>

        <p>
          The score is the total number of correct responses minus the number of
          incorrect responses. If you hover your cursor over the score with your
          mouse, you can see the breakdown of correct and incorrect responses.
        </p>

        <p>
          The "Notes: Trumpet" game helps you learn trumpet fingerings, and it
          assumes that you are reading notes in concert pitch and playing them
          on a B-flat trumpet. (Note that this is not the usual way music for
          B-flat instruments is written in traditional music notation, where a
          B-flat note is written as a C.)
        </p>
        {dn && (
          <p>
            Have a MIDI keyboard or other MIDI instrument? Download the{" "}
            <Link to={urlDir + "notetrainer-application/"}>NoteTrainer</Link>
            application.
          </p>
        )}
      </article>
    </Layout>
  );
};
