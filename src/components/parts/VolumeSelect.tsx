import { observer } from "mobx-react";
import React from "react";
import { makeSelectOptions } from "../../js/utils";
import { SelectOption } from "../../types/types";
import { AudioVisualizerStore } from "./AudioVisualizer/AudioVisualizerStore";
import { GameStore } from "./Game/GameStore";

const volumeOptions: SelectOption<number>[] = [
  { value: 1.0, label: "Volume 4" },
  { value: 0.65, label: "Volume 3" },
  { value: 0.35, label: "Volume 2" },
  { value: 0.1, label: "Volume 1" },
  { value: 0, label: "No Sound" },
];

export const VolumeSelect = observer(function VolumeSelect({
  store,
  className,
}: {
  store: AudioVisualizerStore | GameStore;
  className?: string;
}) {
  return (
    <div className={"select is-small"}>
      <select
        className={className}
        name="Volume Selector"
        title="Volume"
        onChange={(event) => store.setVolume(parseFloat(event.target.value))}
        value={store.state.volume}
      >
        {makeSelectOptions(volumeOptions)}
      </select>
    </div>
  );
});
