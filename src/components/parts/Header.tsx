import React, { useState, Fragment, useMemo } from "react";
import { Link, withPrefix } from "gatsby";
import { PageLayoutProps } from "../../types/types";

import * as styles from "./Header.module.css";

type NavItem = {
  label: string;
  mobileLabel?: string;
  dnOnly?: boolean;
} & (
  | {
      // internal links
      slug: string;
    }
  | {
      // external links
      url: string;
    }
);

export const Header = (props: PageLayoutProps) => {
  const { urlDir, clairnoteName, otherClairnoteName, sn, dn, isBlog, path } =
    props;

  // Remove "/dn/" (if it's there) to get "base" path for both sn and dn sites.
  const basePath = path?.slice(dn ? 4 : 1);

  const [burgerMenuActive, setBurgerMenuActive] = useState(false);

  const nav: Record<string, { slug: string; subItems: NavItem[] }> = useMemo(
    () => ({
      about: {
        slug: "about/",
        subItems: [
          { slug: "staff/", label: "Staff" },
          { slug: "scales/", label: "Scales" },
          { slug: "intervals/", label: "Intervals" },
          { slug: "chords/", label: "Chords" },
          { slug: "clefs/", label: "Clefs" },
          { slug: "key-signatures/", label: "Key Signatures" },
          { slug: "accidental-signs/", label: "Accidental Signs" },
          {
            slug: "names-of-notes-and-intervals/",
            label: "Names",
            mobileLabel: "Names of Notes and Intervals",
          },
          {
            slug: "rhythm-notation/",
            label: "Rhythm",
            mobileLabel: "Rhythm Notation",
          },
          { slug: "faq/", label: "FAQ" },
          {
            slug: "clairnote-dn-clairnote-sn/",
            label: sn ? "DN" : "SN",
            mobileLabel: `${clairnoteName} and ${otherClairnoteName}`,
          },
        ],
      },
      learn: {
        slug: "learn/",
        subItems: [
          {
            slug: "notetrainer-application/",
            label: "NoteTrainer",
            dnOnly: true,
          },
        ],
      },
      sheetMusic: {
        slug: "sheet-music/",
        subItems: [
          {
            slug: "about-sheet-music/",
            label: "About",
            mobileLabel: "About Sheet Music",
          },
          { slug: "blank-staff-paper/", label: "Blank Staff Paper" },
          {
            slug: "handwritten-sheet-music/",
            label: "Handwritten",
            mobileLabel: "Handwritten Sheet Music",
            dnOnly: true,
          },
        ],
      },
      software: {
        slug: "software/",
        subItems: [
          {
            url: "https://lilybin.clairnote.org",
            label: "LilyBin + Clairnote",
          },
          {
            slug: "software-musescore/",
            label: "MuseScore",
            mobileLabel: "Software: MuseScore",
            dnOnly: true,
          },
        ],
      },
      community: {
        slug: "community/",
        subItems: [],
      },
      blog: {
        slug: "blog/",
        subItems: [],
      },
    }),
    [sn, clairnoteName, otherClairnoteName]
  );

  const reduceToSlug = (result: string[], item: NavItem) => {
    if ("slug" in item) {
      result.push(item.slug);
    }
    return result;
  };

  const slugs = useMemo(() => {
    return {
      about: new Set([
        nav.about.slug,
        ...nav.about.subItems.reduce(reduceToSlug, []),
      ]),
      learn: new Set([
        nav.learn.slug,
        ...nav.learn.subItems.reduce(reduceToSlug, []),
      ]),
      sheetMusic: new Set([
        nav.sheetMusic.slug,
        ...nav.sheetMusic.subItems.reduce(reduceToSlug, []),
      ]),
      software: new Set([
        nav.software.slug,
        ...nav.software.subItems.reduce(reduceToSlug, []),
      ]),
      community: new Set([
        nav.community.slug,
        ...nav.community.subItems.reduce(reduceToSlug, []),
      ]),
    };
  }, [nav]);

  /**
   * Returns an onClick handler function or undefined.
   *
   * If a user clicks a link in the mobile menu for the page that they are
   * already on, the menu should close, but it won't unless we do it in a click
   * handler. But we only want to manually close the menu if it's the same page.
   * (If it's a different page the menu will automatically close when the new
   * page loads.)
   *
   * The `basePath` is `undefined` for "blog/" nav link clicks when user is
   * on a blog page, so handle it as a special case.
   */
  const getCloseMenuFn = (slug: string): (() => void) | undefined => {
    const closeMenu =
      slug === basePath ||
      !!(isBlog && slug === "blog/" && props.location?.pathname === "/blog/");

    return closeMenu ? () => setBurgerMenuActive(false) : undefined;
  };

  /**
   * Used for the submenu items shown on small/mobile screens.
   */
  const navItemToMobileLink = (item: NavItem) => {
    if (sn && item.dnOnly) {
      return <Fragment key={item.label} />;
    }
    if ("url" in item) {
      return (
        <a
          className="navbar-item"
          href={item.url}
          target="_blank"
          onClick={() => setBurgerMenuActive(false)}
          key={item.label}
        >
          {item.mobileLabel || item.label}
        </a>
      );
    }
    return (
      <Link
        to={urlDir + item.slug}
        className="navbar-item"
        onClick={getCloseMenuFn(item.slug)}
        key={item.label}
      >
        {item.mobileLabel || item.label}
      </Link>
    );
  };

  /**
   * Used for the submenu items shown on large screens.
   */
  const navItemToLi = (item: NavItem) => {
    if (sn && item.dnOnly) {
      return <Fragment key={item.label} />;
    }
    if ("url" in item) {
      return (
        <li key={item.label}>
          <a href={item.url} target={"_blank"}>
            {item.label}
          </a>
        </li>
      );
    }
    return (
      <li key={item.label}>
        <Link
          to={urlDir + item.slug}
          className={basePath === item.slug ? styles.navTwoActive : ""}
        >
          {item.label}
        </Link>
      </li>
    );
  };

  return (
    <>
      <nav
        // Add `is-fixed-top` class here for navbar staying fixed at the top.
        className={`navbar ${sn ? "is-primary" : "is-info"}`}
        role="navigation"
        aria-label="main navigation"
      >
        <div className="container">
          {/* Home */}
          <div className={`navbar-brand ${styles.navOneLinkSpacing}`}>
            <Link
              className={`navbar-item ${styles.navOneLink}`}
              to={urlDir}
              title={clairnoteName + " Music Notation"}
              rel="home"
              onClick={getCloseMenuFn("")}
            >
              {clairnoteName}
            </Link>
            <a
              role="button"
              className={`navbar-burger ${burgerMenuActive ? "is-active" : ""}`}
              aria-label="menu"
              aria-expanded="false"
              data-target="navbarMain"
              onClick={() => setBurgerMenuActive(!burgerMenuActive)}
            >
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>

          <div
            id="navbarMain"
            className={`navbar-menu ${burgerMenuActive ? "is-active" : ""}`}
          >
            <div className="navbar-start">
              {/* About */}
              <div
                className={`navbar-item has-dropdown ${styles.navOneLinkSpacing} `}
              >
                <Link
                  to={urlDir + nav.about.slug}
                  className={
                    `navbar-link is-arrowless ${styles.navOneLink} ` +
                    (slugs.about.has(basePath) ? styles.navOneLinkActive : "")
                  }
                  onClick={getCloseMenuFn(nav.about.slug)}
                >
                  About
                </Link>
                <div className={`navbar-dropdown ${styles.navbarDropdown}`}>
                  {nav.about.subItems.map(navItemToMobileLink)}
                </div>
              </div>

              {/* Learn */}
              {sn && (
                <Link
                  to={urlDir + nav.learn.slug}
                  className={
                    `navbar-item ${styles.navOneLinkSpacing} ${styles.navOneLink} ${styles.navbarDropdown} ` +
                    (slugs.learn.has(basePath) ? styles.navOneLinkActive : "")
                  }
                  onClick={getCloseMenuFn(nav.learn.slug)}
                >
                  Learn
                </Link>
              )}
              {dn && (
                <div
                  className={`navbar-item has-dropdown ${styles.navOneLinkSpacing} `}
                >
                  <Link
                    to={urlDir + nav.learn.slug}
                    className={
                      `navbar-link is-arrowless ${styles.navOneLink} ` +
                      (slugs.learn.has(basePath) ? styles.navOneLinkActive : "")
                    }
                    onClick={getCloseMenuFn(nav.learn.slug)}
                  >
                    Learn
                  </Link>
                  <div className={`navbar-dropdown ${styles.navbarDropdown}`}>
                    {nav.learn.subItems.map(navItemToMobileLink)}
                  </div>
                </div>
              )}

              {/* Sheet Music */}
              <div
                className={`navbar-item has-dropdown ${styles.navOneLinkSpacing} `}
              >
                <Link
                  to={urlDir + nav.sheetMusic.slug}
                  className={
                    `navbar-link is-arrowless ${styles.navOneLink} ` +
                    (slugs.sheetMusic.has(basePath)
                      ? styles.navOneLinkActive
                      : "")
                  }
                  onClick={getCloseMenuFn(nav.sheetMusic.slug)}
                >
                  Sheet Music
                </Link>
                <div className={`navbar-dropdown ${styles.navbarDropdown}`}>
                  {nav.sheetMusic.subItems.map(navItemToMobileLink)}
                </div>
              </div>

              {/* Software */}
              <div
                className={`navbar-item has-dropdown ${styles.navOneLinkSpacing} `}
              >
                <Link
                  to={urlDir + nav.software.slug}
                  className={
                    `navbar-link is-arrowless ${styles.navOneLink} ` +
                    (slugs.software.has(basePath)
                      ? styles.navOneLinkActive
                      : "")
                  }
                  onClick={getCloseMenuFn(nav.software.slug)}
                >
                  Software
                  <span className={`${styles.navOneLinkExtraText}`}>
                    : LilyPond
                  </span>
                </Link>
                <div className={`navbar-dropdown ${styles.navbarDropdown}`}>
                  {nav.software.subItems.map(navItemToMobileLink)}
                </div>
              </div>

              {/* Community */}
              <Link
                to={urlDir + nav.community.slug}
                // className={`navbar-item ${styles.navOneLinkSpacing} ${styles.navOneLink} ${styles.}`}
                className={
                  `navbar-item ${styles.navOneLinkSpacing} ${styles.navOneLink} ${styles.navbarDropdown} ` +
                  (slugs.community.has(basePath) ? styles.navOneLinkActive : "")
                }
                onClick={getCloseMenuFn(nav.community.slug)}
              >
                Community
              </Link>

              {/* Blog */}
              <Link
                to={withPrefix(nav.blog.slug)}
                className={
                  `navbar-item ${styles.navOneLinkSpacing} ${styles.navOneLink} ${styles.navbarDropdown} ` +
                  (isBlog ? styles.navOneLinkActive : "")
                }
                style={{ borderBottom: "0px" }}
                onClick={getCloseMenuFn(nav.blog.slug)}
              >
                Blog
              </Link>
            </div>
          </div>
        </div>
      </nav>

      {slugs.about.has(basePath) && (
        <div className={`tabs is-centered ${styles.navTwo}`}>
          <ul>{nav.about.subItems.map(navItemToLi)}</ul>
        </div>
      )}

      {dn && slugs.learn.has(basePath) && (
        <div className={`tabs is-centered ${styles.navTwo}`}>
          <ul>{nav.learn.subItems.map(navItemToLi)}</ul>
        </div>
      )}

      {slugs.sheetMusic.has(basePath) && (
        <div className={`tabs is-centered ${styles.navTwo}`}>
          <ul>{nav.sheetMusic.subItems.map(navItemToLi)}</ul>
        </div>
      )}

      {slugs.software.has(basePath) && (
        <div className={`tabs is-centered ${styles.navTwo}`}>
          <ul>{nav.software.subItems.map(navItemToLi)}</ul>
        </div>
      )}
    </>
  );
};
