import React from "react";
import { StaticQuery, graphql, Link, withPrefix } from "gatsby";
// @ts-ignore
import { slugFromTag, yearsFromPosts } from "../../js/utils-commonjs";

// import * as pageStyles from "../global-styles/page.module.css";
import * as blogStyles from "../global-styles/blog.module.css";

export const BlogSidebar = () => (
  <StaticQuery
    query={graphql`
      query {
        allMarkdownRemark(
          filter: { frontmatter: { draft: { ne: true } } }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              id
              frontmatter {
                title
                date
              }
              fields {
                path
              }
            }
          }
          group(field: frontmatter___tags) {
            fieldValue
            totalCount
          }
        }
      }
    `}
    render={(data: {
      allMarkdownRemark: {
        edges: {
          node: {
            // TODO: which is it for ID, string or number?
            id: string | number;
            frontmatter: {
              title: string;
            };
            fields: {
              path: string;
            };
          };
        }[];
        group: {
          fieldValue: string;
          totalCount: number;
        }[];
      };
    }) => {
      const posts = data.allMarkdownRemark.edges.map((edge) => edge.node);
      const tags = data.allMarkdownRemark.group;
      const years: string[] = yearsFromPosts(data.allMarkdownRemark.edges);

      return (
        <div role="complementary">
          <aside className={blogStyles.blogSidebarSection}>
            <h3 className={blogStyles.blogSidebarTitle}>
              <Link to={withPrefix("blog/")}>The Clairnote Blog</Link>
            </h3>
            <div>News, updates, developments.</div>
          </aside>
          <aside className={blogStyles.blogSidebarSection}>
            <h3 className={blogStyles.blogSidebarTitle}>Follow</h3>
            <ul className={blogStyles.blogSidebarLinksBlock}>
              <li>
                <Link
                  title="Follow blog posts by email"
                  to={withPrefix("blog/follow-by-email/")}
                >
                  Email
                </Link>
              </li>
              <li>
                <a
                  title="Follow blog posts on Mastodon"
                  href="https://mastodon.social/@clairnote"
                  target="_blank"
                  rel="external noopener noreferrer"
                >
                  Mastodon
                </a>
              </li>
              <li>
                <a
                  title="Follow blog posts on Facebook"
                  href="https://www.facebook.com/ClairnoteMusicNotation"
                  target="_blank"
                  rel="external noopener noreferrer"
                >
                  Facebook
                </a>
              </li>
              <li>
                <Link
                  title="Follow blog posts by RSS feed"
                  to={withPrefix("rss.xml")}
                  rel="alternate"
                  type="application/rss+xml"
                >
                  RSS
                </Link>
              </li>
            </ul>
          </aside>
          <aside className={blogStyles.blogSidebarSection}>
            <h3 className={blogStyles.blogSidebarTitle}>Posts by Tag</h3>
            <ul className={blogStyles.blogSidebarLinksBlock}>
              {tags
                .sort((a, b) => (a.totalCount < b.totalCount ? 1 : -1))
                .map((tag) => (
                  <li key={tag.fieldValue}>
                    <Link to={`/blog/tag/${slugFromTag(tag.fieldValue)}/`}>
                      {tag.fieldValue} ({tag.totalCount})
                    </Link>
                  </li>
                ))}
            </ul>
          </aside>
          <aside className={blogStyles.blogSidebarSection}>
            <h3 className={blogStyles.blogSidebarTitle}>Posts by Year</h3>
            <ul className={blogStyles.blogSidebarLinksBlock}>
              {years.map((year) => (
                <li key={year}>
                  <Link to={`/blog/${year}/`}>{year}</Link>
                </li>
              ))}
            </ul>
          </aside>
          <aside className={blogStyles.blogSidebarSection}>
            <h3 className={blogStyles.blogSidebarTitle}>Posts</h3>
            <ul>
              {posts.slice(0, 99).map((post) => (
                <li key={post.id}>
                  <Link to={post.fields.path}>{post.frontmatter.title}</Link>
                </li>
              ))}
            </ul>
          </aside>
        </div>
      );
    }}
  />
);
