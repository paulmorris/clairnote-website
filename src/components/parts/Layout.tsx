import React from "react";
import Helmet from "react-helmet";
import { withPrefix } from "gatsby";

import { Header } from "./Header";
import { Footer } from "./Footer";
import { BlogSidebar } from "./BlogSidebar";

import { dnOnlyPages } from "../../js/utils";
import { PageLayoutProps } from "../../types/types";

import "../global-styles/main.scss";
import * as pageStyles from "../../components/global-styles/page.module.css";

const getCanonicalUrl = (pathname: string) => {
  const dnPath = pathname.slice(0, 4) === "/dn/";
  // slug includes slashes: '/notetrainer-application/'
  const slug = dnPath ? pathname.slice(3) : pathname;
  const dnOnlyPage = dnOnlyPages.includes(slug);
  const path =
    !dnOnlyPage && dnPath
      ? pathname.slice(3)
      : dnOnlyPage && !dnPath
      ? "/dn" + pathname
      : pathname;

  return "https://clairnote.org" + withPrefix(path);
};

const getTitle = (props: PageLayoutProps) => {
  const siteTitle =
    (props.isBlog ? "Clairnote" : props.clairnoteName) + " Music Notation";
  return props.title ? props.title + " | " + siteTitle : siteTitle;
};

const Layout = (props: PageLayoutProps) => {
  return (
    <>
      <Helmet
        htmlAttributes={{ lang: "en-US" }}
        title={props.fullTitle ? props.fullTitle : getTitle(props)}
        link={[
          {
            rel: "canonical",
            href: getCanonicalUrl(props.location.pathname),
          },
        ]}
      >
        {/* For navbar fixed at the top of the page:
            className={"has-navbar-fixed-top"} */}
        {/* An ID of "page" is used for "Top of Page" link. */}
        <body id="page" />
      </Helmet>

      {props.description && (
        <Helmet meta={[{ name: "description", content: props.description }]} />
      )}

      <Header {...props} />

      {!props.isBlog && (
        <div className={`section ${pageStyles.primaryContent}`}>
          <div className="container">{props.children}</div>
        </div>
      )}

      {props.isBlog && (
        <div className="section">
          <div className="container">
            <div className="columns is-variable is-8-desktop">
              <div
                className={`column is-two-thirds ${pageStyles.primaryContent}`}
              >
                {props.children}
              </div>
              <div className="column">
                <BlogSidebar />
              </div>
            </div>
          </div>
        </div>
      )}

      <Footer {...props} />
    </>
  );
};

export default Layout;
