import { AudioVisualizerStore, INoteSeries } from "./AudioVisualizerStore";

// Keep playing a series of notes or intervals. As long as there are more notes
// to play the function keeps scheduling itself to be called again at a given
// delay to correctly time the notes.

const playNext = (
  series: INoteSeries,
  index: number,
  store: AudioVisualizerStore
) => {
  const timestamp = Date.now();
  const notes = series.yPositions[index];

  for (const n of notes) {
    store.piano.play(n);
  }
  store.setNotePlaying(index);

  const delay = series.delays[index] - (Date.now() - timestamp);

  if (index < series.yPositions.length - 1) {
    store.playbackTimerId = window.setTimeout(
      playNext,
      delay,
      series,
      index + 1,
      store
    );
  } else {
    store.playbackTimerId = window.setTimeout(
      () => store.stopPlayback(),
      delay
    );
  }
};

export const startPlayback = (store: AudioVisualizerStore) => {
  const series = store.currentNoteSeries;
  if (series.yPositions.length > 0) {
    store.setAudioStatus("playing");
    store.playbackTimerId = window.setTimeout(playNext, 500, series, 0, store);
  }
};
