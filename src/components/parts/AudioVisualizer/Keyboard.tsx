import React from "react";
import { observer } from "mobx-react";
import { AudioVisualizerStore } from "./AudioVisualizerStore";
import { GameStore } from "../Game/GameStore";
import { range } from "../../../js/utils";
import { INoteButton, PageType } from "./types";
import { NoteButton } from "./NoteButton";
import { getNoteCaption } from "./audiovisualizer-utils";
import { ClearStaffButton, PlaybackRhythmSelect } from "./instruments-shared";

import * as styles from "./instruments.module.css";
import * as avStyles from "./audiovisualizer.module.css";

type KeyboardType = "keyboard-trad" | "keyboard-six-six";

const makeSpacerNoteButton = (note: number): INoteButton => ({
  label: "",
  note: note,
  type: "spacer",
});

const makeNoteButtons = (
  lowestNote: number,
  highestNote: number,
  isTradKeyboard?: boolean
): INoteButton[] => {
  const notes = range(lowestNote, highestNote);

  const noteButtons = notes.reduce<INoteButton[]>((result, note, index) => {
    const label = getNoteCaption(note);
    const type = label.length === 1 ? "white" : "black";

    result.push({ label, note, type });

    if (
      isTradKeyboard &&
      (label === "E" || label === "B") &&
      index !== notes.length - 1
    ) {
      // Add a spacer.
      result.push(makeSpacerNoteButton(note));
    }

    return result;
  }, []);

  return noteButtons;
};

const isEven = (_: INoteButton, i: number) => i % 2 === 0;
const isOdd = (_: INoteButton, i: number) => i % 2 !== 0;

const makeJankoRows = (noteButtons: INoteButton[]) => {
  const topRow = noteButtons.filter(isEven).concat([makeSpacerNoteButton(69)]);
  const bottomRow = noteButtons.filter(isOdd);
  return { topRow, bottomRow };
};

const makeTradRows = (noteButtons: INoteButton[]) => {
  const topRow = noteButtons.filter(isOdd);
  const bottomRow = noteButtons.filter(isEven);
  return { topRow, bottomRow };
};

const KeyboardSubButtons = observer(function KeyboardSubButtons(props: {
  store: AudioVisualizerStore | GameStore;
}) {
  const { store } = props;
  return (
    <div>
      {/* This is only for the AudioVisualizer page. */}
      {"playbackRhythm" in store.state && "setPlaybackRhythm" in store && (
        <PlaybackRhythmSelect store={store} />
      )}

      {/* This is only for the AudioVisualizer page. */}
      {"clearStaff" in store && <ClearStaffButton store={store} />}
    </div>
  );
});

export const Keyboard = observer(function Keyboard(props: {
  store: AudioVisualizerStore | GameStore;
  pageType: PageType;
  keyboardType: KeyboardType;
}) {
  const { store, pageType, keyboardType } = props;
  const { lowestNote, highestNote } = store.keyboardRange;
  const isTradKeyboard = keyboardType === "keyboard-trad";

  const noteButtons = makeNoteButtons(lowestNote, highestNote, isTradKeyboard);

  const { topRow, bottomRow } = isTradKeyboard
    ? makeTradRows(noteButtons)
    : makeJankoRows(noteButtons);

  const renderNoteButton = (noteButton: INoteButton, index: number) => (
    <NoteButton
      store={store}
      pageType={pageType}
      noteButton={noteButton}
      key={index}
    />
  );

  return (
    <>
      <div className={styles.keyboardRowsContainer}>
        <div>{topRow.map(renderNoteButton)}</div>
        <div>{bottomRow.map(renderNoteButton)}</div>
      </div>
      <KeyboardSubButtons store={store} />
    </>
  );
});
