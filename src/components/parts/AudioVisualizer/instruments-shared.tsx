import React from "react";
import { observer } from "mobx-react-lite";
import { makeSelectOptions } from "../../../js/utils";
import { AudioVisualizerStore, PlaybackRhythm } from "./AudioVisualizerStore";
import { SelectOption } from "../../../types/types";

import * as avStyles from "./audiovisualizer.module.css";

export const PlaybackRhythmSelect = observer(
  function PlaybackRhythmSelect(props: { store: AudioVisualizerStore }) {
    const { store } = props;

    const playbackRhythmOptions: SelectOption<PlaybackRhythm>[] = [
      { value: "even", label: "Rhythm: Even" },
      { value: "asPlayed", label: "Rhythm: As Played" },
    ];

    return (
      <div className={avStyles.buttonOrSelectSpacer}>
        <div className="select is-small">
          <select
            name="Playback Rhythm"
            title="Playback Rhythm"
            onChange={(event) => {
              const value = event.target.value;
              // TypeScript can be stupid sometimes.
              for (const entry of playbackRhythmOptions) {
                if (entry.value === value) {
                  store.setPlaybackRhythm(value);
                }
              }
            }}
            value={store.state.playbackRhythm}
          >
            {makeSelectOptions(playbackRhythmOptions)}
          </select>
        </div>
      </div>
    );
  }
);

export const ClearStaffButton = observer(function ClearStaffButton(props: {
  store: AudioVisualizerStore;
}) {
  const { store } = props;
  return (
    <div className={avStyles.buttonOrSelectSpacer}>
      <button className={"button is-small"} onClick={() => store.clearStaff()}>
        Clear Staff
      </button>
    </div>
  );
});
