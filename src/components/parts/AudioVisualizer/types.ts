/* Common types used for AudioVisualizer and Game components. */

export type INoteButton = {
  label: string;
  note: number;
  type: "white" | "black" | "spacer";
};

export type PageType = "avr" | "game";
