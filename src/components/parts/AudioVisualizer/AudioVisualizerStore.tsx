import { observable, action, computed, configure, makeObservable } from "mobx";

import { iota } from "../../../js/utils";
import { makePiano, Piano } from "./audio-load";
import {
  makeNoteSeries,
  makeInstrumentNoteSeries,
  getXCoordinates,
  convertIntervalConfig,
} from "./store-functions";

configure({ enforceActions: "observed" });

const maxNotesOnStaff = 15;
const leftMargin = 3.1347;

// TODO: use these in index.js
/* const staffLinesY = [1.6747, 3.1747, 6.1747, 7.6747]*/
/* const staffLinesX = 0.3347*/

export interface KeyboardRange {
  lowestNote: number;
  highestNote: number;
}

export type IKeyboardType = "Traditional" | "Janko";

type IntervalScaleType = "major" | "naturalMinor" | "wholeTone" | "chromatic";

export type IntervalFlavor = "diatonic" | "chromatic";

export type DiatonicIntervalType =
  | "seconds"
  | "thirds"
  | "fourths"
  | "fifths"
  | "sixths"
  | "sevenths"
  | "octaves";

export type ChromaticIntervalType =
  | "minorSeconds"
  | "majorSeconds"
  | "minorThirds"
  | "majorThirds"
  | "perfectFourths"
  | "tritones"
  | "perfectFifths"
  | "minorSixths"
  | "majorSixths"
  | "minorSevenths"
  | "majorSevenths"
  | "octaves";

export type ScaleType =
  | "major"
  | "naturalMinor"
  | "harmonicMinor"
  | "melodicMinor"
  | "blues"
  | "chromatic"
  | "wholeTone";

export type ModeType =
  | "ionian"
  | "dorian"
  | "phrygian"
  | "lydian"
  | "mixolydian"
  | "aeolian"
  | "locrian";

export type Root =
  | "c"
  | "d"
  | "e"
  | "fs"
  | "gs"
  | "as"
  | "cs"
  | "ds"
  | "f"
  | "g"
  | "a"
  | "b";

export interface INoteSeriesCaption {
  text: string;
  fontSize?: string;
  fill?: string;
  fontWeight?: string;
}

export interface INoteSeries {
  yPositions: number[][];
  captions: INoteSeriesCaption[];
  xOffsets: number[];
  delays: number[];
}

export type IntervalConfig =
  | { flavor: "diatonic"; type: DiatonicIntervalType }
  | { flavor: "chromatic"; type: ChromaticIntervalType };

export type AvrInputMode =
  | "keyboard-trad"
  | "keyboard-six-six"
  | "guitar"
  | "violin"
  | "scales"
  | "intervals"
  | "modes";

type AudioStatus = "loading" | "stopped" | "playing";

export type PlaybackRhythm = "even" | "asPlayed";

export interface AudioVisualizerStoreState {
  audioStatus: AudioStatus;
  volume: number;
  notePlaying: number | undefined;

  inputMode: AvrInputMode;

  scaleType: ScaleType;
  scaleRoot: Root;

  modeType: ModeType;
  modeRoot: Root;

  intervalScaleType: IntervalScaleType;
  intervalConfig: IntervalConfig;

  instrumentNotes: number[];
  instrumentDelays: number[];
  instrumentTimestamp: number | undefined;
  playbackRhythm: PlaybackRhythm;
}

export class AudioVisualizerStore {
  playbackTimerId = 0;
  piano: Piano;

  // @observable
  state: AudioVisualizerStoreState = {
    audioStatus: "loading",
    volume: 0.35,
    notePlaying: undefined,

    inputMode: "scales",

    scaleType: "major",
    scaleRoot: "c",

    modeType: "ionian",
    modeRoot: "c",

    intervalScaleType: "major",
    intervalConfig: { flavor: "diatonic", type: "thirds" },

    instrumentNotes: [],
    instrumentDelays: [],
    instrumentTimestamp: undefined,
    playbackRhythm: "even",
  };

  constructor(args?: { inputMode?: AvrInputMode }) {
    if (args?.inputMode) {
      this.state.inputMode = args.inputMode;
    }
    makeObservable(this, {
      state: observable,
      scaleNoteSeries: computed,
      modeNoteSeries: computed,
      intervalNoteSeries: computed,
      instrumentNoteSeries: computed,
      currentNoteSeries: computed,
      noteIndexesToDraw: computed,
      xCoordinates: computed,
      keyboardRange: computed,

      setAudioStatus: action,
      setVolume: action,
      setNotePlaying: action,
      setInputMode: action,
      setScaleType: action,
      setScaleRoot: action,
      setModeType: action,
      setModeRoot: action,
      setIntervalConfigChromatic: action,
      setIntervalConfigDiatonic: action,
      setIntervalScale: action,
      setPlaybackRhythm: action,

      instrumentNotePlayed: action,
      stopPlayback: action,
      clearStaff: action,
    });

    this.piano = makePiano(() => this.setAudioStatus("stopped"));
  }

  // COMPUTED PROPERTIES

  // @computed
  get scaleNoteSeries(): INoteSeries {
    return makeNoteSeries(this.state.scaleType, this.state.scaleRoot);
  }

  // @computed
  get modeNoteSeries(): INoteSeries {
    return makeNoteSeries(this.state.modeType, this.state.modeRoot);
  }

  // @computed
  get intervalNoteSeries(): INoteSeries {
    return makeNoteSeries(
      this.state.intervalScaleType,
      "c",
      this.state.intervalConfig
    );
  }

  // @computed
  get instrumentNoteSeries() {
    const delays =
      this.state.playbackRhythm === "even"
        ? false
        : this.state.instrumentDelays;
    return makeInstrumentNoteSeries(this.state.instrumentNotes, delays);
  }

  // @computed
  get currentNoteSeries() {
    switch (this.state.inputMode) {
      case "keyboard-trad":
      // Fall through.
      case "keyboard-six-six":
      // Fall through.
      case "guitar":
      // Fall through.
      case "violin":
        return this.instrumentNoteSeries;
      case "scales":
        return this.scaleNoteSeries;
      case "intervals":
        return this.intervalNoteSeries;
      case "modes":
        return this.modeNoteSeries;
    }
  }

  // @computed
  get noteIndexesToDraw() {
    // With instrument entry we may only show part of a series, so we calculate
    // which part, the indexes.
    const seriesLength = this.currentNoteSeries.yPositions.length,
      notePlaying = this.state.notePlaying,
      audioStatus = this.state.audioStatus;
    let firstIndex = 0;

    // Order matters, after the first case the rest are more notes than fit
    // on the staff via instrument entry.
    if (seriesLength <= maxNotesOnStaff) {
      firstIndex = 0;
    } else if (audioStatus === "stopped") {
      // When not playing audio, show last notes in the series.
      firstIndex = seriesLength - maxNotesOnStaff;
    } else if (audioStatus === "playing") {
      if (notePlaying === undefined) {
        // Show first notes in series when about to start playing.
        firstIndex = 0;
      } else {
        // When a note is playing.
        firstIndex =
          notePlaying >= maxNotesOnStaff
            ? notePlaying - (maxNotesOnStaff - 1)
            : 0;
      }
    }

    const notesOnStaff =
      seriesLength < maxNotesOnStaff ? seriesLength : maxNotesOnStaff;

    return iota(notesOnStaff, firstIndex);
  }

  // @computed
  get xCoordinates() {
    return getXCoordinates(
      this.noteIndexesToDraw,
      this.currentNoteSeries.xOffsets,
      leftMargin
    );
  }

  // Seems unnecessary to @computed a constant, but wasn't accessible
  // when just defined as a property on the store object.
  // @computed
  get keyboardRange(): KeyboardRange {
    return {
      // Just over 3 octaves from E (32) to F (69).
      lowestNote: 32,
      highestNote: 69,
    };
  }

  // ACTIONS

  setAudioStatus(status: AudioStatus) {
    this.state.audioStatus = status;
  }
  setVolume(volume: number) {
    this.piano.volume(volume);
    this.state.volume = volume;
  }
  setNotePlaying(note: number | undefined) {
    this.state.notePlaying = note;
  }
  setInputMode(inputMode: AvrInputMode) {
    this.stopPlayback();
    this.state.inputMode = inputMode;
  }
  setScaleType(type: ScaleType) {
    this.stopPlayback();
    this.state.scaleType = type;
  }
  setScaleRoot(root: Root) {
    this.stopPlayback();
    this.state.scaleRoot = root;
  }
  setModeType(type: ModeType) {
    this.stopPlayback();
    this.state.modeType = type;
  }
  setModeRoot(root: Root) {
    this.stopPlayback();
    this.state.modeRoot = root;
  }
  setIntervalConfigChromatic(type: ChromaticIntervalType) {
    this.stopPlayback();
    this.state.intervalConfig = { flavor: "chromatic", type };
  }
  setIntervalConfigDiatonic(type: DiatonicIntervalType) {
    this.stopPlayback();
    this.state.intervalConfig = { flavor: "diatonic", type };
  }
  setIntervalScale(scaleType: IntervalScaleType, flavor: IntervalFlavor) {
    this.stopPlayback();
    const flavorChanged = this.state.intervalConfig.flavor !== flavor;

    if (flavorChanged) {
      this.state.intervalConfig = convertIntervalConfig(
        this.state.intervalConfig
      );
    }

    this.state.intervalScaleType = scaleType;
  }
  setPlaybackRhythm(rhythm: PlaybackRhythm) {
    this.state.playbackRhythm = rhythm;
  }

  // @action
  instrumentNotePlayed(note: number) {
    this.stopPlayback();
    const stamp = Date.now();
    this.piano.play(note);
    this.state.instrumentNotes.push(note);
    const previousStamp = this.state.instrumentTimestamp;
    if (previousStamp) {
      const rawDelay = stamp - previousStamp;
      const delay = rawDelay > 2000 ? 2000 : rawDelay;
      this.state.instrumentDelays.push(delay);
    }
    this.state.instrumentTimestamp = stamp;
  }

  // @action
  clearStaff() {
    this.stopPlayback();
    this.state.instrumentNotes = [];
    this.state.instrumentDelays = [];
    this.state.instrumentTimestamp = undefined;
  }

  // @action
  stopPlayback() {
    if (this.state.audioStatus === "playing") {
      window.clearTimeout(this.playbackTimerId);
      this.state.audioStatus = "stopped";
      this.state.notePlaying = undefined;
    }
  }
}
