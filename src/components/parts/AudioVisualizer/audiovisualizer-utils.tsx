const noteCaptionLookup = new Map([
  [32, "E"],
  [33, "F"],
  [34, "F#/Gb"],
  [35, "G"],
  [36, "G#/Ab"],
  [37, "A"],
  [38, "A#/Bb"],
  [39, "B"],
  [40, "C"],
  [41, "C#/Db"],
  [42, "D"],
  [43, "D#/Eb"],
]);

export const getNoteCaption = (note: number): string => {
  while (note < 32) note += 12;
  while (note > 43) note -= 12;
  return noteCaptionLookup.get(note) || "";
};
