import React from "react";
import { observer } from "mobx-react";
import { GameStore } from "../Game/GameStore";
import { AudioVisualizerStore } from "./AudioVisualizerStore";
import { INoteButton, PageType } from "./types";

import * as styles from "./note-button.module.css";

export const NoteButton = observer(function NoteButton(props: {
  store: AudioVisualizerStore | GameStore;
  pageType: PageType;
  noteButton: INoteButton;
  className?: string;
}) {
  const { store, pageType, noteButton, className } = props;
  const { type, note, label } = noteButton;

  const typeClass =
    type === "white"
      ? styles.whiteNoteButton
      : type === "black"
      ? styles.blackNoteButton
      : type === "spacer"
      ? styles.spacerNoteButton
      : "";

  // Only add these classes for AudioVisualizer keyboard input.
  const noteClass =
    pageType !== "avr"
      ? ""
      : "inputMode" in store.state &&
        store.state.inputMode !== "keyboard-trad" &&
        store.state.inputMode !== "keyboard-six-six"
      ? ""
      : note >= 32 && note <= 39
      ? styles.avrNoteButton32to39
      : note >= 53 && note <= 64
      ? styles.avrNoteButton53to64
      : note >= 65
      ? styles.avrNoteButton65toInfinity
      : "";

  const classes = `${styles.noteButton} ${typeClass} ${noteClass} ${
    className || ""
  }`;

  return label.length === 0 ? (
    <div className={classes} key={"key" + note} />
  ) : (
    <button
      onClick={() => store.instrumentNotePlayed(note)}
      className={classes}
      key={"key" + note}
    >
      {label.length === 1 ? (
        <div className={styles.noteButtonLabel}>{label}</div>
      ) : (
        <>
          {label.split("/").map((textPart, index) => {
            const labelTwoClass = index === 0 ? "" : styles.noteButtonLabelTwo;
            return (
              <div
                className={`${styles.noteButtonLabel} ${labelTwoClass}`}
                key={"keyText" + index}
              >
                {textPart}
              </div>
            );
          })}
        </>
      )}
    </button>
  );
});
