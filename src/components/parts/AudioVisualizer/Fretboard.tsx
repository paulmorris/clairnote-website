import React from "react";
import { GameStore } from "../Game/GameStore";
import { getNoteCaption } from "./audiovisualizer-utils";
import { AudioVisualizerStore } from "./AudioVisualizerStore";
import { NoteButton } from "./NoteButton";
import { PageType } from "./types";

import * as styles from "./instruments.module.css";

type FretboardConfig = number[][];

export const guitarFretboardConfig: FretboardConfig = [
  [32, 37, 42, 47, 51, 56],
  [33, 38, 43, 48, 52, 57],
  [34, 39, 44, 49, 53, 58],
  [35, 40, 45, 50, 54, 59],
  [36, 41, 46, 51, 55, 60],
];

export const violinFretboardConfig: FretboardConfig = [
  [35, 42, 49, 56],
  [36, 43, 50, 57],
  [37, 44, 51, 58],
  [38, 45, 52, 59],
  [39, 46, 53, 60],
  [40, 47, 54, 61],
  [41, 48, 55, 62],
];

export const Fretboard = ({
  store,
  pageType,
  fretboardConfig,
}: {
  store: GameStore | AudioVisualizerStore;
  pageType: PageType;
  fretboardConfig: FretboardConfig;
}) => {
  return (
    <div className={styles.fretboardContainer}>
      {fretboardConfig.map((row, index1) => {
        return (
          <div key={index1}>
            {row.map((note, index2) => (
              <NoteButton
                store={store}
                pageType={pageType}
                noteButton={{
                  label: getNoteCaption(note),
                  note,
                  type: index1 === 0 ? "black" : "white",
                }}
                className={index1 === 0 ? styles.fretboardOpenStringButton : ""}
                key={index2}
              />
            ))}
          </div>
        );
      })}
    </div>
  );
};
