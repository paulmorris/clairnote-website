// This rule is going away soon.
// https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/issues/398
/* eslint-disable jsx-a11y/no-onchange */

import React, { useState } from "react";
import { observer } from "mobx-react";
/* import DevTools from "mobx-react-devtools";*/

import { AudioVisualizerStore, AvrInputMode } from "./AudioVisualizerStore";
import { Staff } from "./staff";
import { startPlayback } from "./audio-play";
import { Keyboard } from "./Keyboard";
import { SelectOption, IButtonConfig } from "../../../types/types";
import { VolumeSelect } from "../VolumeSelect";
import { makeSelectOptions } from "../../../js/utils";
import {
  Fretboard,
  guitarFretboardConfig,
  violinFretboardConfig,
} from "./Fretboard";
import { ClearStaffButton, PlaybackRhythmSelect } from "./instruments-shared";

import * as avStyles from "./audiovisualizer.module.css";
import * as utilStyles from "../../global-styles/util.module.css";

const scaleTypeButtons = (store: AudioVisualizerStore): IButtonConfig[] => {
  const type = store.state.scaleType;
  return [
    {
      text: "Major",
      clickHandler: () => store.setScaleType("major"),
      pressed: type === "major",
    },
    {
      text: "Natural Minor",
      clickHandler: () => store.setScaleType("naturalMinor"),
      pressed: type === "naturalMinor",
    },
    {
      text: "Harmonic Minor",
      clickHandler: () => store.setScaleType("harmonicMinor"),
      pressed: type === "harmonicMinor",
    },
    {
      text: "Melodic Minor",
      clickHandler: () => store.setScaleType("melodicMinor"),
      pressed: type === "melodicMinor",
    },
    {
      text: "Blues",
      clickHandler: () => store.setScaleType("blues"),
      pressed: type === "blues",
    },
    {
      text: "Chromatic",
      clickHandler: () => store.setScaleType("chromatic"),
      pressed: type === "chromatic",
    },
    {
      text: "Whole Tone",
      clickHandler: () => store.setScaleType("wholeTone"),
      pressed: type === "wholeTone",
    },
  ];
};

const modeTypeButtons = (store: AudioVisualizerStore): IButtonConfig[] => {
  const type = store.state.modeType;
  return [
    {
      text: "Ionian",
      clickHandler: () => store.setModeType("ionian"),
      pressed: type === "ionian",
    },
    {
      text: "Dorian",
      clickHandler: () => store.setModeType("dorian"),
      pressed: type === "dorian",
    },
    {
      text: "Phrygian",
      clickHandler: () => store.setModeType("phrygian"),
      pressed: type === "phrygian",
    },
    {
      text: "Lydian",
      clickHandler: () => store.setModeType("lydian"),
      pressed: type === "lydian",
    },
    {
      text: "Mixolydian",
      clickHandler: () => store.setModeType("mixolydian"),
      pressed: type === "mixolydian",
    },
    {
      text: "Aeolian",
      clickHandler: () => store.setModeType("aeolian"),
      pressed: type === "aeolian",
    },
    {
      text: "Locrian",
      clickHandler: () => store.setModeType("locrian"),
      pressed: type === "locrian",
    },
  ];
};

const rootNoteButtonsTopRow = (
  store: AudioVisualizerStore,
  setterName: "setScaleRoot" | "setModeRoot",
  rootType: "scaleRoot" | "modeRoot"
): IButtonConfig[] => {
  const root = store.state[rootType];
  return [
    {
      text: "C",
      clickHandler: () => store[setterName]("c"),
      pressed: root === "c",
    },
    {
      text: "D",
      clickHandler: () => store[setterName]("d"),
      pressed: root === "d",
    },
    {
      text: "E",
      clickHandler: () => store[setterName]("e"),
      pressed: root === "e",
    },
    {
      text: "F#/Gb",
      clickHandler: () => store[setterName]("fs"),
      pressed: root === "fs",
    },
    {
      text: "G#/Ab",
      clickHandler: () => store[setterName]("gs"),
      pressed: root === "gs",
    },
    {
      text: "A#/Bb",
      clickHandler: () => store[setterName]("as"),
      pressed: root === "as",
    },
  ];
};

const rootNoteButtonsBottomRow = (
  store: AudioVisualizerStore,
  setterName: "setScaleRoot" | "setModeRoot",
  rootType: "scaleRoot" | "modeRoot"
): IButtonConfig[] => {
  const root = store.state[rootType];
  return [
    {
      text: "C#/Db",
      clickHandler: () => store[setterName]("cs"),
      pressed: root === "cs",
    },
    {
      text: "D#/Eb",
      clickHandler: () => store[setterName]("ds"),
      pressed: root === "ds",
    },
    {
      text: "F",
      clickHandler: () => store[setterName]("f"),
      pressed: root === "f",
    },
    {
      text: "G",
      clickHandler: () => store[setterName]("g"),
      pressed: root === "g",
    },
    {
      text: "A",
      clickHandler: () => store[setterName]("a"),
      pressed: root === "a",
    },
    {
      text: "B",
      clickHandler: () => store[setterName]("b"),
      pressed: root === "b",
    },
  ];
};

const intervalScaleButtons = (store: AudioVisualizerStore): IButtonConfig[] => {
  const type = store.state.intervalScaleType;
  return [
    {
      text: (
        <span>
          C Major <span className={avStyles.wideText}>Key</span>
        </span>
      ),
      clickHandler: () => store.setIntervalScale("major", "diatonic"),
      pressed: type === "major",
    },

    {
      text: (
        <span>
          C Minor <span className={avStyles.wideText}>Key</span>
        </span>
      ),
      clickHandler: () => store.setIntervalScale("naturalMinor", "diatonic"),
      pressed: type === "naturalMinor",
    },

    {
      text: (
        <span>
          Whole Tone <span className={avStyles.wideText}>Scales</span>
        </span>
      ),
      clickHandler: () => store.setIntervalScale("wholeTone", "chromatic"),
      pressed: type === "wholeTone",
    },

    {
      text: (
        <span>
          Chromatic <span className={avStyles.wideText}>Scale</span>
        </span>
      ),
      clickHandler: () => store.setIntervalScale("chromatic", "chromatic"),
      pressed: type === "chromatic",
    },
  ];
};

const chromaticIntervalsRowOne = (
  store: AudioVisualizerStore
): IButtonConfig[] => {
  const type = store.state.intervalConfig.type;
  return [
    {
      text: (
        <>
          <span className={avStyles.wideText}>Minor 2nds</span>
          <span className={avStyles.narrowText}>m2</span>
        </>
      ),
      title: "1 semitone",
      clickHandler: () => store.setIntervalConfigChromatic("minorSeconds"),
      pressed: type === "minorSeconds",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Minor 3rds</span>
          <span className={avStyles.narrowText}>m3</span>
        </>
      ),
      title: "3 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("minorThirds"),
      pressed: type === "minorThirds",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>4ths</span>
          <span className={avStyles.narrowText}>P4</span>
        </>
      ),
      title: "5 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("perfectFourths"),
      pressed: type === "perfectFourths",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>5ths</span>
          <span className={avStyles.narrowText}>P5</span>
        </>
      ),
      title: "7 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("perfectFifths"),
      pressed: type === "perfectFifths",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Major 6ths</span>
          <span className={avStyles.narrowText}>M6</span>
        </>
      ),
      title: "9 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("majorSixths"),
      pressed: type === "majorSixths",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Major 7ths</span>
          <span className={avStyles.narrowText}>M7</span>
        </>
      ),
      title: "11 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("majorSevenths"),
      pressed: type === "majorSevenths",
    },
  ];
};

const chromaticIntervalsRowTwo = (
  store: AudioVisualizerStore
): IButtonConfig[] => {
  const type = store.state.intervalConfig.type;
  return [
    {
      text: (
        <>
          <span className={avStyles.wideText}>Major 2nds</span>
          <span className={avStyles.narrowText}>M2</span>
        </>
      ),
      title: "2 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("majorSeconds"),
      pressed: type === "majorSeconds",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Major 3rds</span>
          <span className={avStyles.narrowText}>M3</span>
        </>
      ),
      title: "4 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("majorThirds"),
      pressed: type === "majorThirds",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Tritones</span>
          <span className={avStyles.narrowText}>TT</span>
        </>
      ),
      title: "6 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("tritones"),
      pressed: type === "tritones",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Minor 6ths</span>
          <span className={avStyles.narrowText}>m6</span>
        </>
      ),
      title: "8 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("minorSixths"),
      pressed: type === "minorSixths",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Minor 7ths</span>
          <span className={avStyles.narrowText}>m7</span>
        </>
      ),
      title: "10 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("minorSevenths"),
      pressed: type === "minorSevenths",
    },
    {
      text: (
        <>
          <span className={avStyles.wideText}>Octaves</span>
          <span className={avStyles.narrowText}>P8</span>
        </>
      ),
      title: "12 semitones",
      clickHandler: () => store.setIntervalConfigChromatic("octaves"),
      pressed: type === "octaves",
    },
  ];
};

const diatonicIntervalsRow = (store: AudioVisualizerStore): IButtonConfig[] => {
  const type = store.state.intervalConfig.type;
  return [
    {
      text: "2nds",
      title: "1 or 2 semitones",
      clickHandler: () => store.setIntervalConfigDiatonic("seconds"),
      pressed: type === "seconds",
    },
    {
      text: "3rds",
      title: "3 or 4 semitones",
      clickHandler: () => store.setIntervalConfigDiatonic("thirds"),
      pressed: type === "thirds",
    },
    {
      text: "4ths",
      title: "5 (or 6) semitones",
      clickHandler: () => store.setIntervalConfigDiatonic("fourths"),
      pressed: type === "fourths",
    },
    {
      text: "5ths",
      title: "7 (or 6) semitones",
      clickHandler: () => store.setIntervalConfigDiatonic("fifths"),
      pressed: type === "fifths",
    },
    {
      text: "6ths",
      title: "8 or 9 semitones",
      clickHandler: () => store.setIntervalConfigDiatonic("sixths"),
      pressed: type === "sixths",
    },
    {
      text: "7ths",
      title: "10 or 11 semitones",
      clickHandler: () => store.setIntervalConfigDiatonic("sevenths"),
      pressed: type === "sevenths",
    },
    {
      text: "Octaves",
      title: "12 semitones",
      clickHandler: () => store.setIntervalConfigDiatonic("octaves"),
      pressed: type === "octaves",
    },
  ];
};

const SecondaryControls = observer(
  function AudioVisualizerSecondaryControls(props: {
    store: AudioVisualizerStore;
  }) {
    const { store } = props;
    const { inputMode } = props.store.state;

    if (inputMode === "scales" || inputMode === "modes") {
      const setterName = inputMode === "modes" ? "setModeRoot" : "setScaleRoot";
      const rootType = inputMode === "modes" ? "modeRoot" : "scaleRoot";

      return (
        <>
          <div className={avStyles.subcontrols}>
            {inputMode === "scales" && (
              <div className={avStyles.scalesButtonRow}>
                <div
                  className={`field has-addons has-addons-centered ${utilStyles.flexWrapWrap}`}
                >
                  {scaleTypeButtons(store).map(
                    ({ text, title, clickHandler, pressed }, index) => (
                      <div
                        className={`control ${utilStyles.marginBottomMinusOnePx}`}
                        key={index + "Button"}
                      >
                        <button
                          title={title}
                          onClick={clickHandler}
                          className={`button is-small ${
                            avStyles.scaleTypeButton
                          } ${pressed ? avStyles.buttonPressed : ""}`}
                        >
                          <span>{text}</span>
                        </button>
                      </div>
                    )
                  )}
                </div>
              </div>
            )}

            {inputMode === "modes" && (
              <div className={avStyles.modesButtonRow}>
                <div
                  className={`field has-addons has-addons-centered ${utilStyles.flexWrapWrap}`}
                >
                  {modeTypeButtons(store).map(
                    ({ text, title, clickHandler, pressed }, index) => (
                      <div
                        className={`control ${utilStyles.marginBottomMinusOnePx}`}
                        key={index + "Button"}
                      >
                        <button
                          title={title}
                          onClick={clickHandler}
                          className={`button is-small ${
                            avStyles.scaleTypeButton
                          } ${pressed ? avStyles.buttonPressed : ""}`}
                        >
                          <span>{text}</span>
                        </button>
                      </div>
                    )
                  )}
                </div>
              </div>
            )}

            <div className={avStyles.scaleRootButtonsTopRow}>
              <div className={"field has-addons has-addons-centered"}>
                {rootNoteButtonsTopRow(store, setterName, rootType).map(
                  ({ text, title, clickHandler, pressed }, index) => (
                    <div className="control" key={index + "Button"}>
                      <button
                        title={title}
                        onClick={clickHandler}
                        className={`button is-small ${
                          avStyles.scaleRootButton
                        } ${pressed ? avStyles.buttonPressed : ""}`}
                      >
                        <span>{text}</span>
                      </button>
                    </div>
                  )
                )}
              </div>
            </div>

            <div className={avStyles.scaleRootButtonsBottomRow}>
              <div className={"field has-addons has-addons-centered"}>
                {rootNoteButtonsBottomRow(store, setterName, rootType).map(
                  ({ text, title, clickHandler, pressed }, index) => (
                    <div className="control" key={index + "Button"}>
                      <button
                        title={title}
                        onClick={clickHandler}
                        className={`button is-small ${
                          avStyles.scaleRootButton
                        } ${pressed ? avStyles.buttonPressed : ""}`}
                      >
                        <span>{text}</span>
                      </button>
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
        </>
      );
    }

    if (inputMode === "keyboard-trad") {
      return (
        <div className={avStyles.subcontrols}>
          <Keyboard
            store={store}
            pageType={"avr"}
            keyboardType={"keyboard-trad"}
          />
        </div>
      );
    }

    if (inputMode === "keyboard-six-six") {
      return (
        <div className={avStyles.subcontrols}>
          <Keyboard
            store={store}
            pageType={"avr"}
            keyboardType={"keyboard-six-six"}
          />
        </div>
      );
    }

    if (inputMode === "guitar") {
      return (
        <div className={avStyles.subcontrols}>
          <Fretboard
            store={store}
            pageType={"avr"}
            fretboardConfig={guitarFretboardConfig}
          />
          <div>
            <PlaybackRhythmSelect store={store} />
            <ClearStaffButton store={store} />
          </div>
        </div>
      );
    }

    if (inputMode === "violin") {
      return (
        <div className={avStyles.subcontrols}>
          <Fretboard
            store={store}
            pageType={"avr"}
            fretboardConfig={violinFretboardConfig}
          />
          <div>
            <PlaybackRhythmSelect store={store} />
            <ClearStaffButton store={store} />
          </div>
        </div>
      );
    }

    if (inputMode === "intervals") {
      return (
        <div className={avStyles.subcontrols}>
          <div className={avStyles.intervalScaleButtonRow}>
            <div className={"field has-addons has-addons-centered"}>
              {intervalScaleButtons(store).map(
                ({ text, title, clickHandler, pressed }, index) => (
                  <div className="control" key={index + "Button"}>
                    <button
                      title={title}
                      onClick={clickHandler}
                      className={`button is-small ${
                        pressed ? avStyles.buttonPressed : ""
                      }`}
                    >
                      <span>{text}</span>
                    </button>
                  </div>
                )
              )}
            </div>
          </div>

          {store.state.intervalConfig.flavor === "chromatic" && (
            <>
              <div className={avStyles.chromaticIntervalButtonsTopRow}>
                <div className={"field has-addons has-addons-centered"}>
                  {chromaticIntervalsRowOne(store).map(
                    ({ text, title, clickHandler, pressed }, index) => (
                      <div className="control" key={index + "Button"}>
                        <button
                          title={title}
                          onClick={clickHandler}
                          className={`button is-small ${
                            avStyles.chromaticIntervalButton
                          } ${pressed ? avStyles.buttonPressed : ""}`}
                        >
                          <span>{text}</span>
                        </button>
                      </div>
                    )
                  )}
                </div>
              </div>

              <div className={avStyles.chromaticIntervalButtonsBottomRow}>
                <div className={"field has-addons has-addons-centered"}>
                  {chromaticIntervalsRowTwo(store).map(
                    ({ text, title, clickHandler, pressed }, index) => (
                      <div className="control" key={index + "Button"}>
                        <button
                          title={title}
                          onClick={clickHandler}
                          className={`button is-small ${
                            avStyles.chromaticIntervalButton
                          } ${pressed ? avStyles.buttonPressed : ""}`}
                        >
                          <span>{text}</span>
                        </button>
                      </div>
                    )
                  )}
                </div>
              </div>
            </>
          )}

          {store.state.intervalConfig.flavor === "diatonic" && (
            <div>
              <div className={"field has-addons has-addons-centered"}>
                {diatonicIntervalsRow(store).map(
                  ({ text, title, clickHandler, pressed }, index) => (
                    <div className="control" key={index + "Button"}>
                      <button
                        title={title}
                        onClick={clickHandler}
                        className={`button is-small ${
                          avStyles.diatonicIntervalButton
                        } ${pressed ? avStyles.buttonPressed : ""}`}
                      >
                        <span>{text}</span>
                      </button>
                    </div>
                  )
                )}
              </div>
            </div>
          )}
        </div>
      );
    }

    return null;
  }
);

const inputModeOptions: SelectOption<AvrInputMode>[] = [
  { label: "Traditional Keyboard", value: "keyboard-trad" },
  { label: "Janko-style Keyboard", value: "keyboard-six-six" },
  { label: "Guitar", value: "guitar" },
  { label: "Violin or Mandolin", value: "violin" },
  { label: "Scales", value: "scales" },
  { label: "Intervals", value: "intervals" },
  { label: "Modes", value: "modes" },
];

const TopButtonRow = observer(function AudioVisualizerTopButtonRow({
  store,
  hideNoteSeriesMenu,
}: {
  store: AudioVisualizerStore;
  hideNoteSeriesMenu?: boolean;
}) {
  const audioLoading = store.state.audioStatus === "loading";
  const audioStopped = store.state.audioStatus === "stopped";

  const playButtonText = audioStopped ? "Play Audio" : "Stop Audio";
  const playButtonClickHandler = audioStopped
    ? () => startPlayback(store)
    : () => store.stopPlayback();

  return (
    <div className={`${avStyles.topButtonRow}`}>
      {!hideNoteSeriesMenu && (
        <div className={avStyles.buttonOrSelectSpacer}>
          <div className={"select is-small"}>
            <select
              title="Show scales, intervals, melodies, or modes"
              onChange={(event) => {
                const value = event.target.value;
                // TypeScript can be stupid sometimes.
                for (const entry of inputModeOptions) {
                  if (entry.value === value) {
                    store.setInputMode(value);
                  }
                }
              }}
              value={store.state.inputMode}
            >
              {makeSelectOptions(inputModeOptions)}
            </select>
          </div>
        </div>
      )}

      <div className={avStyles.buttonOrSelectSpacer}>
        <button
          onClick={playButtonClickHandler}
          className={`button is-small ${audioLoading ? "is-loading" : ""} ${
            avStyles.playAudioButton
          }`}
        >
          {playButtonText}
        </button>
      </div>

      {store.state.audioStatus !== "loading" && (
        <div className={avStyles.buttonOrSelectSpacer}>
          <VolumeSelect store={store} />
        </div>
      )}
    </div>
  );
});

export const AudioVisualizer = observer(function AudioVisualizer(props: {
  sn: boolean;
  inputMode?: AvrInputMode;
  hideNoteSeriesMenu?: boolean;
}) {
  const { sn, hideNoteSeriesMenu, inputMode } = props;

  const [store] = useState(new AudioVisualizerStore({ inputMode }));
  return (
    <div className={avStyles.audiovisualizer}>
      <Staff
        staffLinesY={[1.6747, 3.1747, 6.1747, 7.6747]}
        staffLinesX={0.3347}
        noteSeries={store.currentNoteSeries}
        noteIndexesToDraw={store.noteIndexesToDraw}
        xCoordinates={store.xCoordinates}
        notePlaying={store.state.notePlaying}
        width="199.12mm"
        height="67mm"
        viewBox="0 0 66.6537 16.5962"
        clairnoteType={sn ? "sn" : "dn"}
      />
      <TopButtonRow store={store} hideNoteSeriesMenu={hideNoteSeriesMenu} />
      <SecondaryControls store={store} />
      {/* <DevTools /> */}
    </div>
  );
});
