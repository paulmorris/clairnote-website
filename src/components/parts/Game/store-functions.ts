import { randomInteger } from "../../../js/utils";
import { NotesGameType } from "./GameStore";

// use with Array.map to transpose up an octave
const upAnOctave = (num: number) => {
  return num + 12;
};

const changeKey = (key: number) => {
  // "keyChanges" limits us to key changes that contain
  // 3 or more new notes that weren't in the last key,
  // for better note coverage.
  const keyChanges = [4, 6, 9, 11];
  return (key + keyChanges[randomInteger(4)]) % 12;
};

const transposeToNewKey = (notes: number[], newkey: number) => {
  // transposes the note numbers in array "notes" by number "newkey"
  // lowest and mod let us use modulo/remainder operator
  // by effectively shifting the series, so it's as if it started at 0
  const lowest = notes[0],
    highest = notes[notes.length - 1],
    mod = highest + 1 - lowest;
  return notes.map((note) => ((note - lowest + newkey) % mod) + lowest);
};

// A function for testing, should be moved into proper tests eventually
/*
anyRepeats: function(ary) {
    var i;
    for (i = 3; i < ary.length; i += 1) {
        if (ary[i] === ary[i-1] ||
            ary[i] === ary[i-2] ||
            ary[i] === ary[i-3]) {
            // console.log("repeats!");
            return true;
        }
    }
    return false;
};
*/

// The order of the notes is randomized using a recursive function.
// We avoid repeated notes, and actually make it so that the closest the same
// note can be to itself is 3 notes between them. We have to stop
// when there are only 3 notes left in the set to avoid chance of infinite recursion
// if the only notes left to choose from happen to be the same as the last 3 notes loaded.
const pickRandom = (src: number[], last3: number[]): number => {
  // pick a value randomly from src and return its index if it is not in
  // last3 (the last 3 notes or intervals), else recurse and try again.
  const rnd = randomInteger(src.length);
  const n = src[rnd];
  if (last3.indexOf(n) === -1) {
    return rnd;
  }
  return pickRandom(src, last3);
};

const shuffle = (ary: number[], last3: number[]) => {
  let i;
  const oldAry = ary.slice();
  const len = oldAry.length - 3;
  const newAry = last3.slice();
  let index;

  // randomize the order of notes.
  for (i = 0; i < len; i += 1) {
    // pick a semi-random note from oldAry, add it to newAry, remove it from oldAry
    index = pickRandom(oldAry, newAry.slice(-3));
    newAry.push(oldAry[index]);
    oldAry.splice(index, 1);
  }

  // for testing purposes...
  // if (anyRepeats(newAry)) { return shuffle(ary, last3); }

  // don't return the first three items, they are already in the queue
  return newAry.slice(3);
};

const doubleArray = <T>(array: T[]) => array.slice().concat(array.slice());

const getRandomKey = () => Math.floor(Math.random() * 12);

const getMoreTargets = (source: number[], last3: number[]) =>
  shuffle(doubleArray(source), last3);

export const generateNoteTargets = (
  noteSource: number[],
  notesGameType: NotesGameType,
  total: number
) => {
  const result: number[] = [];

  if (notesGameType === "oneOctaveAll" || notesGameType === "twoOctavesAll") {
    // for "all notes" games, change the key of the notes
    let key = getRandomKey();

    while (result.length < total) {
      key = changeKey(key);
      const last3 = result.slice(-3);
      const transposed = transposeToNewKey(noteSource, key);
      let newAdds = getMoreTargets(transposed, last3);

      if (notesGameType === "twoOctavesAll") {
        // reduce length so we aren't in the same key too long
        newAdds = newAdds.slice(0, 14);
      }

      Array.prototype.push.apply(result, newAdds);
    }
  } else {
    while (result.length < total) {
      const last3 = result.slice(-3),
        newAdds = getMoreTargets(noteSource, last3);
      Array.prototype.push.apply(result, newAdds);
    }
  }
  return result;
};

export const generateIntervalTargets = (
  intervalSource: number[],
  total: number
) => {
  const result: number[] = [];
  while (result.length < total) {
    const last3 = result.slice(3),
      newAdds = getMoreTargets(intervalSource, last3);
    Array.prototype.push.apply(result, newAdds);
  }
  return result;
};

// interval is the interval
// lowNotes is the array to search for the lower note
// highNotes is the array to search for the higher note
const getNoteForInterval = (
  interval: number,
  lowNotes: number[],
  highNotes: number[]
): number => {
  const r = randomInteger(lowNotes.length),
    lowNote = lowNotes[r];
  if (highNotes.indexOf(lowNote + interval) !== -1) {
    return lowNote;
  }
  lowNotes.splice(r, 1);
  if (lowNotes.length === 0) {
    // this should never happen, all intervals should always be
    // possible between some pair of notes from lowNotes and highNotes
    // Return lowNote as a reasonable fallback.
    return lowNote;
  }
  return getNoteForInterval(interval, lowNotes, highNotes);
};

export const getNotesForIntervals = (
  noteSource: number[],
  intervals: number[]
): number[] => {
  const lowNotes = transposeToNewKey(noteSource.slice(), getRandomKey()),
    highNotes = lowNotes.slice().concat(lowNotes.map(upAnOctave));

  // get notes to go with intervals
  const result = intervals.map((interval) =>
    getNoteForInterval(interval, lowNotes.slice(), highNotes.slice())
  );

  return result;
};
