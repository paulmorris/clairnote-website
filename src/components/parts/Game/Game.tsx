import React, { useState } from "react";
import { observer } from "mobx-react";
// import DevTools from "mobx-react-devtools";

import { GameStore } from "./GameStore";
import { ControlsTop } from "./controls-top";
import { ControlsBottom } from "./controls-bottom";
import { Staff } from "../AudioVisualizer/staff";

import * as gameStyles from "./game.module.css";
import * as avStyles from "../AudioVisualizer/audiovisualizer.module.css";

export const Game = observer(function Game(props: { sn: boolean }) {
  const { sn } = props;
  const clairnoteType = sn ? "sn" : "dn";
  const [gameStore] = useState(new GameStore());
  return (
    <div className={`${avStyles.audiovisualizer} ${gameStyles.gameWrapper}`}>
      <ControlsTop gameStore={gameStore} />
      <Staff
        staffLinesY={[1.6747, 3.1747, 6.1747, 7.6747]}
        staffLinesX={0.3347}
        noteSeries={gameStore.targetsNoteSeries}
        noteIndexesToDraw={gameStore.state.targetIndexesToDraw}
        xCoordinates={gameStore.xCoordinates}
        // The ratio of width/height should be the same as the ratio of
        // width/height in the viewBox (the last two values).
        width="149.34mm"
        height="39.185mm"
        viewBox="0 0 49.990275 13.116840"
        // Below are the values before the height was increased slightly so
        // that the low E note would appear fully for guitar games.
        // height="37.185mm"
        // viewBox="0 0 49.990275 12.44715"
        clairnoteType={clairnoteType}
        notePlaying={undefined}
      />
      <Staff
        staffLinesY={[1.6747, 3.1747, 6.1747, 7.6747]}
        staffLinesX={0.3347}
        noteSeries={gameStore.attemptsNoteSeries}
        noteIndexesToDraw={gameStore.attemptIndexesToDraw}
        xCoordinates={gameStore.xCoordinates}
        width="149.34mm"
        height="49.58mm"
        viewBox="0 0 49.990275 16.5962"
        clairnoteType={clairnoteType}
        notePlaying={undefined}
      />
      <div className={gameStyles.feedback}>{gameStore.state.feedbackText}</div>
      <ControlsBottom gameStore={gameStore} />

      {/* <DevTools /> */}
    </div>
  );
});
