// This rule is going away soon.
// https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/issues/398
/* eslint-disable jsx-a11y/no-onchange */

import React from "react";
import { observer } from "mobx-react";
import {
  GameStore,
  GameType,
  IntervalsGameType,
  NotesGameType,
} from "./GameStore";
import { makeSelectOptions } from "../../../js/utils";
import { SelectOption } from "../../../types/types";
import { VolumeSelect } from "../../parts/VolumeSelect";

import * as gameStyles from "./game.module.css";
import * as avStyles from "../AudioVisualizer/audiovisualizer.module.css";

const gameTypeOptions: SelectOption<GameType>[] = [
  { value: "notesKeyboardTrad", label: "Notes: Traditional Keyboard" },
  { value: "notesKeyboardSixSix", label: "Notes: Janko-style Keyboard" },
  { value: "notesGuitar", label: "Notes: Guitar" },
  { value: "notesViolin", label: "Notes: Violin or Mandolin" },
  { value: "notesTrumpet", label: "Notes: Trumpet" },
  { value: "intervals", label: "Intervals" },
];

const notesGameTypeOptions: SelectOption<NotesGameType>[] = [
  { value: "oneOctaveNaturals", label: "1 Octave: Naturals" },
  { value: "oneOctaveSharpsFlats", label: "1 Octave: Sharps & Flats" },
  { value: "oneOctaveAll", label: "1 Octave: All" },
  { value: "twoOctavesNaturals", label: "2 Octaves: Naturals" },
  { value: "twoOctavesSharpsFlats", label: "2 Octaves: Sharps & Flats" },
  { value: "twoOctavesAll", label: "2 Octaves: All" },
];

// TODO: Rework things to make notes game types more flexible (per instrument).
const guitarOrViolinNotesGameTypeOptions: SelectOption<NotesGameType>[] = [
  { value: "oneOctaveNaturals", label: "1 Octave: Naturals" },
  { value: "oneOctaveSharpsFlats", label: "1 Octave: Sharps & Flats" },
  { value: "oneOctaveAll", label: "1 Octave: All Notes" },
  { value: "twoOctavesNaturals", label: "All Naturals" },
  { value: "twoOctavesSharpsFlats", label: "All Sharps & Flats" },
  { value: "twoOctavesAll", label: "All Notes" },
];

const intervalsGameTypeOptions: SelectOption<IntervalsGameType>[] = [
  { value: "evenIntervals", label: "Even Number of Semitones" },
  { value: "oddIntervals", label: "Odd Number of Semitones" },
  { value: "allIntervals", label: "All Intervals" },
  { value: "diatonicNumberIntervals", label: "Diatonic Number Intervals" },
];

const targetsAtATimeOptions: SelectOption<number>[] = [
  { value: 1, label: "1 at a Time" },
  { value: 2, label: "2 at a Time" },
  { value: 4, label: "4 at a Time" },
  { value: 8, label: "8 at a Time" },
];

export const ControlsTop = observer(function ControlsTop(props: {
  gameStore: GameStore;
}) {
  const { gameStore } = props;
  const { gameType, notesGameType, intervalsGameType } = gameStore.state;
  return (
    <div className={gameStyles.controlsTop}>
      <div
        className={gameStyles.scoreBoard}
        title={
          gameStore.state.rightTally +
          " correct, " +
          gameStore.state.wrongTally +
          " incorrect"
        }
      >
        {gameStore.state.secondsLeft} Seconds &nbsp; {gameStore.totalTally}{" "}
        Points
      </div>

      {gameStore.state.gameStatus === "off" ? (
        <div className={avStyles.buttonOrSelectSpacer}>
          <button
            className={"button is-small"}
            onClick={() => gameStore.gameStarted()}
          >
            Start
          </button>
        </div>
      ) : (
        <div className={avStyles.buttonOrSelectSpacer}>
          <button
            className={"button is-small"}
            onClick={() => gameStore.endGameClearStaves()}
          >
            Stop
          </button>
        </div>
      )}

      <div className={avStyles.buttonOrSelectSpacer}>
        <div className="select is-small">
          <select
            name="Select which notes or intervals to practice."
            title="Select which notes or intervals to practice."
            onChange={(event) => {
              const value = event.target.value;
              // TypeScript can be stupid sometimes.
              for (const entry of gameTypeOptions) {
                if (entry.value === value) {
                  gameStore.changeGameType(value);
                }
              }
            }}
            value={gameType}
          >
            {makeSelectOptions(gameTypeOptions)}
          </select>
        </div>
      </div>

      {(gameType === "notesKeyboardTrad" ||
        gameType === "notesKeyboardSixSix" ||
        gameType === "notesTrumpet") && (
        <div className={avStyles.buttonOrSelectSpacer}>
          <div className="select is-small">
            <select
              name="Select which notes to practice."
              title="Select which notes to practice."
              onChange={(event) => {
                const value = event.target.value;
                // TypeScript can be stupid sometimes.
                for (const entry of notesGameTypeOptions) {
                  if (entry.value === value) {
                    gameStore.changeNotesGameType(value);
                  }
                }
              }}
              value={notesGameType}
              className={gameStyles.gameTypeSelect}
            >
              {makeSelectOptions(notesGameTypeOptions)}
            </select>
          </div>
        </div>
      )}

      {(gameType === "notesGuitar" || gameType === "notesViolin") && (
        <div className={avStyles.buttonOrSelectSpacer}>
          <div className="select is-small">
            <select
              name="Select which notes to practice."
              title="Select which notes to practice."
              onChange={(event) => {
                const value = event.target.value;
                // TypeScript can be stupid sometimes.
                for (const entry of notesGameTypeOptions) {
                  if (entry.value === value) {
                    gameStore.changeNotesGameType(value);
                  }
                }
              }}
              value={notesGameType}
              className={gameStyles.gameTypeSelect}
            >
              {makeSelectOptions(guitarOrViolinNotesGameTypeOptions)}
            </select>
          </div>
        </div>
      )}

      {gameType === "intervals" && (
        <div className={avStyles.buttonOrSelectSpacer}>
          <div className="select is-small">
            <select
              name="Select which intervals to practice."
              title="Select which intervals to practice."
              onChange={(event) => {
                const value = event.target.value;
                // TypeScript can be stupid sometimes.
                for (const entry of intervalsGameTypeOptions) {
                  if (entry.value === value) {
                    gameStore.changeIntervalsGameType(value);
                  }
                }
              }}
              value={intervalsGameType}
              className={gameStyles.gameTypeSelect}
            >
              {makeSelectOptions(intervalsGameTypeOptions)}
            </select>
          </div>
        </div>
      )}

      <div className={avStyles.buttonOrSelectSpacer}>
        <div className="select is-small">
          <select
            name="How many notes/intervals on the staff at once?"
            title="How many notes/intervals on the staff at once?"
            onChange={(event) =>
              gameStore.setTargetsAtATime(parseInt(event.target.value))
            }
            value={gameStore.state.targetsAtATime}
          >
            {makeSelectOptions(targetsAtATimeOptions)}
          </select>
        </div>
      </div>

      <div className={avStyles.buttonOrSelectSpacer}>
        <VolumeSelect store={gameStore} />
      </div>
    </div>
  );
});
