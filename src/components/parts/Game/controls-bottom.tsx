import React from "react";
import { observer } from "mobx-react";
import { Keyboard } from "../AudioVisualizer/Keyboard";
import { IButtonConfig } from "../../../types/types";
import { GameStore } from "./GameStore";

import * as gameStyles from "./game.module.css";
import * as utilStyles from "../../global-styles/util.module.css";
import * as avStyles from "../AudioVisualizer/audiovisualizer.module.css";
import {
  Fretboard,
  guitarFretboardConfig,
  violinFretboardConfig,
} from "../AudioVisualizer/Fretboard";

const makeOddIntervalsRow = (gameStore: GameStore): IButtonConfig[] => [
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Minor 2nd</span>
        <span className={gameStyles.narrowText}>m2</span>
      </>
    ),
    title: "1 semitone",
    clickHandler: () => gameStore.intervalClicked([1]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Minor 3rd</span>
        <span className={gameStyles.narrowText}>m3</span>
      </>
    ),
    title: "3 semitones",
    clickHandler: () => gameStore.intervalClicked([3]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Perfect 4th</span>
        <span className={gameStyles.narrowText}>P4</span>
      </>
    ),
    title: "5 semitones",
    clickHandler: () => gameStore.intervalClicked([5]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Perfect 5th</span>
        <span className={gameStyles.narrowText}>P5</span>
      </>
    ),
    title: "7 semitones",
    clickHandler: () => gameStore.intervalClicked([7]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Major 6th</span>
        <span className={gameStyles.narrowText}>M6</span>
      </>
    ),
    title: "9 semitones",
    clickHandler: () => gameStore.intervalClicked([9]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Major 7th</span>
        <span className={gameStyles.narrowText}>M7</span>
      </>
    ),
    title: "11 semitones",
    clickHandler: () => gameStore.intervalClicked([11]),
  },
];

const makeEvenIntervalsRow = (gameStore: GameStore): IButtonConfig[] => [
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Major 2nd</span>
        <span className={gameStyles.narrowText}>M2</span>
      </>
    ),
    title: "2 semitones",
    clickHandler: () => gameStore.intervalClicked([2]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Major 3rd</span>
        <span className={gameStyles.narrowText}>M3</span>
      </>
    ),
    title: "4 semitones",
    clickHandler: () => gameStore.intervalClicked([4]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Tritone</span>
        <span className={gameStyles.narrowText}>TT</span>
      </>
    ),
    title: "6 semitones",
    clickHandler: () => gameStore.intervalClicked([6]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Minor 6th</span>
        <span className={gameStyles.narrowText}>m6</span>
      </>
    ),
    title: "8 semitones",
    clickHandler: () => gameStore.intervalClicked([8]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Minor 7th</span>
        <span className={gameStyles.narrowText}>m7</span>
      </>
    ),
    title: "10 semitones",
    clickHandler: () => gameStore.intervalClicked([10]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Octave</span>
        <span className={gameStyles.narrowText}>P8</span>
      </>
    ),
    title: "12 semitones",
    clickHandler: () => gameStore.intervalClicked([12]),
  },
];

const makeDiatonicNumberIntervalsRow = (
  gameStore: GameStore
): IButtonConfig[] => [
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Second</span>
        <span className={gameStyles.narrowText}>2nd</span>
      </>
    ),
    title: "1 or 2 semitones",
    clickHandler: () => gameStore.intervalClicked([1, 2]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Third</span>
        <span className={gameStyles.narrowText}>3rd</span>
      </>
    ),
    title: "3 or 4 semitones",
    clickHandler: () => gameStore.intervalClicked([3, 4]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Fourth</span>
        <span className={gameStyles.narrowText}>4th</span>
      </>
    ),
    title: "5 semitones (P4)",
    clickHandler: () => gameStore.intervalClicked([5, 6]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Fifth</span>
        <span className={gameStyles.narrowText}>5th</span>
      </>
    ),
    title: "7 semitones (P5)",
    clickHandler: () => gameStore.intervalClicked([6, 7]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Sixth</span>
        <span className={gameStyles.narrowText}>6th</span>
      </>
    ),
    title: "8 or 9 semitones",
    clickHandler: () => gameStore.intervalClicked([8, 9]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Seventh</span>
        <span className={gameStyles.narrowText}>7th</span>
      </>
    ),
    title: "10 or 11 semitones",
    clickHandler: () => gameStore.intervalClicked([10, 11]),
  },
  {
    text: (
      <>
        <span className={gameStyles.wideText}>Octave</span>
        <span className={gameStyles.narrowText}>Octave</span>
      </>
    ),
    title: "12 semitones",
    clickHandler: () => gameStore.intervalClicked([12]),
  },
];

const makeTrumpetRow = (gameStore: GameStore): IButtonConfig[] => [
  { text: "•••", clickHandler: () => gameStore.trumpetClicked("•••") },
  { text: "•◦•", clickHandler: () => gameStore.trumpetClicked("•◦•") },
  { text: "◦••", clickHandler: () => gameStore.trumpetClicked("◦••") },
  { text: "••◦", clickHandler: () => gameStore.trumpetClicked("••◦") },
  { text: "•◦◦", clickHandler: () => gameStore.trumpetClicked("•◦◦") },
  { text: "◦•◦", clickHandler: () => gameStore.trumpetClicked("◦•◦") },
  { text: "◦◦◦", clickHandler: () => gameStore.trumpetClicked("◦◦◦") },
];

export const ControlsBottom = observer(function ControlsBottom(props: {
  gameStore: GameStore;
}) {
  const { gameStore } = props;
  const { gameType, intervalsGameType } = gameStore.state;
  return (
    <>
      {gameType === "notesKeyboardTrad" && (
        <div>
          <Keyboard
            store={gameStore}
            pageType={"game"}
            keyboardType={"keyboard-trad"}
          />
        </div>
      )}
      {gameType === "notesKeyboardSixSix" && (
        <div>
          <Keyboard
            store={gameStore}
            pageType={"game"}
            keyboardType={"keyboard-six-six"}
          />
        </div>
      )}
      {gameType === "notesGuitar" && (
        <div>
          <Fretboard
            store={gameStore}
            pageType={"game"}
            fretboardConfig={guitarFretboardConfig}
          />
        </div>
      )}
      {gameType === "notesViolin" && (
        <div>
          <Fretboard
            store={gameStore}
            pageType={"game"}
            fretboardConfig={violinFretboardConfig}
          />
        </div>
      )}
      {gameType === "notesTrumpet" && (
        <div
          className={`field has-addons has-addons-centered ${utilStyles.flexWrapWrap}`}
        >
          {makeTrumpetRow(gameStore).map(
            ({ text, title, clickHandler }, index) => (
              <div className={"control"} key={index + "Button"}>
                <button
                  title={title}
                  onClick={clickHandler}
                  className={`button ${utilStyles.marginBottomMinusOnePx}`}
                >
                  <span className={gameStyles.trumpetButtonText}>{text}</span>
                </button>
              </div>
            )
          )}
        </div>
      )}
      {gameType === "intervals" && (
        <>
          {intervalsGameType === "oddIntervals" && (
            <div className={"field has-addons has-addons-centered"}>
              {makeOddIntervalsRow(gameStore).map(
                ({ text, title, clickHandler }, index) => (
                  <div className="control" key={index + "Button"}>
                    <button
                      title={title}
                      onClick={clickHandler}
                      className={`button is-small ${avStyles.chromaticIntervalButton}`}
                    >
                      {text}
                    </button>
                  </div>
                )
              )}
            </div>
          )}
          {intervalsGameType === "evenIntervals" && (
            <div className={"field has-addons has-addons-centered"}>
              {makeEvenIntervalsRow(gameStore).map(
                ({ text, title, clickHandler }, index) => (
                  <div className="control" key={index + "Button"}>
                    <button
                      title={title}
                      onClick={clickHandler}
                      className={`button is-small ${avStyles.chromaticIntervalButton}`}
                    >
                      {text}
                    </button>
                  </div>
                )
              )}
            </div>
          )}
          {intervalsGameType === "allIntervals" && (
            <>
              <div className={avStyles.chromaticIntervalButtonsTopRow}>
                <div className={"field has-addons has-addons-centered"}>
                  {makeOddIntervalsRow(gameStore).map(
                    ({ text, title, clickHandler }, index) => (
                      <div className="control" key={index + "Button"}>
                        <button
                          title={title}
                          onClick={clickHandler}
                          className={`button is-small ${avStyles.chromaticIntervalButton}`}
                        >
                          {text}
                        </button>
                      </div>
                    )
                  )}
                </div>
              </div>
              <div className={avStyles.chromaticIntervalButtonsBottomRow}>
                <div className={"field has-addons has-addons-centered"}>
                  {makeEvenIntervalsRow(gameStore).map(
                    ({ text, title, clickHandler }, index) => (
                      <div className="control" key={index + "Button"}>
                        <button
                          title={title}
                          onClick={clickHandler}
                          className={`button is-small ${avStyles.chromaticIntervalButton}`}
                        >
                          {text}
                        </button>
                      </div>
                    )
                  )}
                </div>
              </div>
            </>
          )}
          {intervalsGameType === "diatonicNumberIntervals" && (
            <div className={"field has-addons has-addons-centered"}>
              {makeDiatonicNumberIntervalsRow(gameStore).map(
                ({ text, title, clickHandler }, index) => (
                  <div className="control" key={index + "Button"}>
                    <button
                      title={title}
                      onClick={clickHandler}
                      className={`button is-small ${avStyles.diatonicNumberIntervalButton} ${utilStyles.marginBottomMinusOnePx}`}
                    >
                      {text}
                    </button>
                  </div>
                )
              )}
            </div>
          )}
        </>
      )}
    </>
  );
});
