import { observable, action, computed, configure, makeObservable } from "mobx";

import {
  generateNoteTargets,
  generateIntervalTargets,
  getNotesForIntervals,
} from "./store-functions";

import { iota, randomInteger, closestNumber } from "../../../js/utils";
import { makePiano, Piano } from "../AudioVisualizer/audio-load";

import { getXCoordinates } from "../AudioVisualizer/store-functions";
import {
  IKeyboardType,
  INoteSeries,
  INoteSeriesCaption,
} from "../AudioVisualizer/AudioVisualizerStore";

configure({ enforceActions: "observed" });

const oneOctaveNaturals = [40, 42, 44, 45, 47, 49, 51];
const oneOctaveSharpsFlats = [41, 43, 46, 48, 50];

const keyboardTwoOctavesNaturals = [
  40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57, 59, 61, 63,
];
const keyboardNoteSets = {
  oneOctaveNaturals,
  oneOctaveSharpsFlats,
  oneOctaveAll: oneOctaveNaturals,
  twoOctavesNaturals: keyboardTwoOctavesNaturals,
  twoOctavesSharpsFlats: [41, 43, 46, 48, 50, 53, 55, 58, 60, 62],
  twoOctavesAll: keyboardTwoOctavesNaturals,
};

const guitarTwoOctavesNaturals = [
  32, 33, 35, 37, 39, 40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57, 59,
];
const guitarNoteSets = {
  oneOctaveNaturals,
  oneOctaveSharpsFlats,
  oneOctaveAll: oneOctaveNaturals,
  twoOctavesNaturals: guitarTwoOctavesNaturals,
  twoOctavesSharpsFlats: [34, 36, 38, 41, 43, 46, 48, 50, 53, 55, 58, 60],
  twoOctavesAll: guitarTwoOctavesNaturals,
};

const violinTwoOctavesNaturals = [
  35, 37, 39, 40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57, 59, 61,
];
const violinNoteSets = {
  oneOctaveNaturals,
  oneOctaveSharpsFlats,
  oneOctaveAll: oneOctaveNaturals,
  twoOctavesNaturals: violinTwoOctavesNaturals,
  twoOctavesSharpsFlats: [36, 38, 41, 43, 46, 48, 50, 53, 55, 58, 60, 62],
  twoOctavesAll: violinTwoOctavesNaturals,
};

const trumpetTwoOctavesNaturals = [
  32, 33, 35, 37, 39, 40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57,
];
const trumpetNoteSets = {
  oneOctaveNaturals,
  oneOctaveSharpsFlats,
  oneOctaveAll: oneOctaveNaturals,
  twoOctavesNaturals: trumpetTwoOctavesNaturals,
  twoOctavesSharpsFlats: [34, 36, 38, 41, 43, 46, 48, 50, 53, 55],
  twoOctavesAll: trumpetTwoOctavesNaturals,
};

const intervalSets = {
  evenIntervals: [2, 4, 6, 8, 10, 12],
  oddIntervals: [1, 3, 5, 7, 9, 11],
  allIntervals: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  // Omit tritones (6) from diatonic number intervals to keep things simple.
  diatonicNumberIntervals: [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12],
};

const noteCaptions = [
  "G# or Ab",
  "A",
  "A# or Bb",
  "B",
  "C",
  "C# or Db",
  "D",
  "D# or Eb",
  "E",
  "F",
  "F# or Gb",
  "G",
];

const intervalCaptions = [
  "Unison",
  "Minor 2nd",
  "Major 2nd",
  "Minor 3rd",
  "Major 3rd",
  "Perfect 4th",
  "Tritone",
  "Perfect 5th",
  "Minor 6th",
  "Major 6th",
  "Minor 7th",
  "Major 7th",
  "Octave",
];

const trumpetValvesToNotes = new Map([
  ["•••", [32, 39]], // e, b,
  ["•◦•", [33, 40]], // f, c
  ["◦••", [34, 41, 46, 58]],
  ["••◦", [35, 42, 47, 51, 59]], // g, d, g, b, g
  ["•◦◦", [36, 43, 48, 52, 55, 60]],
  ["◦•◦", [37, 44, 49, 53, 56, 61]], // a, e, a, e, a
  ["◦◦◦", [38, 45, 50, 54, 57, 62]],
]);

/**
 * Return the closest note to the target note.
 */
const noteFromTrumpetValves = (
  valves: string,
  noteTarget: number
): number | undefined => {
  const notes = trumpetValvesToNotes.get(valves);

  return notes?.reduce((closest, current) =>
    Math.abs(closest - noteTarget) < Math.abs(current - noteTarget)
      ? closest
      : current
  );
};

const trumpetNotesToValves = new Map([
  [32, "•••"], // e
  [33, "•◦•"], // f
  [34, "◦••"],
  [35, "••◦"], // g
  [36, "•◦◦"],
  [37, "◦•◦"], // a
  [38, "◦◦◦"], // b-flat
  [39, "•••"], // b
  [40, "•◦•"], // c
  [41, "◦••"],
  [42, "••◦"], // d
  [43, "•◦◦"],
  [44, "◦•◦"], // e
  [45, "◦◦◦"], // f
  [46, "◦••"],
  [47, "••◦"], // g
  [48, "•◦◦"],
  [49, "◦•◦"], // a
  [50, "◦◦◦"], // b-flat
  [51, "••◦"], // b
  [52, "•◦◦"], // c
  [53, "◦•◦"],
  [54, "◦◦◦"], // d
  [55, "•◦◦"],
  [56, "◦•◦"], // e
  [57, "◦◦◦"], // f
  [58, "◦••"],
  [59, "••◦"], // g
  [60, "•◦◦"],
  [61, "◦•◦"], // a
  [62, "◦◦◦"], // b-flat
  // "63", "◦◦◦" // b (unused)
]);

const secondsPerGame = 60;
const gameLeftMarginDefault = 4.3;
const noteXOffsetDefault = 5.3;
const correctCaptionColor = "green";
const incorrectCaptionColor = "#E3701A";

export type IGameNoteSeries = Omit<INoteSeries, "xOffsets" | "delays">;

export type GameType =
  | "notesKeyboardTrad"
  | "notesKeyboardSixSix"
  | "notesGuitar"
  | "notesViolin"
  | "notesTrumpet"
  | "intervals";

export type NotesGameType =
  | "oneOctaveNaturals"
  | "oneOctaveSharpsFlats"
  | "oneOctaveAll"
  | "twoOctavesNaturals"
  | "twoOctavesSharpsFlats"
  | "twoOctavesAll";

export type IntervalsGameType =
  | "evenIntervals"
  | "oddIntervals"
  | "allIntervals"
  | "diatonicNumberIntervals";

type ViewWidth = "small" | "large";

interface GameStoreState {
  gameType: GameType;
  notesGameType: NotesGameType;
  intervalsGameType: IntervalsGameType;

  targetsAtATime: number;
  keyboardType: IKeyboardType;

  rightTally: number;
  wrongTally: number;
  secondsLeft: number;
  feedbackText: string;

  gameStatus: "on" | "off" | "onButAttemptsOff";
  currentTarget: number;
  noteTargets: number[];
  intervalTargets: number[];
  targetsOnStaff: number;

  targetIndexesToDraw: number[];
  attemptsToDraw: number[][];
  attemptCaptions: INoteSeriesCaption[];

  volume: number;
  viewWidth: ViewWidth;
}

export class GameStore {
  feedbackTimer: number | undefined;
  staffTimer: number | undefined;
  targetsTimer: number | undefined;
  countdownTimer: number | undefined;
  mediaQueryList: MediaQueryList | undefined;
  piano: Piano;

  // @observable
  state: GameStoreState = {
    // UI game selection state
    gameType: "notesKeyboardTrad",
    notesGameType: "oneOctaveNaturals",
    intervalsGameType: "evenIntervals",
    // number of target notes or intervals at a time on the staff
    targetsAtATime: 4,
    keyboardType: "Traditional",

    // UI-output visible current game state
    rightTally: 0,
    wrongTally: 0,
    secondsLeft: 0,
    feedbackText: "",

    // internal current game state
    // gameStatus is: off, on, onButAttemptsOff
    gameStatus: "off",
    currentTarget: 0,
    noteTargets: [],
    intervalTargets: [],
    targetsOnStaff: 4,

    targetIndexesToDraw: [],
    attemptsToDraw: [],
    attemptCaptions: [],

    volume: 0.35,
    viewWidth: "small",
  };

  constructor() {
    makeObservable(this, {
      state: observable,
      targetsNoteSeries: computed,
      attemptsNoteSeries: computed,
      attemptIndexesToDraw: computed,
      xCoordinates: computed,
      totalTally: computed,
      noteSource: computed,
      intervalSource: computed,
      keyboardRange: computed,

      changeGameType: action,
      changeNotesGameType: action,
      changeIntervalsGameType: action,
      gameStarted: action,
      gameStarted2: action,
      gameStarted3: action,
      countItDown: action,
      endGame: action,
      endGameClearStaves: action,
      instrumentNotePlayed: action,
      intervalClicked: action,
      trumpetClicked: action,
      handleAttempt: action,
      clearLastCaption: action,
      clearBothStaves: action,
      loadNextTargets: action,
      showTargets: action,

      setTargetsAtATime: action,
      setKeyboardType: action,
      setVolume: action,
      setViewWidth: action,
    });

    this.piano = makePiano();

    // Adapt to changes in the viewport width. Set the initial viewWidth.
    // Check if `window` is defined (so if in the browser or in node.js), to
    // prevent an error when running `gatsby build`, which is a node.js context.
    if (typeof window !== "undefined") {
      this.mediaQueryList = window.matchMedia("(min-width: 425px)");
      this.mediaQueryList.addEventListener("change", (event) =>
        this.setViewWidth(event.matches)
      );
      this.setViewWidth(this.mediaQueryList.matches);
    }
  }

  // @computed
  get targetsNoteSeries(): IGameNoteSeries {
    return this.state.gameType === "intervals"
      ? {
          yPositions: this.state.noteTargets.map((x, i) => [
            x,
            x + this.state.intervalTargets[i],
          ]),
          captions: [],
        }
      : {
          yPositions: this.state.noteTargets.map((x) => [x]),
          captions: [],
        };
  }

  // @ computed
  get attemptsNoteSeries(): IGameNoteSeries {
    return {
      yPositions: this.state.attemptsToDraw,
      captions: this.state.attemptCaptions,
    };
  }

  // @computed
  get attemptIndexesToDraw() {
    return iota(this.state.attemptsToDraw.length);
  }

  // @computed
  get xCoordinates() {
    return getXCoordinates(
      [0, 1, 2, 3, 4, 5, 6, 7, 8],
      Array(9).fill(noteXOffsetDefault),
      gameLeftMarginDefault
    );
  }

  // @computed
  get totalTally() {
    return this.state.rightTally - this.state.wrongTally;
  }

  // @computed
  get noteSource() {
    const { gameType, notesGameType } = this.state;
    switch (gameType) {
      case "intervals":
        return oneOctaveNaturals;
      case "notesKeyboardTrad":
        return keyboardNoteSets[notesGameType];
      case "notesKeyboardSixSix":
        return keyboardNoteSets[notesGameType];
      case "notesGuitar":
        return guitarNoteSets[notesGameType];
      case "notesViolin":
        return violinNoteSets[notesGameType];
      case "notesTrumpet":
        return trumpetNoteSets[notesGameType];
    }
  }

  // @computed
  get intervalSource() {
    return intervalSets[this.state.intervalsGameType];
  }

  // @computed
  get keyboardRange() {
    const isTwoOctaveGame = [
      "twoOctavesNaturals",
      "twoOctavesSharpsFlats",
      "twoOctavesAll",
    ].includes(this.state.notesGameType);

    return isTwoOctaveGame && this.state.viewWidth === "large"
      ? {
          // Two octaves from C (40) to B (63).
          lowestNote: 40,
          highestNote: 63,
        }
      : {
          // One octave from C (40) to B (51).
          lowestNote: 40,
          highestNote: 51,
        };
  }

  // ACTIONS

  changeGameType(type: GameType) {
    this.endGameClearStaves();
    this.state.gameType = type;
  }

  changeNotesGameType(type: NotesGameType) {
    this.endGameClearStaves();
    this.state.notesGameType = type;
  }

  changeIntervalsGameType(type: IntervalsGameType) {
    this.endGameClearStaves();
    this.state.intervalsGameType = type;
  }

  // @action
  gameStarted() {
    this.state.gameStatus = "on";
    this.state.rightTally = 0;
    this.state.wrongTally = 0;

    this.clearBothStaves(this);

    this.state.currentTarget = 0;
    this.state.targetsOnStaff = 0;
    this.state.secondsLeft = secondsPerGame;

    // generate note targets
    if (this.state.gameType === "intervals") {
      this.state.intervalTargets = generateIntervalTargets(
        this.intervalSource,
        120
      );
      this.state.noteTargets = getNotesForIntervals(
        this.noteSource,
        this.state.intervalTargets
      );
    } else {
      this.state.noteTargets = generateNoteTargets(
        this.noteSource,
        this.state.notesGameType,
        120
      );
    }

    this.state.feedbackText = "Ready...";
    this.feedbackTimer = window.setTimeout(
      this.gameStarted2.bind(undefined, this),
      1000
    );
  }

  // @action
  gameStarted2(store: GameStore) {
    store.state.feedbackText = "Go!";
    store.feedbackTimer = window.setTimeout(
      store.gameStarted3.bind(undefined, store),
      500
    );
  }

  // @action
  gameStarted3(store: GameStore) {
    store.state.feedbackText = " ";
    store.showTargets(store);
    store.countdownTimer = window.setInterval(
      store.countItDown.bind(undefined, store),
      1000
    );
  }

  // @action
  countItDown(store: GameStore) {
    store.state.secondsLeft -= 1;
    if (store.state.secondsLeft === 0) {
      store.endGame();
      store.state.feedbackText = "Game Over — Score: " + store.totalTally;
    }
  }

  // @action
  endGame() {
    clearTimeout(this.staffTimer);
    clearTimeout(this.targetsTimer);
    clearTimeout(this.countdownTimer);
    clearTimeout(this.feedbackTimer);
    this.state.gameStatus = "off";
    this.state.secondsLeft = 0;
    this.state.feedbackText = " ";
  }

  // @action
  endGameClearStaves() {
    // Stop button clicked or different game selected.
    this.endGame();
    this.clearBothStaves(this);
    this.state.rightTally = 0;
    this.state.wrongTally = 0;
  }

  // @action
  instrumentNotePlayed(note: number) {
    if (this.state.gameStatus !== "on") {
      // if game is not fully on, let them play music
      this.piano.play(note);
      return;
    }

    const targetNote = this.state.noteTargets[this.state.currentTarget];
    const exactlyCorrect = note === targetNote;

    // When playing 2 octave keyboard games on small screens, we show only one
    // octave of keyboard. Then the note played does not have to match the
    // octave of the target note to be correct.  E.g. playing c1 is correct for
    // a c2 target.
    const correct =
      exactlyCorrect ||
      (this.state.viewWidth === "small" &&
        (this.state.gameType === "notesKeyboardTrad" ||
          this.state.gameType === "notesKeyboardSixSix") &&
        note + 12 === targetNote);

    const adjustedNote = correct && !exactlyCorrect ? note + 12 : note;

    const message = correct
      ? "Yes! " + noteCaptions[note % 12]
      : "Try again...";

    // Could be used to give hints, instead of just try again.
    // note < state.noteTargets[this.state.currentTarget] ? "Too low..." : "Too high...";

    this.handleAttempt(correct, adjustedNote, message, undefined);
  }

  // @action
  intervalClicked(intervals: number[]) {
    if (this.state.gameStatus !== "on") {
      // if game is not fully on, let them play some intervals
      // TODO: this isn't working for intervals for some reason.
      const index = randomInteger(intervals.length);
      const note = 7 + intervals[index];
      this.piano.play(7);
      this.piano.play(note);
      return;
    }

    const noteTarget = this.state.noteTargets[this.state.currentTarget];
    const intervalTarget = this.state.intervalTargets[this.state.currentTarget];
    const isCorrect = intervals.includes(intervalTarget);

    const message = isCorrect
      ? "Yes! " + intervalCaptions[intervalTarget]
      : "Try again...";

    // Could be used to give hints, instead of just try again.
    // interval < this.state.intervalTargets[this.state.currentTarget] ? "Too small..." : "Too large...";

    this.handleAttempt(
      isCorrect,
      noteTarget,
      message,
      isCorrect ? intervalTarget : closestNumber(intervals, intervalTarget)
    );
  }

  // @action
  trumpetClicked(valves: string) {
    if (this.state.gameStatus !== "on") {
      // if game is not on, let them play the trumpet
      const note = noteFromTrumpetValves(valves, 42);
      if (note) {
        this.piano.play(note);
      }
      return;
    }
    const noteTarget = this.state.noteTargets[this.state.currentTarget];
    const targetValves = trumpetNotesToValves.get(noteTarget);
    const correct = valves === targetValves;

    if (correct) {
      const message = "Yes! " + noteCaptions[noteTarget % 12] + "  " + valves;
      this.handleAttempt(correct, noteTarget, message);
    } else {
      const wrongNote = noteFromTrumpetValves(valves, noteTarget);
      // Could be used to give hints.
      // const message = wrongNote < noteTarget ? "Too low..." : "Too high...";
      if (wrongNote) {
        this.handleAttempt(correct, wrongNote, "Try again...");
      }
    }
  }

  // @action
  handleAttempt(
    correct: boolean,
    note: number,
    message: string,
    interval?: number
  ) {
    const notesArray = interval ? [note, note + interval] : [note];

    const caption = {
      text: message,
      fill: correct ? correctCaptionColor : incorrectCaptionColor,
      fontWeight: "bold",
    };

    // These indexes are positions on the staff.
    const attemptIndex = this.state.attemptsToDraw.length - 1;

    const targetIndex =
      this.state.currentTarget - this.state.targetIndexesToDraw[0];

    const firstAttempt = attemptIndex < targetIndex;

    if (firstAttempt) {
      this.state.attemptsToDraw.push(notesArray);
      this.state.attemptCaptions.push(caption);
    } else {
      this.state.attemptsToDraw[attemptIndex] = notesArray;
      this.state.attemptCaptions[attemptIndex] = caption;
    }

    if (interval) {
      this.piano.play(note);
      this.piano.play(note + interval);
    } else {
      this.piano.play(note);
    }

    if (correct) {
      this.state.rightTally += 1;
      this.state.currentTarget += 1;
      // reload the target staff if needed
      if (this.state.attemptsToDraw.length >= this.state.targetsOnStaff) {
        // Briefly turn game off so no attempts are registered while reloading
        // the targets on the staff.
        this.state.gameStatus = "onButAttemptsOff";
        this.staffTimer = window.setTimeout(
          this.clearBothStaves.bind(undefined, this),
          600
        );
        this.targetsTimer = window.setTimeout(
          this.loadNextTargets.bind(undefined, this),
          850
        );
      } else {
        // clear the note caption
        const lastCaption = this.state.attemptCaptions.length - 1;

        this.feedbackTimer = window.setTimeout(
          this.clearLastCaption.bind(undefined, this, lastCaption),
          800
        );
      }
    } else {
      // incorrect
      this.state.wrongTally += 1;
    }
  }

  // @action
  clearLastCaption(store: GameStore, index: number) {
    store.state.attemptCaptions[index] = { text: "" };
    // MobX says check length to avoid reading non-existent array slots.
    // if (store.state.attemptCaptions.length >= index + 1) { ... }
  }

  // @action
  clearBothStaves(store: GameStore) {
    store.state.targetIndexesToDraw = [];
    store.state.attemptsToDraw = [];
    store.state.attemptCaptions = [];
  }

  // @action
  loadNextTargets(store: GameStore) {
    store.state.gameStatus = "on";
    store.showTargets(store);
  }

  // @action
  showTargets(store: GameStore) {
    store.state.targetsOnStaff = store.state.targetsAtATime;
    const indexes = iota(store.state.targetsAtATime, store.state.currentTarget);
    store.state.targetIndexesToDraw = indexes;
  }

  setTargetsAtATime(targetCount: number) {
    this.state.targetsAtATime = targetCount;
  }

  setKeyboardType(type: IKeyboardType) {
    this.state.keyboardType = type;
  }

  setVolume(volume: number) {
    this.piano.volume(volume);
    this.state.volume = volume;
  }

  setViewWidth(isLarge: boolean) {
    const width = isLarge ? "large" : "small";
    this.state.viewWidth = width;
  }
}
