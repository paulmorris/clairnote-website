import React, { useState } from "react";

export const ContactUsModal = (props: { children: JSX.Element }) => {
  const [isActive, setIsActive] = useState(false);

  const toggleIsActive = (event: React.MouseEvent) => {
    event.preventDefault();
    setIsActive((prev) => !prev);
  };

  return (
    <>
      <div onClick={toggleIsActive}>{props.children}</div>

      {isActive && (
        <div className={"modal is-active"}>
          <div className="modal-background" onClick={toggleIsActive}></div>
          <div className="modal-content">
            <div className="box">
              <div className="block">
                <h3 className="title is-3">Contact Us</h3>
              </div>
              <div className="block">
                Feel free to reach out with questions, comments, or for any
                reason.
              </div>
              <div className="block">Please send an email to:</div>
              <div className="block">[...] at [...] dot org</div>
              <div className="block">
                Replace both of the missing parts with "clairnote".
              </div>
              <div className="block">(Obfuscated to thwart spam bots.)</div>
            </div>
          </div>
          <button
            className="modal-close is-large"
            aria-label="close"
            onClick={toggleIsActive}
          ></button>
        </div>
      )}
    </>
  );
};
