import React, { useState } from "react";
import { withPrefix } from "gatsby";

import * as pageStyles from "../global-styles/page.module.css";

// https://stackoverflow.com/questions/29291688/video-displayed-in-reactjs-component-not-updating#32215374

export function VideoKiosk(props: { fileName: string; dn: boolean }) {
  const [fileName, setFileName] = useState(props.fileName);

  const clairnoteType = props.dn ? "DN" : "SN";
  const src = withPrefix("video/" + fileName + "-" + clairnoteType);
  const webmSrc = src + ".webm";
  const mp4Src = src + ".mp4";

  return (
    <>
      {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
      <video
        style={{ height: "360px", marginBottom: "1.8em" }}
        className={pageStyles.videoBoxed}
        controls={true}
        preload="auto"
        width="640"
        height="360"
        key={fileName}
      >
        <source src={webmSrc} type="video/webm" />
        <source src={mp4Src} type="video/mp4" />
        Your browser does not support the <code>video</code> element.
      </video>

      <div className="buttons is-centered">
        <button
          className={`button is-small ${pageStyles.buttonLight}`}
          onClick={() => setFileName("Strauss-Blue-Danube-Waltz-Clairnote")}
        >
          The Blue Danube Waltz, by J.J. Strauss
        </button>
        <button
          className={`button is-small ${pageStyles.buttonLight}`}
          onClick={() => setFileName("Somewhere-Over-the-Rainbow-Clairnote")}
        >
          Somewhere Over the Rainbow
        </button>
        <button
          className={`button is-small ${pageStyles.buttonLight}`}
          onClick={() => setFileName("Do-Re-Mi-Clairnote")}
        >
          Do Re Mi from the Sound of Music
        </button>
        <button
          className={`button is-small ${pageStyles.buttonLight}`}
          onClick={() => setFileName("Traditional-Greensleeves-Clairnote")}
        >
          Greensleeves
        </button>
        <button
          className={`button is-small ${pageStyles.buttonLight}`}
          onClick={() => setFileName("Bach-WTK1-Prelude1-Clairnote")}
        >
          The Well Tempered Clavier I, Prelude I, by J.S. Bach
        </button>
        <button
          className={`button is-small ${pageStyles.buttonLight}`}
          onClick={() => setFileName("Beethoven-Fuer-Elise-Clairnote")}
        >
          Für Elise by L.V. Beethoven
        </button>
        <button
          className={`button is-small ${pageStyles.buttonLight}`}
          onClick={() => setFileName("Joplin-Entertainer-Clairnote")}
        >
          The Entertainer by Scott Joplin
        </button>
      </div>
    </>
  );
}
