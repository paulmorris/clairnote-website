import { Link } from "gatsby";
import React from "react";
import { PageLayoutProps } from "../../types/types";

export const SnDnSwitch = (props: PageLayoutProps & { className?: string }) => {
  // Remove "/dn/" or "/" (4 or 1) from the front of pathname.
  const start = props.dn ? 4 : 1;
  const to = props.otherUrlDir + props.location.pathname.slice(start);

  const text = props.dn ? "DN->SN" : "SN->DN";

  const title = props.dn
    ? "Switch from Clairnote DN to SN"
    : "Switch from Clairnote SN to DN";

  return (
    <Link className={props.className} to={to} title={title}>
      {text}
    </Link>
  );
};
