import React from "react";
import { Link } from "gatsby";
import { PageLayoutProps } from "../../types/types";
import * as styles from "./Footer.module.css";

const snColor = "#547878"; // hsl(180, 17.6%, 40%)
const snColorDarker = "hsl(180, 17.6%, 30%)";

const dnColor = "#545578"; // hsl(238.3, 17.6%, 40%)
const dnColorDarker = "hsl(238.3, 17.6%, 30%)";

export const Footer = ({
  sn,
  urlDir,
  clairnoteName,
  isBlog,
  otherUrlDir,
  otherClairnoteName,
  dn,
  location,
}: PageLayoutProps) => (
  <footer>
    <div
      className={`section ${styles.footerSection} ${styles.footerSectionVeryLight}`}
    >
      <div className="container">
        <nav className="level">
          <div className={`level-left ${styles.buttonLinksPlacement}`}>
            <div className="level-item">
              <div className={`${styles.footerSectionTitle}`}>
                Follow Clairnote
              </div>
            </div>
            <div className="level-item">
              <Link
                className={`button is-ghost ${""}`}
                title="Subscribe to Get Occasional Updates"
                to="/newsletter/"
              >
                Email Newsletter
              </Link>
            </div>
            <div className="level-item">
              <a
                className={`button is-ghost ${""}`}
                title="Clairnote on Mastodon"
                href="https://mastodon.social/@clairnote"
                target="_blank"
                rel="external noopener noreferrer"
              >
                Mastodon
              </a>
            </div>
            <div className="level-item">
              <a
                className={`button is-ghost ${""}`}
                title="Clairnote on Facebook"
                href="https://www.facebook.com/ClairnoteMusicNotation"
                target="_blank"
                rel="external noopener noreferrer"
              >
                Facebook
              </a>
            </div>
          </div>
        </nav>
      </div>
    </div>

    <div
      className={`section ${styles.footerSection} ${styles.footerSectionDark}`}
      style={{
        backgroundColor: sn ? snColor : dnColor,
      }}
      role="contentinfo"
    >
      <div className="container">
        <div className="level">
          <div className={`level-left ${styles.buttonLinksPlacement}`}>
            <div className="level-item">
              <div className={`${styles.footerSectionTitle}`}>
                Questions or Comments?
              </div>
            </div>
            <div className="level-item">
              <Link
                to={urlDir + "contact/"}
                title={"Contact Us"}
                className={`button is-ghost ${styles.buttonLink}`}
              >
                Contact Us
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div
      className={`section ${styles.footerSection} ${styles.footerSectionDark}`}
      style={{
        backgroundColor: sn ? snColorDarker : dnColorDarker,
      }}
      role="contentinfo"
    >
      <div className="container">
        <nav className="level">
          <div className={`level-left ${styles.buttonLinksPlacement}`}>
            <div className="level-item">
              <Link
                className={`button is-ghost ${styles.buttonLink}`}
                title="Search clairnote.org"
                to={urlDir + "search/"}
              >
                Search this Site
              </Link>
            </div>
            <div className="level-item">
              <Link
                to={
                  isBlog
                    ? // On blog pages we go to the DN home page.
                      otherUrlDir
                    : // Remove "/dn/" or "/" (4 or 1) from the front of pathname.
                      otherUrlDir + location.pathname.slice(dn ? 4 : 1)
                }
                title={`Switch to ${otherClairnoteName} Website`}
                className={`button is-ghost ${styles.buttonLink}`}
              >
                {/* The arrow is unicode: U+279E */}
                {`${clairnoteName.slice(
                  clairnoteName.length - 2
                )} ➞ ${otherClairnoteName.slice(
                  otherClairnoteName.length - 2
                )}`}
              </Link>
            </div>
            <div className="level-item">
              <a
                className={`button is-ghost ${styles.buttonLink}`}
                href="#page"
              >
                Top of Page
              </a>
            </div>
          </div>
        </nav>
      </div>
    </div>

    <div
      className={`section ${styles.footerSection} ${styles.footerSectionDark}`}
      style={{
        backgroundColor: sn ? snColorDarker : dnColorDarker,
      }}
    >
      <div className="container">
        <p className={`fine-print ${styles.finePrint}`}>
          {clairnoteName} music notation and except where otherwise noted the
          content of the clairnote.org website are licensed under a{" "}
          <a
            href="http://creativecommons.org/licenses/by-sa/4.0/"
            target="_blank"
            rel="license external noopener noreferrer"
          >
            Creative Commons Attribution-ShareAlike 4.0 International License
          </a>
          . See also{" "}
          <a
            href="http://musicnotation.org"
            target="_blank"
            rel="external noopener noreferrer"
          >
            The Music Notation Project
          </a>{" "}
          and{" "}
          <a
            href="https://twinnote.clairnote.org"
            target="_blank"
            rel="external noopener noreferrer"
          >
            TwinNote Music Notation
          </a>
          .
        </p>
      </div>
    </div>
  </footer>
);
