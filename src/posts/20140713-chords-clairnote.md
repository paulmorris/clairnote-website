---
title: "Chords in Clairnote"
link: https://clairnote.org/blog/2014/07/chords-clairnote/
author: paulmorris
special_byline: ""
description:
post_id: 2683
date: 2014/07/13
created_gmt: 2014/07/13
comment_status: closed
slug: chords-clairnote
draft: false
post_type: post
tags: ["Clairnote Website"]
---

Have you ever wondered what chords look like in Clairnote? Including the more complex chords used in Jazz music? A new [Chords page](/dn/chords/) has been added to this site that illustrates all the major and minor triads in Clairnote, in root position, first inversion, and second inversion. It also includes an illustration of 46 more complex "jazz" chords, complete with audio playback.

In Clairenote intervals are easy to recognize, and this makes the interval patterns in chords easy to see and understand. For example, it is easy to see the difference between a major triad and a minor triad. (Something you cannot do in traditional notation where they look the same.) This aspect of Clairnote is particularly helpful for reading and understanding the more complex harmonies found in jazz music.

The [Intervals page](/dn/intervals/) illustrates the intervals that are the building blocks of chords. This page now includes links to two new PDF files that show the intervals arranged in a 4x3 grid pattern in order to highlight similarities in their appearance.
