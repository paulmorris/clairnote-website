---
title: "New Key Signatures"
link: https://clairnote.org/blog/2016/02/new-key-signatures/
author: paulmorris
special_byline: ""
description:
post_id: 3307
date: 2016/02/16
created_gmt: 2016/02/16
comment_status: closed
slug: new-key-signatures
draft: false
post_type: post
tags: ["Music Notation", "LilyPond"]
---

Clairnote now has newly revised key signatures that are simpler, more compact, and easier to read, while providing more information and greater parity with traditional key signatures.  Namely, they now indicate _which_ notes in the key are sharp or flat, and not just _how many_ are sharp or flat.  This greater parity makes it easier to learn both Clairnote and traditional notation, to switch from one to the other, and to use certain aspects of standard music theory with Clairnote.

Here are images of an E major key signature in the old design:

![Clairnote's old E major key signature](/images/Clairnote-keysig-e-major.svg)

And in the new design:

![E major key signature in Clairnote music notation](/images/Clairnote-keysignature-e-major.svg)

The old key signatures indicated how many sharps or flats were in the key by placing a number and one of Clairnote's [accidental signs](/dn/accidental-signs/) above the staff.  This told you where a key was on the [circle/spiral of fifths](https://en.wikipedia.org/wiki/Circle_of_fifths).  However, which notes were sharp or flat was not directly indicated.  This was okay because it is not strictly necessary to know that in Clairnote, unlike with traditional notation and its key signatures.  But assuming you did want to know (sometimes helpful when playing music with others who are using traditional notation), you would have to look at the stack of dots, find the ones that were not the "natural" notes (A, B, C, D, E, F, or G), and those would be the sharps or flats. 

The new key signatures have "tails" connected to the dots that indicate which notes are sharps or flats.  A tail that moves downward (from left to right) indicates a flat note. Conversely, a tail that moves upward indicates a sharp.  You can think of the tails as the trace of the dots’ movement as they moved into position in the stack of dots (either up/sharp or down/flat).  Kind of like the tails of meteors in the night sky.  This symbolism is similar to Clairnote’s [accidental signs](/dn/accidental-signs/). 

Another benefit of the new design is that now the tonic note is always the lowest dot and the pattern of solid and hollow dots indicates whether the key is major, minor, or modal.  In the old design the tonic note dot was a wider oval, and its position in the stack of dots indicated major, minor, or modal keys.  The new design conveys this information in a clearer and more obvious way.

Here are images of an F minor key signature that show this difference.  First the old design where the tonic dot is the second from the top, indicating the key is minor:

![Clairnote's old F minor key signature](/images/Clairnote-keysig-f-minor.svg)

And the new design where the lowest dot indicates the tonic note and the 2-3-2 dot pattern indicates the key is minor:

![F minor key signature in Clairnote music notation](/images/Clairnote-keysignature-f-minor.svg)

Because the tonic dot no longer has to be any wider than the other dots they can all be wider ovals that are easier to read.  Note that to prevent collisions between the dots and the tails, the stacks of dots ascend moving to the right in flat keys and to the left in sharp keys, as you can see in the images above.  This gives sharp and flat key signatures a more distinct and easily recognizable appearance.

Overall the new design is an aesthetic improvement – simpler and more compact, no longer made up of two separate unconnected parts, and no longer adding yet another numerical element above the staff.

The new key signatures are documented on the newly revised [Key Signatures](/dn/key-signatures/) page, including a comprehensive PDF file that shows every possible key signature – major, minor, and modal.

All of the sheet music in the [Sheet Music Library](/dn/sheet-music/) and on the [More Sheet Music](/dn/sheet-music/) page has been updated with the new key signatures.  Also a new version of the <del>clairnote-code.ly</del> clairnote.ly file for producing Clairnote with LilyPond is available on the [Software:LilyPond](/dn/software/) page.  It includes the new key signatures along with several other refinements that will be covered in future posts.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
