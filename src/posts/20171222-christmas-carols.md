---
title: "Christmas Carols"
link: https://clairnote.org/blog/2017/12/christmas-carols/
author: paulmorris
special_byline: ""
description:
post_id: 3780
date: 2017/12/22
created_gmt: 2017/12/22
comment_status: closed
slug: christmas-carols
draft: false
post_type: post
tags: ["Sheet Music"]
---

Willem Feltkamp recently converted a bunch of Christmas carols to Clairnote and shared them on the [Music Notation Project](http://musicnotation.org) forum.  I've updated his LilyPond files to a more recent development version of LilyPond in order to create PDFs using the most recent "clairnote.ly" file.

Here they are:

**[Christmas Carols in Clairnote (PDF)](/more-sheet-music-files/Christmas-Music-Clairnote.pdf)**

The carols are:

Angels we have heard on high  
Away in the Manger  
Deck the Halls  
Hark! The Herald Angels Sing  
Here We Come a-Caroling  
I Heard the Bells on Christmas Day  
Jingle Bells  
Joy to the world  
March of the Kings  
O Christmas Tree  
O Holy Night  
Silent Night  
The First Noel  
We Wish You a Merry Christmas  
While Shepherds Watched Their Flocks

I also created a version in Clairnote SN:

**[Christmas Carols in Clairnote SN (PDF)](https://clairnote.org/more-sheet-music-files/Christmas-Music-Clairnote-SN.pdf)**

What is Clairnote SN you ask?  It's a different “flavor” or variant of Clairnote that I have been experimenting with.  The "SN" stands for "standard noteheads".  Read about it on the  [Clairnote and Clairnote SN](/dn/clairnote-dn-clairnote-sn/) page and check out the new [Clairnote SN](/) section of this website.  Perhaps the new year will bring a blog post dedicated to it as well.

Thanks Willem!  I've enjoyed playing these carols on the piano.  Merry Christmas and Happy Holidays to all.
