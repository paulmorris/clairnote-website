---
title: 'New "NoteTrainer" Application'
link: https://clairnote.org/blog/2016/05/new-notetrainer-application/
author: paulmorris
special_byline: ""
description:
post_id: 3402
date: 2016/05/08
created_gmt: 2016/05/08
comment_status: closed
slug: new-notetrainer-application
draft: false
post_type: post
tags: ["Untagged"]
---

Recently I was checking out [The Jankó Keyboard Group](https://www.facebook.com/groups/janko.piano/) on Facebook and was delighted to learn that Andrey Fidrya had written a nice little application to help people learn how to read Clairnote.  It displays a series of notes, then you play them on your MIDI keyboard (or other MIDI instrument), and it lets you know if you have played the correct notes or not.  Here's what it looks like:

![Screenshot of NoteTrainer MIDI App](/images/notetrainer-midi-app-screenshot.png)

It's called "NoteTrainer" and it runs on Windows, Mac OS X, and GNU/Linux.  I've added a page to this website to make it easy to find:

**[NoteTrainer Application](/notetrainer-application/)**

On that page is a link to the Github page where you can download the most recent version of it.

Many thanks to Andrey Fidrya for writing this application!  It's not often that such pleasant surprises come along, and it's great to see this great addition to the Clairnote "ecosystem."
