---
title: "Handwritten Clairnote"
link: https://clairnote.org/blog/2015/04/handwritten-clairnote/
author: paulmorris
special_byline: ""
description:
post_id: 2869
date: 2015/04/14
created_gmt: 2015/04/14
comment_status: closed
slug: handwritten-clairnote
draft: false
post_type: post
tags: ["Sheet Music"]
---

With all the attention we give to software for Clairnote it's easy to forget that you can also write music in Clairnote by hand. Just grab a pen, print out some [blank staff paper](/dn/blank-staff-paper/), and go to town!

Recently a Clairnote user generously contributed a number of works that he had written out by hand so that we can offer them here on the Clairnote website.  Check them out on the new page for [Handwritten Sheet Music](/dn/handwritten-sheet-music/). They are primarily works for guitar, written on three-octave staves like the one shown below.  Our many thanks to him for this! 

![handwritten-example](/images/handwritten-example.png)

Computer geeks need not fear however, as there is also a new version of the <del>clairnote-code.ly</del> clairnote.ly file that is available from the [Software: LilyPond](/dn/software/) page. It offers a number of improvements that we'll cover in future posts.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
