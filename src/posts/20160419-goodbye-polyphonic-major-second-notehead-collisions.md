---
title: "Goodbye to Polyphonic Major Second Notehead Collisions"
link: https://clairnote.org/blog/2016/04/goodbye-polyphonic-major-second-notehead-collisions/
author: paulmorris
special_byline: ""
description:
post_id: 3367
date: 2016/04/19
created_gmt: 2016/04/19
comment_status: closed
slug: goodbye-polyphonic-major-second-notehead-collisions
draft: false
post_type: post
tags: ["LilyPond"]
---

Previously when there were notes in two different voices that were two semitones apart (e.g. a major second), the noteheads would collide when LilyPond rendered them in Clairnote.  Now thanks to improvements in LilyPond such collisions are a thing of the past.

In Clairnote harmonic notes that are one or two semitones apart (e.g. minor or major seconds) are placed on opposite sides of the stem to avoid notehead collisions.  This is similar to the appearance of seconds in traditional notation.  However, by default LilyPond only treats notes that are _one_ vertical staff position apart as colliding and moves them to prevent the collision.  Over three years ago I wrote some Scheme code for chromatic staff systems like Clairnote that automatically moved notes to the correct side of the stem to prevent such collisions. (At the time it was the most complex thing that I had tackled in my earlier days learning Scheme.)  See for example, the major seconds in the second measure here:

![Seconds and minor thirds in Clairnote music notation](/images/Clairnote-seconds-minor-thirds.svg)

However, my code only worked for harmonic notes in a single voice such as notes in a chord.  In polyphonic music, if notes in different voices happened to be two semitones apart (and therefore two staff positions apart) then LilyPond would not adjust their horizontal position and they would collide or overlap (albeit not all that much in some cases).  Like this:

![Major seconds colliding incorrectly in Clairnote music notation](/images/Clairnote-major-seconds-collisions.svg)

(Fortunately, major second harmonic intervals are more dissonant and so tend to be less common than other intervals that are more consonant.)

I'm now glad to report that this is fixed.  With the current <del>clairnote-code.ly</del> clairnote.ly file and LilyPond 2.19.34 and later, polyphonic notes two semitones apart do not collide, but are spaced like seconds in traditional music notation:

![Major seconds in Clairnote music notation](/images/Clairnote-major-seconds-no-collisions.svg)

![Major seconds in Traditional music notation](/images/Traditional-major-seconds-no-collisions.svg)

Fixing these multi-voice collisions using Scheme code in "user space" turned out to be too complex to be feasible.  It really needed to be done in LilyPond's C++ code.  Eventually, I learned enough about C++ and LilyPond to be able to submit [a patch](https://sourceforge.net/p/testlilyissues/issues/4682/) that introduced a "note-collision-threshold" property that lets users customize how many staff spaces apart notes need to be for LilyPond to consider them to be colliding and to therefore adjust their horizontal position accordingly.  Now this threshold is no longer hard-coded at one staff space.

As a bonus, the Scheme code I wrote for single voices (a little over 100 lines of code, and that's in the more concise latest version) will no longer be needed because the new property applies both to notes in a single voice and to notes in different voices.  (For notes in a single voice the property is overridden on the "Stem" grob and for multiple voices it is overridden on the "NoteCollision" grob.)

So at this point I recommend using the most recent development version of LilyPond (2.19.x).  For now the current clairnote.ly file still has the Scheme code to provide the correct note placement for notes in a single voice – for use with LilyPond 2.18.2.  (This code is only used when needed, depending on the version of LilyPond.)

Note that if you prefer the former behavior for some reason you can get it back with:


    \override Staff.NoteCollision.note-collision-threshold = 1

This concludes this series of posts about recent improvements to the rendering of Clairnote by LilyPond.  As always you can download the most recent version of the clairnote.ly file from the [Software: LilyPond](/dn/software/) page.  Here's to refining all these subtle details!

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
