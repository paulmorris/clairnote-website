---
title: "Clairnote Print Handout"
link: https://clairnote.org/blog/2014/02/clairnote-print-handout/
author: paulmorris
special_byline: ""
description:
post_id: 2332
date: 2014/02/20
created_gmt: 2014/02/20
comment_status: closed
slug: clairnote-print-handout
draft: false
post_type: post
tags: ["Untagged"]
---

This website does a nice job of introducing and promoting Clairnote, but sometimes there's no substitute for print media. So I recently created a handout that introduces Clairnote. It was a challenge to fit everything on two pages (one sheet, front and back) but all the most important points made it in, along with some helpful illustrations (at sufficiently high resolution). If anyone would like to print, share, or otherwise distribute it, here is the [PDF file](/files/Clairnote-handout.pdf). (I still plan to return to the topics mentioned in the previous post, time permitting.)
