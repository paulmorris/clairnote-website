---
title: "Stems and 3-Octave Staves"
link: https://clairnote.org/blog/2015/12/stems-3-octave-staves/
author: paulmorris
special_byline: ""
description:
post_id: 3261
date: 2015/12/21
created_gmt: 2015/12/21
comment_status: closed
slug: stems-3-octave-staves
draft: false
post_type: post
tags: ["LilyPond"]
---

As 2015 hurtles to a close it is a good time to look back at recent improvements to the code for rendering Clairnote with LilyPond.  At this point it seems like a story of diminishing returns for effort expended, perhaps some version of that [80/20 thing](https://en.wikipedia.org/wiki/Pareto_principle), where 80% of the effort goes towards 20% of the functionality, as more marginal "corner cases" take more effort to implement or fix.  (Or maybe it's just the [90/90 rule](https://en.wikipedia.org/wiki/Ninety-ninety_rule)?)  The main focus of this blog post is a case in point – getting the correct default stem direction on 3-octave staves.

Ultimately, this is all good news since it indicates the progress that's been made and a certain maturity in the code and its support for Clairnote, allowing for greater focus on subtle refinements and/or moving on to work on other areas (like the [Sheet Music Library](/dn/sheet-music/)).  There have been a number of improvements this past year, many of them "under the hood".  Rather than recap them here, I'll just link to the blog posts: [Double Stems Revised](/blog/2015/10/double-stems-revised/), [8va / 8vb and Clefs](/blog/2015/09/8va-8vb-and-clefs/), and [Staves of Unusual Size](/blog/2015/04/staves-unusual-size/).  There are a few that still need a post, but here we'll just cover one of them.

## Default Stem Direction on 3-Octave Staves

In traditional notation the default stem direction changes at the vertical midpoint of the staff, with notes on the middle line and above having a down-stem, and those below the middle line having an up-stem.  It is the same on the standard 2-octave Clairnote staff where the middle C ledger line is the vertical midpoint:

![2-octave-stem-direction](/images/2-octave-stem-direction.png)

Previously, for 3-octave staves (that you might use for guitar music) this was not working correctly.  Despite the different midpoint of the staff the stems still changed direction at the note C:

![3-octave-stem-direction-old](/images/3-octave-stem-direction-old.png)

Now this has been fixed so that stems change direction at the midpoint of the staff, at the note F-sharp/G-flat:

![3-octave-stem-direction-new](/images/3-octave-stem-direction-new.png)

This might seem like an easy thing to fix, but in LilyPond there is no direct way to change the point on the staff where stems change their default direction.[1]  Stems are effectively "hard-wired" to change their default direction at position 0, which is assumed to be the midpoint of the staff.  Ties and beams also change their default direction where stems do and are also "hard-wired" to 0.

([1] Well, there is for special staves like Mensural, Petrucci, and Vaticana (look up the "neutral-position" grob property), but only the stem direction is involved, whereas the default direction of ties and beams also needs to change.)

I actually looked into submitting a patch to LilyPond to customize the position on the staff where the default direction of stems, ties, and beams change.  I got quite far with stems, but this effort foundered on the more complex code for ties and especially beams.  The assumption that 0 is the midpoint of the staff, and so positive values are above it and negative below it, are incorporated into LilyPond's code in ways that are not easy to identify or change...

So if you cannot bring a horse to water, sometimes it is easier to bring water to the horse.  What now happens with 3-octave staves is that the positions of the staff lines are changed so that the staff is (re-)centered at position 0, where the stems change default direction.  In addition to changing the staff lines, the clef settings also have to be changed so that the notes continue to appear at their correct positions on the staff.  Luckily we can change these clef settings "on the fly" based on the staff – whether it spans an odd or even number of octaves.  The code that can dynamically change traditional clef settings to Clairnote settings ([described here](/blog/2015/09/8va-8vb-and-clefs/)) can also change them to the Clairnote settings for staves that span an odd number of octaves. 

What else?  Well, the key signatures, time signatures, and repeat sign dots also have to be re-positioned when they are on staves with an odd number of octaves.  Done.  Did I mention that all of this works with staves that have an arbitrary number of octaves (although four is probably the largest that would see much use). 

What if we want the clefs to appear at a different octave/position on the staff?  There's now a way to do this to allow for all possible configurations.  And the key signatures, time signatures and repeat sign dots all move with the clef so they always have the same relative position in relation to each other. 

I'll pass over the more technical details and just cover the syntax.  Below are some examples taken from the [Software:LilyPond](/dn/software/) page where all of this is documented.  Note that rather than the previous `\cnThreeOctaveStaff` or `\cnFourOctaveStaff`, we now use `\cnStaffOctaveSpan 3` or `\cnStaffOctaveSpan 4` instead.  It's better to have a single command that can handle any number of octaves. 

The commands `\cnExtendStaffUp` and `\cnExtendStaffDown` work as before. They do not affect where the stems change default direction, and should be used if you want to change the staff in the middle of the music (rather than `\cnStaffOctaveSpan` ).  Use `\cnClefPositionShift` to change the position (octave) of the clef on the staff.


    \new Staff \with {
      \cnStaffOctaveSpan 3
    } {
      e4 f g a
      \cnExtendStaffUp
      e''' f''' g''' a'''
      \cnUnextendStaffUp
      a g f e
    }

![Clairnote-staff-octave-span-example](/images/Clairnote-staff-octave-span-example.png)


    \new Staff \with {
      \cnStaffOctaveSpan 3
      \cnClefPositionShift -1
    } {
      e4 f g a
      e''' f''' g''' a'''
      a g f e
    }

![Clairnote-clef-position-shift-example](/images/Clairnote-clef-position-shift-example.png) 

That covers it for now.  These multi-octave staves really call for custom clef symbols.  Maybe next year I will get around to designing and implementing those...  Here's to 2016 and More good things to come...

## Comments

**[willem](#820 "2015-12-21 18:09:29"):** good work Paul! With Lilypond improving gradually and Musescore to use as a wysiwyg input editor it is easy to create beautiful Clairnote sheet music! keep up the good work! Willem

**[Paul Morris](#821 "2015-12-21 21:11:41"):** Thanks Willem! Agreed that Musescore --> MusicXML --> LilyPond works pretty well.
