---
title: "Staves of Unusual Size"
link: https://clairnote.org/blog/2015/04/staves-unusual-size/
author: paulmorris
special_byline: ""
description:
post_id: 2883
date: 2015/04/23
created_gmt: 2015/04/23
comment_status: closed
slug: staves-unusual-size
draft: false
post_type: post
tags: ["LilyPond"]
---

Announcing the latest LilyPond improvements...  Last week I released a new version of the code that extends LilyPond so that it can produce sheet music in Clairnote (<del>clairnote-code.ly</del> clairnote.ly version 20150412).  Here is a brief summary of all the many improvements: 

1\. Better support for staves of various sizes.  This includes shortcuts for directly setting the size of the staff: `\cnStaffOctaveSpan` [originally was <del>`\oneOctaveStaff`</del>, <del>`\twoOctaveStaff`</del>, <del>`\threeOctaveStaff`</del>, <del>`\fourOctaveStaff`</del>],  and shortcuts for temporarily extending the staff by an octave to avoid ledger lines: `\cnExtendStaffUp`, `\cnUnextendStaffUp`, `\cnExtendStaffDown`, `\cnUnextendStaffDown`. The [Staff](/dn/staff/) page now documents these various staff sizes, while the [Software: LilyPond](/dn/software/#octaves) page documents the shortcuts and offers new [templates](/dn/software/#templates) for three and four octave staves. 

[Update: these commands are now prefixed with "cn". This differentiates custom Clairnote commands from standard LilyPond commands. For example, what was originally `\extendStaffUp` is now `\cnExtendStaffUp`. – October 7, 2015]

[Update: `\cnThreeOctaveStaff` and friends are now `\cnStaffOctaveSpan 3` – December 16, 2015] 

2\. Improved whole note glyphs with a better implementation (thanks to [Inkscape](https://inkscape.org/en/) and the SVG file format).  They are ever-so-slightly smaller and more aesthetically refined for use in Clairnote (the hollow "doughnut hole" is vertically smaller for a slightly thicker "doughnut" on top and bottom). 

3\. Support for the current development version of LilyPond (version 2.19.x) with continued support for the current stable version 2.18.2.  This includes support for LilyPond's new `\magnifyStaff` command that lets you scale an individual staff to different sizes without altering the proportions of its various components (well, not 100% but pretty close).  This involved a new and more robust way of handling the vertical compression of the Clairnote staff so that it now "plays nicely" with the standard LilyPond methods for scaling the size of staves.  Previously such scaling never quite worked without introducing some distortions (to custom key signatures, custom time signatures, etc.), so it's nice to have this really solved.

4\. Significantly refactored and simplified code for placing notes on the correct side of the stem in chords and harmonies (using a new approach inspired by the LilyPond source code that handles this). 

5\. Improved code for creating documents that contain both traditional and Clairnote staves.  Basically, now all clefs are supported by <del>StaffTrad and \clefsTrad</del> TradStaff whereas before only treble, bass, and alto clefs worked for both Clairnote and traditional staves (without using a different approach that involved having two copies of the music input). 

<del>6\. The code for placing traditional staves alongside Clairnote staves in the same document is now contained in its own file "trad-and-clairnote.ly".  This shortens, simplifies, and makes the "clairnote.ly" file a bit more efficient.  This more modular approach makes sense because by far the most common use case is to produce only Clairnote output.  See [Software: LilyPond](/dn/software/#same-file).</del>

[Update: the code for traditional staves is now back in the clairnote.ly file, basically thanks to a better implementation of the code for handling Clairnote clefs in LilyPond.  More details in a future post. – May 9, 2015]

That covers it!  Most of these improvements merit a post of their own, but I may not be able to find the time given my current constraints...  As usual the most recent version of the files are available for download on the [Software: LilyPond](/dn/software/) page.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
