---
title: "Sheet Music Library: 71 New Works"
link: https://clairnote.org/blog/2015/06/sheet-music-library-71-new-works/
author: paulmorris
special_byline: ""
description:
post_id: 2972
date: 2015/06/19
created_gmt: 2015/06/19
comment_status: closed
slug: sheet-music-library-71-new-works
draft: false
post_type: post
tags: ["Sheet Music"]
---

Just a quick post to announce that I have added 71 new works to the Clairnote [Sheet Music Library](/dn/sheet-music/).  These are in addition to the 109 from my [previous post](/blog/2015/06/sheet-music-library-1-year-and-109-new-works/), and they bring the total number in the library up to 460 (up from 389).  As usual they are Clairnote versions of sheet music from the [Mutopia Project](http://www.mutopiaproject.org/).

What's the story with this batch of new works?  Previously the library only contained simpler pieces that only involved a single LilyPond file, but now the library's conversion infrastructure can automatically handle works that are encoded in multiple LilyPond files.  So... these new additions are all encoded in more than one LilyPond file.  Many of them are more complex works for ensembles that include separate sheet music for each part as well as for all the parts together in one score (e.g. a conductor's score).  For example, try filtering the library for the instruments "Orchestra," "Ensemble," "String-ensemble", or "String-quartet".

Below is a list of all the new additions.  (There's even a L.V. Beethoven piece for four trombones!)

Anonymous | Trio a due mandolini e Basso (Gimo 359) | 2 Mandolins, Bass

J. S. Bach (1685–1750) | Menuet in G | Transcribed for Trumpet Duet

E. Barbella (1718–1777) | Sonata a Due Mandolini (Gimo 12) | 2 Mandolins

E. Barbella (1718–1777) | Sonata a Due Mandolini (Gimo 13) | 2 Mandolins

E. Barbella (1718–1777) | Sonata a Due Mandolini (Gimo 14) | 2 Mandolins

E. Barbella (1718–1777) | Sonata a Due Mandolini (Gimo 15) | 2 Mandolins

E. Barbella (1718–1777) | Sonata a Due Mandolini e Basso (Gimo 18) | 2 Mandolins and Bass

L. V. Beethoven (1770–1827) | Rondo A Capriccio | Piano

L. V. Beethoven (1770–1827) | Drei Equali | Trombone

B. Blake (1751–1827) | A Second Sett of Six Duetts for a Violin & Tenor: No. 1 | Violin, Viola

B. Blake (1751–1827) | A Second Sett of Six Duetts for a Violin & Tenor: No. 2 | Violin, Viola

B. Blake (1751–1827) | A Second Sett of Six Duetts for a Violin & Tenor: No. 3 | Violin, Viola

B. Blake (1751–1827) | A Second Sett of Six Duetts for a Violin & Tenor: No. 4 | Violin, Viola

B. Blake (1751–1827) | A Second Sett of Six Duetts for a Violin & Tenor: No. 5 | Violin, Viola

B. Blake (1751–1827) | A Second Sett of Six Duetts for a Violin & Tenor: No. 6 | Violin, Viola

D. Caudioso (17??–?) | Concerto di mandolino a solo con Violini e Basso (Gimo 58) | Ensemble: Mandolin, 2 Violins, ‘Cello

C. Cecere (1706–1761) | Concerto di Mandolino con Violini, e Basso (Gimo 60) | Ensemble: Mandolin, 2 Violins, ‘Cello

F. F. Chopin (1810–1849) | Prelude: Op. 28, No. 2 | Piano

F. F. Chopin (1810–1849) | Prelude: Op. 28, No. 6 | Piano

F. F. Chopin (1810–1849) | Prelude: Op. 28, No. 7 | Piano

F. F. Chopin (1810–1849) | Valse Op. 64, No. 1 (‘Minute Waltz’) | Piano

G. Cocchi (1712–1796) | Sinfonia a due mandolini e Basso (Gimo 76) | 2 Mandolins, Bass

G. Gabellone (1727–1796) | Concerto di mandolino con Violini e Basso obligati (Gimo 88) | Ensemble: Mandolin, 2 Violins, ‘Cello

G. B. Gervasio (c.1725–c.1785) | Sonata Per Camera di Mandolino e Basso (Gimo 141) | Mandolin and Bass

G. B. Gervasio (c.1725–c.1785) | Sonata Per Camera di Mandolino e Basso (Gimo 142) | Mandolin and Bass

G. B. Gervasio (c.1725–c.1785) | Sonata Per Camera di Mandolino e Basso (Gimo 144) | Mandolin and Bass

G. B. Gervasio (c.1725–c.1785) | Sonata per Mandolino e Basso (Gimo 145) | Mandolin and Bass

G. B. Gervasio (c.1725–c.1785) | Duetto à Due Mandolini (Gimo 147) | 2 Mandolins

G. B. Gervasio (c.1725–c.1785) | Sonata per Camera di Mandolino è Basso (Gimo 149) | 2 Mandolins, Bass

G. B. Gervasio (c.1725–c.1785) | Trio a Due Mandolini e Basso (Gimo 150) | 2 Mandolins, Bass

M. Giuliani (1781–1829) | Serenade op.127 nr.1: Maestoso | Flute, Violin, Guitar

M. Giuliani (1781–1829) | Six Variations for Guitar | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 6 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 8 | Guitar

G. Giuliano (c.1750) | Sonata in E Per Mandolino, e Basso | Mandolin and Bass

G. Giuliano (c.1750) | Sonata in Ess Per Mandolino, e Basso | Mandolin and Bass

G. Giuliano (c.1750) | Sinfoni per Mannolino con Più Istromenti (Gimo 153) | Ensemble: Mandolin, Violin, ‘Cello, Bass

E. Grieg (1843–1907) | Anitra’s Dance (Tanz) from Peer Gynt Suite I | String Ensemble: Violins, Violas, Cello, Double Bass, Triangle

C. Hunter (1876–1906) | Possum And Taters | Piano

S. Joplin (1868–1917) | Bethena | Piano

S. Joplin (1868–1917) | Solace | Piano

J. B. Lully (1632–1687) | Acte II, Scene III from Armide | Voice (Tenor), String Ensemble, Basso Continuo

F. Mendelssohn-Bartholdy (1809–1847) | Ein Sommernachtstraum – No.5 | Orchestra: Flute, Oboe, Clarinet, Bassoon, French Horn, Violins, Viola, ‘Cello, Double Bass

W. A. Mozart (1756–1791) | Andante in C Major K. 315 | Flute and Orchestra: Violins, Viola, Cello, Horns, Oboes and Bass

W. A. Mozart (1756–1791) | Komm, liebe Zither, komm | Voice and Mandolin

W. A. Mozart (1756–1791) | String Quartet KV. 387 (nr. 14) | String Quartet: Two Violins, Viola, ‘Cello

W. A. Mozart (1756–1791) | String Quartet KV. 428 (nr. 16) | String Quartet: Two Violins, Viola, ‘Cello

W. A. Mozart (1756–1791) | Horn Concerto No. 3 | Horn in F

W. A. Mozart (1756–1791) | String Quartet KV. 458 (nr. 17) \ | String Quartet: Two Violins, Viola, ‘Cello

W. A. Mozart (1756–1791) | String Quartet KV. 465 (nr. 19) \ | String Quartet: Two Violins, Viola, ‘Cello

W. A. Mozart (1756–1791) | 12 Duets (No. 10: Andante) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 11: Menuetto) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 12: Allegro) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 2: Menuetto) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 5: Larghetto) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 6: Menuetto) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 7: Adagio) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 8: Allegro) | Horn Duet

W. A. Mozart (1756–1791) | 12 Duets (No. 9: Menuetto) | Horn Duet

W. A. Mozart (1756–1791) | Trio in E-flat Major KV. 498 | Clarinet (or Violin), Viola and Piano (also Piano solo)

W. A. Mozart (1756–1791) | Canzonetta \ | Ensemble: Baritone, Mandolin, Violin, Viola, ‘Cello

W. A. Mozart (1756–1791) | Dies Irae | Voice (SATB), Orchestra: Basset Horns in F, Bassoon, Trumpet in D, Timpani, Violins, Viola, Cello,
Organ/Bass

N. Paganini (1782–1840) | 24 Caprices for Solo Violin: 02 | Violin

N. Paganini (1782–1840) | 24 Caprices for Solo Violin: 03 | Violin

N. Paganini (1782–1840) | Sonata No. 10 in A | Violin, Cello

A. Rolla (1757–1841) | 6 Duetti a Due Viole: No. 1 | Viola

A. Rolla (1757–1841) | 6 Duetti a Due Viole: No. 2 | Viola

A. Rolla (1757–1841) | Sei Duetti a Due Viole | Viola

A. Rolla (1757–1841) | Sei Duetti a Due Viole | Viola

F. Schubert (1797–1828) | Gretchen am Spinnrade | Voice and Piano

F. Schubert (1797–1828) | Die Forelle | Voice and Piano

V. Ugolino (?–?) | Concerto Per Mandolino, Violino Primo, Violino Secondo e Basso (Gimo 297) | Ensemble: Mandolin, 2 Violins, ‘Cello

A. Vivaldi (1678–1741) | Concerto in D Minor for Viola d’amore and Lute | Viola d’amore, Lute, String Ensemble: Violins, Viola, Cello, Basso
Continuo
