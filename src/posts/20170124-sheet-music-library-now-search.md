---
title: "Sheet Music Library Now With Search"
link: https://clairnote.org/blog/2017/01/sheet-music-library-now-search/
author: paulmorris
special_byline: ""
description:
post_id: 3466
date: 2017/01/24
created_gmt: 2017/01/24
comment_status: closed
slug: sheet-music-library-now-search
draft: false
post_type: post
tags: ["Clairnote Website"]
---

Just a quick post to announce that I have revamped the [Clairnote Sheet Music Library](/dn/sheet-music/), providing a number of improvements. Adding search was the primary motivation.  Previously you had to rely on your browser's "find text on the page" feature to approximate search functionality.  Now we have real search as we've come to expect, with only the relevant results shown, sorted by how well they match the search term(s).

A javascript package called [lunr.js](http://lunrjs.com/) provides the new search functionality.  It implements a _client-side_ search engine.  All the meta data for the sheet music library is loaded into the browser when the page loads. The browser then handles searches without having to communicate further with the server (which can be slow).  This makes searches really fast.  (The previous version of the library also loaded all of the meta data into the browser, so the page load times are about the same.)

## Filters, Like Before but Better

Beyond searching you can still filter the library like before and the filters work with the search feature as you would expect.  However, now you can easily see which filters are currently in effect and the filter interface is simpler, divided into the three separate categories (instruments, styles, and composers) rather than showing all of them together.  Now that you can search, you may not need the filters as much, but they can still be helpful.  As before you can browse through parts of the library by using the filters.  (Just don't enter any search terms.)  The filters also help communicate what is in the library so you know what to search or filter for.

## Out with the Grid, In with the List

Another improvement is that the library now displays works / search results in a more compact and space-efficient list format, whereas before it displayed them as a fancy grid of rectangles, like this:

![The old grid layout of the Clairnote sheet music library](/images/grid-clairnote-sheet-music-library-January-2017.png)

The new list format is simpler and should make it easier to find what you're looking for.  (It also avoids some problems with the grid layout on some versions of Internet Explorer.)  Here is a screenshot:

![The old grid layout of the Clairnote sheet music library](/images/list-clairnote-sheet-music-library-January-2017.png)

## What Happened to the MIDI Playback?

While MIDI playback was a good idea in theory, in practice it was not so great, so I removed it.  One problem was that you could not control the tempo and the playback was often too fast or too slow.  Another was that with some files that had multiple voices only one of them would play.  I decided that it was best to just let people download the midi files and play them on a full-featured MIDI player.  Any effort towards providing a good experience with MIDI in the browser would be better spent elsewhere.  (At least at this point.  Maybe MIDI playback support in browsers will improve?)

The next post will cover recent additions to the library — 92 new works!
