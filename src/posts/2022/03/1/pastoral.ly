\version "2.20.0"
% automatically converted by musicxml2ly from /home/andres/Música/Clairnote/pastoral.mxl
%\pointAndClickOff
\include "clairnote.ly"

\header {
  subtitle =  "Opus 100 - 25 Easy Studies"
  title =  "La Pastorale"
  encodingdate =  "2022-02-20"
  source =  "/home/user/pastoral-mutopia.pdf"
  composer =  "Friedrich Burgmüller"
  encodingsoftware =  "MuseScore 3.2.3"
  tagline =  ""
}

#(set-global-staff-size 18)
\paper {
  paper-width = 21.0\cm
  paper-height = 29.7\cm
  top-margin = 1.0\cm
  bottom-margin = 1.0\cm
  left-margin = 1.0\cm
  right-margin = 1.0\cm
  indent = 1.6 \cm
  short-indent = 0.2 \cm
  system-system-spacing.padding = 0.8 \cm 
  ragged-last-bottom = ##t
  ragged-last = ##t
}

\layout {
  \context {
    \Score
    skipBars = ##t
    autoBeaming = ##f
  }
}
PartPOneVoiceOne =  {
  \clef "treble" \key g \major \time 6/8 | % 1
  \tempo "Andantino"
  g'8 ( -1 [ _\p b'8 -2 _\markup{
    \small\italic {dolce cantabile}
  } c''8 ] d''8 [ b'8 -1 e''8 -3 ]
  | % 2
  d''8 [ g''8 -5 e''8 -3 ] d''8 [ b'8 c''8 -2 ] \repeat volta 2 {
    | % 3
    d''4. ) ( _\p b'8 -1 [ d''8 -2 \acciaccatura { fis''8 } e''8 ] | % 4
    d''4. g''8 ) ( [ d''8 b'8 -1 ] | % 5
    a'4. -2 \acciaccatura { a'16 [ b'16 ] } c''8 [ a'8 b'8 ] | % 6
    g'4. ) g'8 ( -1 [ b'8 -2 c''8 ] | % 7
    d''4. b'8 -1 [ d''8 -2 \acciaccatura { fis''8 } e''8 ] | % 8
    d''4. d''8 ) ( [ b'8 g''8 ] | % 9
    fis''4. _\markup{ \small\italic {cresc.} } e''8 [ fis''8 e''8 ]
    | \barNumberCheck #10
    d''4. d'''4 ) r8
  }
  | % 11
  a''4. ( _\mf c''8 -1 [ e''8 -4 d''8 ] | % 12
  b'4. ) -1 d''8 ( [ cis''8 d''8 -1 ] | % 13
  a''4. c''8 -1 [ e''8 -4 d''8 ] | % 14
  b'4. ) d''8 ( [ e''8 d''8 ] | % 15
  cis''2. -2 | % 16
  d''8 ) [ es''8 -3 _\> es''8 -3 ] es''8 ( -3 [ d''8 c''8 ] _\! | % 17
  b'8 -2 [ _\> c''8 -1 d''8 ] _\! fis''8 -4 [ _\> e''8 c''8 -1 ] _\! | % 18
  b'4. -2 a'8 ) -1 [ d''8 -4 d''8 -3 ] | % 19
  d''4. ( _\p _\markup{ \small\italic {dolce} } b'8 [ d''8
  \acciaccatura { fis''8 } e''8 ] | \barNumberCheck #20
  d''4. g''8 ) ( [ d''8 b'8 -1 ] | % 21
  a'4. -2 \acciaccatura { a'16 [ b'16 ] } c''8 [ a'8 b'8 ] | % 22
  g'4. ) g'8 ( [ b'8 c''8 ] | % 23
  d''4. _\markup{ \small\italic {cresc.} } b'8 -1 [ d''8 -2 e''8 ] | % 24
  d''4. c''8 ) ( [ _\> e''8 g''8 -. ] _\! | % 25
  g''8 [ _\> d''8 b'8 ) -1 ] _\! b'4 ( a'8 | % 26
  g'4. ) _\p \acciaccatura { b'8 } a'8 ( [ e'8 fis'8 ] | % 27
  g'4. ) \acciaccatura { b'8 } a'8 ( [ e'8 fis'8 ] | % 28
  g'8 ) -. -1 [ _\markup{ \small\italic {dim. e poco rall.} } b'8 -.
  -2 d''8 -. -3 ] g''8 -. -1 [ b''8 -. d'''8 -. ] | % 29
  g'''4 _\pp r8 r4. \bar "|."
}

PartPOneVoiceFive =  {
  \clef "bass" \key g \major \time 6/8 R2.*2 \repeat volta 2 {
    \stemNeutral <g-5 b-3 d'-1>8 [ <g b d'>8 <g b d'>8 ] <g b d'>4. <g b d'>8
    [ <g b d'>8 <g b d'>8 ] <g b d'>4. <g c' d'>8 [ <g c' d'>8 <g c'
    d'>8 ] <g c' d'>4. <g b d'>8 [ <g b d'>8 <g b d'>8 ] <g b
    d'>4. <g b d'>8 [ <g b d'>8 <g b d'>8 ] <g b d'>4. <g b d'>8
    [ <g b d'>8 <g b d'>8 ] <g b d'>4. | % 9
    \clef "treble" a8 ( [ a'8 ) a'8 ] g'8 ( [ a'8 g'8 ) ] fis'8 ( [
    a'8 fis'8 ) ] d'4 r8
  }
  | % 11
  \clef "bass" <fis c'>2. <g b>2. <fis c'>2. <g b>2. | % 15
  \clef "treble" r8 bes'8 -2 [ _\> bes'8 -2 ] bes'8 ( -2 [ a'8 -1 g'8
  ] _\! fis'2. ) g'4. <c' e'>4. \stemDown d'4. _~ d'4 r8 | % 19
  \stemNeutral \clef "bass" <g b d'>8 [ <g b d'>8 <g b d'>8 ] <g b d'>4. <g b d'>8
  [ <g b d'>8 <g b d'>8 ] <g b d'>4. <g c' d'>8 [ <g c' d'>8 <g c' d'>8
  ] <g c' d'>4. <g b d'>8 [ <g b d'>8 <g b d'>8 ] <g b d'>4. <f g b
  d'>8 [ <f g b d'>8 <f g b d'>8 ] <f g b d'>4. <e g c'>8 [ <e g
  c'>8 <e g c'>8 ] <e g c'>4. <d g b>4. d8 ( [ d'8 c'8 ) ] <g b>8
  [ <g b d'>8 <g b d'>8 ] <g c' d'>4. <g b d'>8 [ <g b d'>8 <g b d'>8
  ] <g c' d'>4. <g b d'>4 r8 <g b d'>4 r8 g,4 r8 r4. \bar "|."
}

PartPOneVoiceSix =  {
  \clef "bass" \key g \major \time 6/8 s1. \repeat volta 2 {
    s4*15 s2. | % 9
    \clef "treble" s1.
  }
  | % 11
  \clef "bass" \stemUp d''8 \rest d'8 [ d'8 ] d'4. d''8 \rest d'8 [ d'8 ] d'4.
  d''8 \rest d'8 [ d'8 ] d'4. d''8 \rest d'8 [ d'8 ] d'4. | % 15
  \clef "treble" s4*9 f''8 \rest g'8 [ g'8 ] fis'4 r8 | % 19
  \clef "bass" s4*9 s1*6 \bar "|."
}


% The score definition
\score {
  <<

    \new PianoStaff
    <<
      \set PianoStaff.instrumentName = "Piano"

      \context Staff = "1" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceOne" {  \PartPOneVoiceOne }
      >> \context Staff = "2" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceFive" {  \voiceOne \PartPOneVoiceFive }
        \context Voice = "PartPOneVoiceSix" {  \voiceTwo \PartPOneVoiceSix }
      >>
    >>

  >>
  \layout {}
  % To create MIDI output, uncomment the following line:
  %  \midi {\tempo 4 = 120 }
}

