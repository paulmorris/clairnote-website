\version "2.20.0"
% automatically converted by musicxml2ly from /home/andres/Música/Clairnote/adelita.mxl
\pointAndClickOff

\header {
  encodingsoftware =  "MuseScore 3.2.3"
  encodingdate =  "2022-03-20"
  subtitle =  Mazurka
  composer =  "Francisco Tárrega"
  title =  "¡Adelita!"
}

#(set-global-staff-size 20.1587428571)
\paper {

  paper-width = 21.01\cm
  paper-height = 29.69\cm
  top-margin = 1.0\cm
  bottom-margin = 2.0\cm
  left-margin = 1.0\cm
  right-margin = 1.0\cm
  indent = 1.61615384615\cm
  short-indent = 0.505048076923\cm
}
\layout {
  \context {
    \Score
    autoBeaming = ##f
  }
}
PartPOneVoiceOne =  {
  \repeat volta 2 {
    \clef "treble_8" \key g \major \time 3/4 | % 1
    e''8 ( [ ^\markup{ \bold {Andante} } _\p dis''8 ) ] b'2 -> | % 2
    d''8 ( [ c''8 ) ] fis'2 -> | % 3
    c''8 ( [ ^ "C VII" b'8 ) ] dis'4 -> g'4 | % 4
    \grace { fis'16 ( [ g'16 ] } fis'8 ) -> [ e'8 ] b8 [ e'8 ] g'8 [
    b'8 \glissando ] | % 5
    e''8 ( -> [ dis''8 ) ] b'2 -> | % 6
    d''16 [ e''16 c''8 ] fis'2 -> | % 7
    c''8 ( [ ^ "C VII" b'8 ) ] dis'4 -> g'4 \glissando _\markup{
      \small\italic {rit.}
    } | % 8
    <g b e'>2.
  }
  \repeat volta 2 {
    | % 9
    \key e \major | % 9
    gis'4. _\f ^ "C IV" b'8 e'8 [ fis'8 ] | \barNumberCheck #10
    gis'4. b'8 e'8 [ fis'8 ] | % 11
    gis'8 [ b'8 \glissando ] e''8 [ dis''8 ] \acciaccatura {
      dis''16
      [ e''16 ]
    } dis''8 [ ^ "C IX" cis''8 ] | % 12
    \acciaccatura { cis''16 [ dis''16 ] } cis''8 [ _\markup{
      \small\italic {rit.}
    } _\> ^ "C VII" b'8 ] a'8 [ fis'8 ]
    dis'8 [ b8 \glissando ] | % 13
    gis4. _\! ^\markup{ \italic {a tempo} } b8 e8 [ fis8 ] | % 14
    gis8 [ b8 \glissando ] _\markup{ \small\italic {tenuto} } gis'8
    [ dis'8 ] \acciaccatura { e'16 ^\fermata [ fis'16 ^\fermata ] }
    e'8. ^\fermata ^ "C VIII" _\> ais16 | % 15
    b8. [ _\! _\p <a cis'>16 ] <gis b>4 _\markup{
      \small\italic
      {rit.}
    } <fis a dis'>4 ^ "C II" | % 16
    <gis b e'>2 r4
  }
}

PartPOneVoiceThree =  {
  \repeat volta 2 {
    \clef "treble_8" \key g \major \time 3/4 e2 \rest <e' g'>4 e2
    \rest <c' e'>4 e2 \rest <a dis'>4 s2. e2 \rest <e' g'>4 e2 \rest
    <c' e'>4 e2 \rest <a dis'>4 s2.
  }
  \repeat volta 2 {
    | % 9
    \key e \major e4 \rest <gis b e'>4 cis'4 e4 \rest <gis b e'>4
    cis'4 <b e'>4 e4 \rest e'4 s2. d,4 \rest <b e'>4 <cis' e'>4 s4*9
  }
}

PartPOneVoiceTwo =  {
  \repeat volta 2 {
    \clef "treble_8" \key g \major \time 3/4 e,2. a,2. b,2. e,2. e,2.
    a,2. b,2. c4 \rest e,2
  }
  \repeat volta 2 {
    ^ "Fine" | % 9
    \key e \major e,2 a,4 e,2 a,4 e,2 fis4 <b, a dis'>2 r4 e,2 a,4
    e,2 <c ais>4 b,2 b,4 e4 e,2
  }
}


% The score definition
\score {
  <<

    \new Staff
    <<
      \set Staff.instrumentName = "Classical Guitar"
      \set Staff.shortInstrumentName = "Guit."

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceOne" {  \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceThree" {  \voiceTwo \PartPOneVoiceThree }
        \context Voice = "PartPOneVoiceTwo" {  \voiceThree \PartPOneVoiceTwo }
      >>
    >>

  >>
  \layout {}
  % To create MIDI output, uncomment the following line:
  %  \midi {\tempo 4 = 106 }
}

