# Clairnote Website

Initially based on the default Gatsby v2 starter ([gatsby-starter-default](https://github.com/gatsbyjs/gatsby-starter-default/tree/v2)).

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/).

## Generating SVG images from LilyPond (.ly) files

See notes in `/src/images/README.md`.

## Sheet Music Search Index

A `lunr` search index needs to be pre-built, to prevent runtime code
from having to build it over and over. Build it by running:

`npm run build-indexes`

The relevant sheet music data files will be read and search indexes created.
The indexes are then imported for use on the "Sheet Music" page. These index
files are not checked into git since they are built artifacts.

## Cache Clearing

To clear gatsby's cache, manually delete the `.cache` directory in the root
directory. For example, clearing the cache is required to see images added to a
blog post.
